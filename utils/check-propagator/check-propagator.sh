#! /bin/bash

##########################################################################################
# Output invocation
echo
echo "______________________________________________________________________"
echo "SCRIPT INVOKED AS:"
echo $0 $@
echo "______________________________________________________________________"
echo
#
##########################################################################################





##########################################################################################
# Definition of the usage
function usage() {
  cat <<EOF

Usage: check-quote.sh [options]

    -h			This message
    -t [runmode]	Options: reference / td
    -x [octopus]	The executable
    -m [ndivisions]	The fundamental number of division of each period to get the spacing (default: 200)
    -p [npoints]	(default: 4)
    -l [gridsize]       Whether the grid of points is linear, in which case gridsize is the step size. If "-l" is
                        not given, the grid is logarithmic, meaning that it is built by doubling the number of divisions.
    -v [propagator]	[default: propagator_taylor_expansion]
    -f [logfactor]
EOF
  exit 0;
}
#
##########################################################################################


##########################################################################################
# Definition of the quote and compare invocations
function runquote() {
  $CHECKPRBINDIR/quote
}


function runcompare() {
  dim=$(head -n 1 input/H0)
  tail -n 1 output/td-coeff-int > td-coeff
  tail -n 1 output-reference/td-coeff-int > td-coeff.ref
  paste td-coeff td-coeff.ref | awk -v "dim=$dim" '{ nrm2=0.0; for (i = 2; i <= dim+1; ++i) sum = sum + ($i-$(i+2*dim+1))^2; printf "%.12e\n", sqrt(sum) }'
}
#
##########################################################################################





##########################################################################################
# Read command line options. First, assign defaults.

[ ${CHECKPRBINDIR} ] || CHECKPRBINDIR=$HOME/bin
echo ${CHECKPRBINDIR}

RUNMODE=td
ndivisions=200
npoints=4
logfactor=2.0
TDPropagator=propagator_taylor_expansion
LINEARGRID=no
while getopts "ht:x:m:p:v:l:f:" opt ; do
    case "$opt" in
        h) usage ;;
        t) RUNMODE="$OPTARG" ;;
	x) CHECKPRBINDIR="$OPTARG" ;;
        m) ndivisions="$OPTARG" ;;
        p) npoints="$OPTARG" ;;
        v) SETPROPAGATOR="yes"; TDPropagator="$OPTARG" ;;
        l) LINEARGRID="yes"; gridsize="$OPTARG";;
        f) logfactor="$OPTARG" ;;
        ?) echo "Error parsing arguments"; exit 1 ;;
    esac
done


echo ${CHECKPRBINDIR}


#
##########################################################################################


##########################################################################################
# This is for gnuplot: check if we are running in the background or not.
a=$(ps -o stat= -p $$)
[ "${a/+/}" = "$a" ] && RUNINBACKGROUND=1 || RUNINBACKGROUND=0
#
##########################################################################################



echo "______________________________________________________________________"
echo "RUNNING $CHECKPRBINDIR/quote"
echo "______________________________________________________________________"
echo




case $RUNMODE in


##########################################################################################
  reference)

  echo
  echo "______________________________________________________________________"
  echo "RUNNING IN TD REFERENCE MODE"
  echo "______________________________________________________________________"
  echo

  ntsteps_reference=$(($ndivisions*100))

  echo "   Number of time steps for the reference calculation = " $ntsteps_reference
  echo "   Propagator = " $TDPropagator


  echo "ntsteps = " $ntsteps_reference >> tmp
  echo "propagator = " $TDPropagator >> tmp
  cat tmp input.lua.base > input.lua
  runquote > out.reference 2>&1

  test -d output-reference && rm -rf output-reference
  mv output output-reference

  ;;


##########################################################################################
  td)

  echo
  echo "______________________________________________________________________"
  echo "RUNNING IN TD MODE"
  echo "______________________________________________________________________"
  echo

  echo "   Number of time steps (initial) = " $ndivisions
  echo "   Propagator = " $TDPropagator

  #declare -a prop=( $TDPropagator )

  declare -a ndiv
  ndiv[1]=$ndivisions

  for i in $(seq 2 $npoints)
  do
    if [ ${LINEARGRID} == "no" ] ; then
      ndiv[i]=$(bc -l <<< "scale=0; $logfactor*${ndiv[$i-1]}/1")
    else
      ndiv[$i]=$((ndiv[$i-1]+$gridsize))
    fi
  done

  k=$TDPropagator

    TDPropagator=$k
    rm -f results.$k
    for j in ${ndiv[*]}
    do
      i=$(echo "1/$j" | bc -l)
      echo "   Propagator: " $k ";   Dividing factor: " $j ";#   Time step: " $i
      echo "ntsteps = " $j > tmp
      echo "propagator = " $TDPropagator >> tmp
      cat tmp input.lua.base > input.lua
      runquote > out.$k.$j 2>&1
      cost=$(grep 'Total propagation time:' out.$k.$j | awk '{print $4}')
      echo "Cost = $cost"
      runcompare > res.$k.$j
      delta=$(cat res.$k.$j)
      echo $i $delta $cost >> results.$k
      mv input.lua input.lua.$k.$j
    done

    cat<<EOF|gnuplot -persist
if ($RUNINBACKGROUND) { 
   set terminal pngcairo enhanced; set output "$k.png" 
}
else {
   set terminal x11 enhanced
}
set key bottom right
stats "results.$k" u (log10(\$1)):(log10(\$2)) name "a"
stitle=sprintf("SLOPE = %f",a_slope)
set ytics nomirror
set y2tics
set xlabel "log_{10} {/Symbol D}t"
set ylabel "log_{10} E"
set y2label "Cost (s)"
set title "Error in the wavefunction"
plot "results.$k" u (log10(\$1)):(log10(\$2)) w p title "$k" axes x1y1, a_intercept + a_slope*x w l title stitle, "results.$k" u (log10(\$1)):((\$3)) w lp title "COST" axes x1y2
#plot "results.$k" u (log10(\$1)):(log10(\$2)) w p title "$k" axes x1y1, a_intercept + a_slope*x w l title stitle
EOF

  #done

  ;;


##########################################################################################
  *) 

  echo "Unacceptable run mode";
  exit 1;

  ;;

esac




