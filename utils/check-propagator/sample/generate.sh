#! /bin/bash

# This is an example of use of the check-propagator.sh script.

if [ ! -d "$1" ] ; then
  echo "You must supply the quote bin directory first argument."
  exit 2
fi
quotedir=$1

export OMP_NUM_THREADS=1

test -d tmp && rm -rf tmp
mkdir tmp

cd tmp

cp ../input.lua.base .
cp -a ../input .

propagator=propagator_cfmagnus4
#propagator=propagator_rungekutta
#propagator=propagator_taylor_expansion

../check-propagator.sh -x $quotedir -t reference -v $propagator -m 20
../check-propagator.sh -x $quotedir -t td -v $propagator -m 20

cd ..
