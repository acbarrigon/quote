# Module to find the aotus library.

find_path(AOTUS_INCLUDE_DIRS
    NAMES aotus_module.mod
    HINTS ${AOTUS_PREFIX}/include)

find_library(AOTUS_LIBRARIES
    HINTS ${AOTUS_PREFIX}/lib
    NAMES aotus)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(AOTUS
    FAIL_MESSAGE  DEFAULT_MSG
    REQUIRED_VARS AOTUS_INCLUDE_DIRS AOTUS_LIBRARIES)
