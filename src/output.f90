!! Copyright (C) 2014-20xx The quote developing team.
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
module output_m
  use global_m
  use hamiltonian_m
  use tdfield_m
  use states_m

  implicit none

  private
  public ::     &
    output_print

  contains

  
  subroutine output_print(tdf , h , st)
    type(tdfield_t), intent(inout) :: tdf(:)   
    type(hamiltonian_t), intent(inout) :: h
    type(states_t), intent (in) :: st(0:maxntime)

    character(len=50) :: format_string
    integer :: iunit_occup = 11, &
               iunit_coeff_sch = 12, &
               iunit_coeff_int = 13
    integer :: nt, j
    real(8) :: t
    real(8), allocatable :: occup(:)
    complex(8), allocatable :: cct(:), cc0(:), u(:, :)

    open(unit = iunit_occup, file = './output/td-occup' , status = 'unknown' )
    open(unit = iunit_coeff_sch, file = './output/td-coeff-sch' , status = 'unknown' )
    open(unit = iunit_coeff_int, file = './output/td-coeff-int' , status = 'unknown' )

    allocate(occup(1:dim))
    allocate(cc0(1:dim))
    allocate(cct(1:dim))
    allocate(u(dim, dim))

    call states_get(init_st, cc0)
    
    write(format_string, '(a,i5,a)') '(',2*dim+1,'e22.12e3)'
    do nt = 0, maxntime
      t = nt*dt
      select case(object)
      case(wavefunction)
        call states_get(st(nt), cct)
      case(evolution_operator)
        call states_get(st(nt), u)
        cct = matmul(u, cc0)
      end select
      
      select case(picture)
      case(schrodinger_picture)
        write(iunit_coeff_sch, format_string) nt*dt, (cct(j), j = 1, dim)
      case(interaction_picture)
        write(iunit_coeff_int, format_string) nt*dt, (cct(j), j = 1, dim)
        call states_to_schroedinger(cct, t)
        write(iunit_coeff_sch, format_string) nt*dt, (cct(j), j = 1, dim)
      end select
      occup(:) = real( conjg(cct(:))*cct(:), kind = 8)
      write(iunit_occup, '(500(e22.12e3,1x))') t, occup(1:dim)
    end do
    
    close(iunit_occup)
    close(iunit_coeff_sch)
    close(iunit_coeff_int)
    deallocate(occup)
    deallocate(cc0)
    deallocate(cct)
    deallocate(u)
  end subroutine output_print

end module output_m
