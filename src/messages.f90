!! Copyright (C) 2014-20xx The quote developing team.
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
module messages_m
  use global_m

  implicit none

  private
  public ::               &
    messages_header,      &
    messages_dir

  contains
  
  subroutine messages_header()
    write(*,*) ''
    write(*,*) ''
    write(*,*) '******************************************************'
    write(*,*) '******************************************************'
    write(*,*) '******************************************************'
    write(*,*) '*                                                    *'
    write(*,*) '*            "QOCT for N-level systems"              *'
    write(*,*) '*                                                    *'
    write(*,*) '*            The quote developing team               *'
    write(*,*) '*                                                    *'
    write(*,*) '* Copyright (C) 2014-20xx The quote developing team. *'
    write(*,*) '*                                                    *'
    write(*,*) '******************************************************'
    write(*,*) '******************************************************'
    write(*,*) '******************************************************'
    write(*,*) ''
    write(*,*) ' git id: '//GIT_COMMIT_HASH
    write(*,*) ''
    write(*,*) '******************************************************'
    write(*,*) '******************************************************'
    write(*,*) '******************************************************'
  end subroutine messages_header


  subroutine messages_dir()
  
    logical  ::  folder_exist
  
    inquire( file = './output/.', exist = folder_exist )
    if ( .not. folder_exist ) call system('mkdir output')
  
  end subroutine messages_dir


end module messages_m
