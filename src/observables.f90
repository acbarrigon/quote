!! Copyright (C) 2014-20xx The quote developing team.
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
module observables_m
  use aotus_module
  use global_m
  use hamiltonian_m
  use input_m
  use states_m

  implicit none

  private
  public ::             &
    observables_init,   &
    observables_print,  &
    observables_number, &
    observables_end,    &
    observable_t,       &
    observables,        &
    nobservables

  type observable_t
    type(operator_t) :: op
    character(len=100) :: name
  end type observable_t

  integer :: nobservables
  type(observable_t), allocatable :: observables(:)

  contains


  integer function observables_number(name)
    character(len=*) name

    integer :: j

    observables_number = 0
    do j = 1, nobservables
      if(trim(adjustl(name)) == trim(adjustl(observables(j)%name)) ) observables_number = j
    end do
  end function observables_number


  subroutine observables_print(h, maxntime, st, dt)
    type(hamiltonian_t), intent(in) :: h
    integer, intent(in)             :: maxntime
    type(states_t), intent(in)      :: st(0:maxntime)
    real(8), intent(in)             :: dt

    integer :: j, nt
    real(8) :: t
    type(states_t) :: d

    do j = 1, nobservables
     open ( unit = 11 , file = './output/td-'//trim(observables(j)%name) )
     do nt = 0, maxntime
       t = nt * dt
       d = st(nt)
       if(picture == interaction_picture) then
         call states_to_schroedinger(d, t)
       end if
       write(11, '(2e22.12e3)') t, real(operator_expvalue(observables(j)%op, d), 8)
     end do
    end do

    close(11)
  end subroutine observables_print


  subroutine observables_init(h)
    type(hamiltonian_t), intent(in) :: h

    character(len=1000) :: variableslist
    integer :: pos1, pos2, j, dim_file, k
    logical :: file_exists
    complex(8), allocatable :: ob(:, :)

    call aot_get_val(L = input_file, key = 'observables', val = variableslist, &
      errcode = input_error, default = "")

    if(variableslist == "") then
      nobservables = 0
      return
    end if

    pos1 = 1
    nobservables = 1
    do
     pos2 = index(variableslist(pos1:), ',')
     if(pos2 == 0) exit
     nobservables = nobservables + 1
     pos1 = pos2+pos1
    end do

    allocate(observables(nobservables))

    if( nobservables == 1 ) then
      observables(1)%name = trim(variableslist)
    else
      pos1 = 1
      pos2 = index(variableslist(1:), ',')
      observables(1)%name = trim(adjustl(variableslist(1:pos2-1)))
      pos1 = pos1+pos2
      do j = 2, nobservables-1
        pos2 = index(variableslist(pos1:), ',')
        observables(j)%name = trim(adjustl(variableslist(pos1:pos1+pos2-2)))
        pos1 = pos2 + pos1        
      end do
      observables(nobservables)%name = trim(adjustl(variableslist(pos1:)))
    end if

    do j = 1, nobservables
      inquire( file = './input/'//trim(observables(j)%name), exist = file_exists )
      if ( .not. file_exists ) then
        write(*, '(a)') 'Error: Could not find observable file /input/'//trim(observables(j)%name)
        stop
      else
        call operator_init(observables(j)%op, dim, mode = object)
        call operator_read(observables(j)%op, './input/'//trim(observables(j)%name) )
        if(picture == interaction_picture) then
          call operator_to_eigenbasis(observables(j)%op)
          call operator_write(observables(j)%op, './output/'//trim(observables(j)%name)//'p')
        end if
      end if
    end do

  end subroutine observables_init


  subroutine observables_end()
    integer :: j
    if(nobservables > 0) then
      do j = 1, nobservables
        call operator_end(observables(j)%op)
      end do
      deallocate(observables)
    end if
  end subroutine observables_end


end module observables_m
