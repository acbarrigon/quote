!! Copyright (C) 2014-20xx The quote developing team.
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
program main
  use aotus_module
  use global_m
  use input_m
  use messages_m
  use static_m
  use td_m
  use qoct_m
  use math_m
  use hamiltonian_m
  use observables_m
  use states_m
  use tdfield_m
  
  implicit none
  
  type(hamiltonian_t) :: h

  call messages_header()
  call input_init()
  call global_init()
  call messages_dir()
  call states_mod_init()
  call hamiltonian_init(h)
  call states_set_tm(hamiltonian_get_evec(h), hamiltonian_get_eval(h))
  call hamiltonian_transform_v(h)
  call states_get_initial()
  call observables_init(h)
  call tdfield_mod_init()

  select case ( job )
    case ( job_static )
      call static(h, 'eigen')
    case ( job_td )
      call td(h)
    case ( job_qoct )
      call qoct(h)
    case default
      print *, 'Error: no valid option has been chosen. Abort'
      stop
  end select

  call tdfield_mod_end()
  call observables_end()
  call hamiltonian_end(h)
  call global_end()
  call input_end()  
end program main
