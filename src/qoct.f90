!! Copyright (C) 2014-20xx The quote developing team.
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.




!*******************************************************
!*******************************************************
!                                                      !
!>This module contains the routines needed to perform  !
! the optimization runs.                               !
!                                                      !
!*******************************************************
!******************************************************* 
module qoct_m
  use aotus_module
  use hamiltonian_m
  use global_m
  use input_m
  use math_m
  use target_m
  use parameters_m
  use states_m
  use tdfield_m
  use propagator_m

  implicit none

  private
  public ::              &
    qoct

  include NLOPT_INCLUDE_FILE

  integer :: qoct_maxiter
  real(8) :: qoct_tol_f, qoct_tol_dr, qoct_tol_f_abs, qoct_tol_dr_abs, qoct_penalty, qoct_stopval
  integer :: qoct_algorithm
  logical :: qoct_bounded_optimization


  integer, parameter :: qoct_algorithm_check = -1
  integer, parameter :: qoct_algorithm_nlopt_gn_direct = nlopt_gn_direct
  integer, parameter :: qoct_algorithm_nlopt_gn_direct_l = nlopt_gn_direct_l
  integer, parameter :: qoct_algorithm_nlopt_gn_direct_l_rand = nlopt_gn_direct_l_rand
  integer, parameter :: qoct_algorithm_nlopt_gn_direct_noscal = nlopt_gn_direct_noscal
  integer, parameter :: qoct_algorithm_nlopt_gn_direct_l_noscal = nlopt_gn_direct_l_noscal
  integer, parameter :: qoct_algorithm_nlopt_gn_direct_l_rand_noscal = nlopt_gn_direct_l_rand_noscal
  integer, parameter :: qoct_algorithm_nlopt_gn_orig_direct = nlopt_gn_orig_direct
  integer, parameter :: qoct_algorithm_nlopt_gn_orig_direct_l = nlopt_gn_orig_direct_l
  integer, parameter :: qoct_algorithm_nlopt_gd_stogo = nlopt_gd_stogo
  integer, parameter :: qoct_algorithm_nlopt_gd_stogo_rand = nlopt_gd_stogo_rand
  integer, parameter :: qoct_algorithm_nlopt_ld_lbfgs_nocedal = nlopt_ld_lbfgs_nocedal
  integer, parameter :: qoct_algorithm_nlopt_ld_lbfgs = nlopt_ld_lbfgs
  integer, parameter :: qoct_algorithm_nlopt_ln_praxis = nlopt_ln_praxis
  integer, parameter :: qoct_algorithm_nlopt_ld_var1 = nlopt_ld_var1
  integer, parameter :: qoct_algorithm_nlopt_ld_var2 = nlopt_ld_var2
  integer, parameter :: qoct_algorithm_nlopt_ld_tnewton = nlopt_ld_tnewton
  integer, parameter :: qoct_algorithm_nlopt_ld_tnewton_restart = nlopt_ld_tnewton_restart
  integer, parameter :: qoct_algorithm_nlopt_ld_tnewton_precond = nlopt_ld_tnewton_precond
  integer, parameter :: qoct_algorithm_nlopt_ld_tnewton_precond_restart = nlopt_ld_tnewton_precond_restart
  integer, parameter :: qoct_algorithm_nlopt_gn_crs2_lm = nlopt_gn_crs2_lm
  integer, parameter :: qoct_algorithm_nlopt_gn_mlsl = nlopt_gn_mlsl
  integer, parameter :: qoct_algorithm_nlopt_gd_mlsl = nlopt_gd_mlsl
  integer, parameter :: qoct_algorithm_nlopt_gn_mlsl_lds = nlopt_gn_mlsl_lds
  integer, parameter :: qoct_algorithm_nlopt_gd_mlsl_lds = nlopt_gd_mlsl_lds
  integer, parameter :: qoct_algorithm_nlopt_ld_mma = nlopt_ld_mma
  integer, parameter :: qoct_algorithm_nlopt_ln_cobyla = nlopt_ln_cobyla
  integer, parameter :: qoct_algorithm_nlopt_ln_newuoa = nlopt_ln_newuoa
  integer, parameter :: qoct_algorithm_nlopt_ln_newuoa_bound = nlopt_ln_newuoa_bound
  integer, parameter :: qoct_algorithm_nlopt_ln_neldermead = nlopt_ln_neldermead
  integer, parameter :: qoct_algorithm_nlopt_ln_sbplx = nlopt_ln_sbplx
  integer, parameter :: qoct_algorithm_nlopt_ln_auglag = nlopt_ln_auglag
  integer, parameter :: qoct_algorithm_nlopt_ld_auglag = nlopt_ld_auglag
  integer, parameter :: qoct_algorithm_nlopt_ln_auglag_eq = nlopt_ln_auglag_eq
  integer, parameter :: qoct_algorithm_nlopt_ld_auglag_eq = nlopt_ld_auglag_eq
  integer, parameter :: qoct_algorithm_nlopt_ln_bobyqa = nlopt_ln_bobyqa
  integer, parameter :: qoct_algorithm_nlopt_gn_isres = nlopt_gn_isres
  integer, parameter :: qoct_algorithm_nlopt_auglag = nlopt_auglag
  integer, parameter :: qoct_algorithm_nlopt_auglag_eq = nlopt_auglag_eq
  integer, parameter :: qoct_algorithm_nlopt_g_mlsl = nlopt_g_mlsl
  integer, parameter :: qoct_algorithm_nlopt_g_mlsl_lds = nlopt_g_mlsl_lds
  integer, parameter :: qoct_algorithm_nlopt_ld_slsqp = nlopt_ld_slsqp
  integer, parameter :: qoct_algorithm_nlopt_ld_ccsaq = nlopt_ld_ccsaq
  integer, parameter :: qoct_algorithm_nlopt_gn_esch = nlopt_gn_esch
  integer, parameter :: qoct_algorithm_nlopt_gn_ags = nlopt_gn_ags

  type(string) :: qoct_algorithm_name(0:43)   = &
    [ string(str = 'nlopt_gn_direct'),                            &
      string(str = 'nlopt_gn_direct_l'),                          &
      string(str = 'nlopt_gn_direct_l_rand'),                     &
      string(str = 'nlopt_gn_direct_noscal'),                     &
      string(str = 'nlopt_gn_direct_l_noscal'),                   &
      string(str = 'nlopt_gn_direct_l_rand_noscal'),              &
      string(str = 'nlopt_gn_orig_direct'),                       &
      string(str = 'nlopt_gn_orig_direct_l'),                     &
      string(str = 'nlopt_gd_stogo'),                             &
      string(str = 'nlopt_gd_stogo_rand'),                        &
      string(str = 'nlopt_ld_lbfgs_nocedal'),                     &
      string(str = 'nlopt_ld_lbfgs'),                             &
      string(str = 'nlopt_ln_praxis'),                            &
      string(str = 'nlopt_ld_var1'),                              &
      string(str = 'nlopt_ld_var2'),                              &
      string(str = 'nlopt_ld_tnewton'),                           &
      string(str = 'nlopt_ld_tnewton_restart'),                   &
      string(str = 'nlopt_ld_tnewton_precond'),                   &
      string(str = 'nlopt_ld_tnewton_precond_restart'),           &
      string(str = 'nlopt_gn_crs2_lm'),                           &
      string(str = 'nlopt_gn_mlsl'),                              &
      string(str = 'nlopt_gd_mlsl'),                              &
      string(str = 'nlopt_gn_mlsl_lds'),                          &
      string(str = 'nlopt_gd_mlsl_lds'),                          &
      string(str = 'nlopt_ld_mma'),                               &
      string(str = 'nlopt_ln_cobyla'),                            &
      string(str = 'nlopt_ln_newuoa'),                            &
      string(str = 'nlopt_ln_newuoa_bound'),                      &
      string(str = 'nlopt_ln_neldermead'),                        &
      string(str = 'nlopt_ln_sbplx'),                             &
      string(str = 'nlopt_ln_auglag'),                            &
      string(str = 'nlopt_ld_auglag'),                            &
      string(str = 'nlopt_ln_auglag_eq'),                         &
      string(str = 'nlopt_ld_auglag_eq'),                         &
      string(str = 'nlopt_ln_bobyqa'),                            &
      string(str = 'nlopt_gn_isres'),                             &
      string(str = 'nlopt_auglag'),                               &
      string(str = 'nlopt_auglag_eq'),                            &
      string(str = 'nlopt_g_mlsl'),                               &
      string(str = 'nlopt_g_mlsl_lds'),                           &
      string(str = 'nlopt_ld_slsqp'),                             &
      string(str = 'nlopt_ld_ccsaq'),                             &
      string(str = 'nlopt_gn_esch'),                              &
      string(str = 'nlopt_gn_ags') ]
  
  contains 

  subroutine qoct(h)
    type(hamiltonian_t), intent(in) :: h

    type(tdfield_t), allocatable :: reftdf(:)
    type(tdfield_t), allocatable :: opttdf(:)
    type(parameters_t) :: parameters
    type(propagator_t) :: pr


    integer :: j, ierr, nfield

    call parameters_init(parameters)

    call propagator_init(pr)

    nfield = hamiltonian_nfield(h)
    allocate ( reftdf(1:nfield) )
    allocate ( opttdf(1:nfield) )

    do j = 1, nfield
      call tdfield_init(reftdf(j), maxntime, dt)
      call tdfield_init(opttdf(j), maxntime, dt, rep = tdfield_realtime)
    end do

    qoct_bounded_optimization = tdfield_read_bounds()

    call tdfield_get_initial(reftdf)

    ! This is the field, as specified in the input file, or as read in the file.
    call tdfield_save( reftdf, './output/reftdf0')
    ! We go to and back from the parameters representation.
    if( tdf_rep == TDFIELD_FOURIER .or. tdf_rep == TDFIELD_CONSTRAINED_FOURIER) then
      do j = 1, nfield
        call tdfield_realtime_to_params(reftdf(j))
        call tdfield_params_to_realtime(reftdf(j))
      end do
    end if
    ! This is the field, once it has been forced to be consistent with the parametrization.
    call tdfield_save( reftdf, './output/reftdf1')

    call target_init(h, dim, ierr)
    if(ierr .ne. 0) then
      write(0, *) 'Could not read a target properly in the input file'
      stop
    end if

    call aot_get_val(L = input_file, key = 'qoct_maxiter', val = qoct_maxiter, &
      errcode = input_error, default = 10)
    call aot_get_val(L = input_file, key = 'qoct_tol_f', val = qoct_tol_f, &
      errcode = input_error, default = 1.0e-7_8)
    call aot_get_val(L = input_file, key = 'qoct_tol_dr', val = qoct_tol_dr, &
      errcode = input_error, default = -1.0_8)
    call aot_get_val(L = input_file, key = 'qoct_tol_f_abs', val = qoct_tol_f_abs, &
      errcode = input_error, default = -1.0_8)
    call aot_get_val(L = input_file, key = 'qoct_tol_dr_abs', val = qoct_tol_dr_abs, &
      errcode = input_error, default = -1.0_8)
    call aot_get_val(L = input_file, key = 'qoct_stopval', val = qoct_stopval, &
      errcode = input_error, default = huge(1.0_8))

    call aot_get_val(L = input_file, key = 'qoct_algorithm', val = qoct_algorithm, &
      errcode = input_error, default = QOCT_ALGORITHM_NLOPT_LD_LBFGS)
    call aot_get_val(L = input_file, key = 'qoct_penalty', val = qoct_penalty, &
      errcode = input_error, default = 1.0_8)

    select case(qoct_algorithm)
    case(qoct_algorithm_check)
      call optimal_control_check(pr, h, reftdf)
    case(qoct_algorithm_nlopt_gn_direct:qoct_algorithm_nlopt_gn_ags)
      call optimal_control_nlopt(pr, h, reftdf, opttdf)
      call tdfield_save(opttdf, './output/opttdf')
    case default
      write(*,*) 'Error choosing the qoct_algorithm variable.'
      write(*,*) 'Maybe you are trying to use an algorithm corresponding to a library that has not been linked?'
      stop
    end select


    call parameters_end(parameters)
    call target_end()
    do j = 1, nfield
      call tdfield_end( reftdf(j) )
      call tdfield_end( opttdf(j) )
    end do
    deallocate(reftdf)
    deallocate(opttdf)
  end subroutine qoct

#include "qoct_check_inc.f90"
#include "qoct_nlopt_inc.f90"

  subroutine qoct_function(pr, h, st, tdf, val, grad)
    type(propagator_t), intent(in) :: pr
    type(hamiltonian_t), intent(in) :: h
    type(states_t), intent(inout) :: st(0:maxntime)
    type(tdfield_t), intent(inout) :: tdf(:)
    real(8), intent(out) :: val
    real(8), intent(out), optional :: grad(:)

    integer :: nt, nfield, j, k
    real(8) :: j1, t, gx
    real(8), allocatable :: j2(:)
    type(states_t), allocatable :: stb(:), stout(:)
    type(tdfield_t) :: gtdf

    call cpu_time(prop_time_t0)
    do nt = 0, maxntime - 1
      call propagator_dt(pr, h, st(nt), st(nt+1), nt, tdf, 'forward')
    end do
    call cpu_time(prop_time_tf)
    propagation_time = propagation_time + (prop_time_tf - prop_time_t0)
    nforward_props = nforward_props + 1
    j1 = target_j1(h, st(maxntime))

    nfield = hamiltonian_nfield(h)
    allocate(j2(1:nfield))
    call tdfield_fluence(tdf, j2)
    j2 = - qoct_penalty * j2
    val = - j1 - j2(1)

    if(present(grad)) then

      allocate(stb(0:maxntime))
      allocate(stout(0:maxntime))
      do j = 0, maxntime
        call states_init(stb(j), dim, mode = object)
        call states_init(stout(j), dim, mode = object)
      end do

      call target_calculate_bc(h, stb(maxntime), st(maxntime))
      call cpu_time(prop_time_t0)
      do nt = maxntime, 1, -1
        call propagator_dt(pr,h ,stb(nt), stb(nt-1), nt, tdf ,'backward' )
      end do
      call cpu_time(prop_time_tf)
      bpropagation_time = bpropagation_time + (prop_time_tf - prop_time_t0)
      nbackward_props = nbackward_props + 1

      call cpu_time(grad_t0)
      call tdfield_init(gtdf, maxntime, dt)
      do k = 1, min(tdf_nparams, size(grad))
        call hamiltonian_derivative(h, tdf(1), k, st, stout)
        do nt = 0, maxntime
          gx = 2.0_8 * aimag( states_dotp(stb(nt), stout(nt)) )
          call tdfield_set(gtdf, nt, gx)
        end do
        grad(k) = - tdfield_integral(gtdf) + qoct_penalty * tdfield_dfluence(tdf(1), k)
      end do
      call tdfield_end(gtdf)
      call cpu_time(grad_tf)
      gradient_time = gradient_time + (grad_tf - grad_t0)
      ngradient_calcs = ngradient_calcs + 1

      do j = 0, maxntime
        call states_end(stb(j))
        call states_end(stout(j))
      end do
      deallocate(stb)
      deallocate(stout)

    end if

    deallocate(j2)
  end subroutine qoct_function
  
end module qoct_m
