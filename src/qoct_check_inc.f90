!! Copyright (C) 2014-20xx The quote developing team.
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
  subroutine optimal_control_check(pr, h, reftdf)
    type(propagator_t), intent(in) :: pr
    type(hamiltonian_t), intent(in) :: h
    type(tdfield_t), intent(inout) :: reftdf(:)

    integer :: j, k, nfield, nt, param
    real(8) :: deltah, err, hh, j1, res, val, x
    real(8), allocatable :: j2(:), derfd(:), derfderror(:), derqoct(:)
    type(states_t), allocatable :: st(:)
    type(tdfield_t), allocatable :: tdf(:)

    write(*, *) '******************************************************'
    write(*, *) 'QOCT algorithm: "check"'
    write(*, *) '******************************************************'
    write(*, *)


    call aot_get_val(L = input_file, key = 'qoct_check_nparam', val = param, &
      errcode = input_error, default = 5)
    param = min(param, tdf_nparams)
    write(*, '(a,i5)') ' Parameter(s) to check: 1 - ', param

    nfield = hamiltonian_nfield(h)

    allocate ( tdf(1:nfield) )
    allocate ( j2(1:nfield) )
    allocate ( derfd(1:param) )
    allocate ( derfderror(1:param) )
    allocate ( derqoct(1:param) )

    allocate(st(0:maxntime))
    do j = 0, maxntime
      call states_init(st(j), dim, mode = object)
    end do
    call states_set_initial(st(0))

    do j = 1, nfield
      call tdfield_init(tdf(j), maxntime, dt)
    end do

    do nt = 0, maxntime - 1
      call propagator_dt(pr, h, st(nt), st(nt+1), nt, reftdf, 'forward')
    end do
    j1 = target_j1(h, st(maxntime))
    write(*, '(a,e22.12e3)') ' J1 = ', j1

    call tdfield_fluence(reftdf, j2)
    write(*, '(a,e22.12e3)') ' J2 = ', -qoct_penalty * j2(1)

    call aot_get_val(L = input_file, key = 'qoct_check_fd_delta', val = deltah, &
      errcode = input_error, default = 0.01_8)

    ! Computation of the derivative with finite difference formula.
    do k = 1, param
      call tdfield_copy(tdf, reftdf)
      call tdfield_get_v(reftdf(1), k, x)
      hh = deltah
      call numder(x, hh, res, err, f)
      derfd(k) = res
      derfderror(k) = err
    end do

    ! Now we will compute the gradient through the QOCT formula.
    call qoct_function(pr, h, st, reftdf, val, derqoct)
    derqoct = - derqoct
    val = -val

    write(*, *)
    write(*, '(a)') '#par,       val(gamma),            dg(num)       +/-        error,                 dg(qoct)'
    write(*, '(a)') '================================================================================================='
    do k = 1, param
      call tdfield_get_v(reftdf(1), k, x)
      write(*, '(i4,2e22.12e3,a5,2e22.12e3)') k, x, derfd(k), ' +/- ', derfderror(k), derqoct(k)
    end do
    write(*, '(a)') '================================================================================================='
    write(*, *)
    write(*, *) ' GRAD DIFF (REL) = ', sqrt(dot_product(derfd-derqoct, derfd-derqoct)) / sqrt(dot_product(derfd, derfd))
    write(*, *) ' GRAD DIFF (ABS) = ', sqrt(dot_product(derfd-derqoct, derfd-derqoct))

    do j = 1, nfield
      call tdfield_end(tdf(j))
    end do
    deallocate(tdf)
    deallocate(j2)
    deallocate(derfd)
    deallocate(derfderror)
    deallocate(derqoct)
    do j = 0, maxntime
      call states_end(st(j))
    end do
    deallocate(st)

    contains

    subroutine f(x, fx)
      implicit none
      real(8), intent(in)    :: x
      real(8), intent(inout) :: fx

      call tdfield_set_v(tdf(1), k, x)
      call tdfield_params_to_realtime(tdf(1))
      call qoct_function(pr, h, st, tdf, fx)
      fx = -fx
    end subroutine f

  end subroutine optimal_control_check

