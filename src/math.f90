!! Copyright (C) 2014-20xx The quote developing team.
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
module math_m
  use iso_c_binding
  use fftw_m

  implicit none

  private
  public ::                &
    numder,                &
    gsl_sf_bessel_j0,      &
    oct_minimize,          &
    oct_minimize_simplex,  &
    fft_r2r_1d,            &
    fft_r2c_1d,            &
    fft_c2r_1d,            &
    interpolate_0,         &
    frobenius_dotp,        &
    frobenius_norm,        &
    check_unitarity,       &
    rdelta,                &
    expi,                  &
    eigensolver


  ! Mathematical constants.
  real(8), parameter, public    :: pi  =  3.14159265358979_8, &
                                   dpi =  6.28318530717959_8
  complex(8), parameter, public :: im = cmplx(0.0_8, 1.0_8, kind = 8)
    

  interface
    real(C_DOUBLE) function gsl_sf_bessel_j0(x) bind(C, name = 'gsl_sf_bessel_J0')
      use iso_c_binding
      implicit none
      real(C_DOUBLE), value :: x 
    end function gsl_sf_bessel_j0
  end interface


  interface
    integer(C_INT) function oct_minimize_simplex(dim, x, step, toldr, maxiter, f, write_iter_info, minimum) &
      bind(C, name = 'oct_minimize_simplex')
      use iso_c_binding
      implicit none
      integer(C_INT), intent(in)    :: dim
      real(C_DOUBLE), intent(inout) :: x
      real(C_DOUBLE), intent(in)    :: step
      real(C_DOUBLE), intent(in)    :: toldr
      integer(C_INT), intent(in)    :: maxiter
      interface
        subroutine f(n, x, val) bind(C)
          use iso_c_binding
          implicit none
          integer(C_INT), intent(in)    :: n
          real(C_DOUBLE), intent(in)    :: x(n)
          real(C_DOUBLE), intent(inout) :: val
        end subroutine f
        subroutine write_iter_info(iter, n, val, maxdr, x) bind(C)
          use iso_c_binding
          implicit none
          integer(C_INT), intent(in) :: iter
          integer(C_INT), intent(in) :: n
          real(C_DOUBLE), intent(in) :: val
          real(C_DOUBLE), intent(in) :: maxdr
          real(C_DOUBLE), intent(in) :: x(n)
        end subroutine
      end interface
      real(C_DOUBLE), intent(inout) :: minimum
    end function oct_minimize_simplex
  end interface

  interface
    integer(C_INT) function oct_minimize(method, dim, x, step, line_tol, &
      tolgrad, toldr, maxiter, f, write_iter_info, minimum) bind(C, name = 'oct_minimize')
      use iso_c_binding
      implicit none
      integer(C_INT), intent(in)    :: method
      integer(C_INT), intent(in)    :: dim
      real(C_DOUBLE), intent(inout) :: x
      real(C_DOUBLE), intent(in)    :: step
      real(C_DOUBLE), intent(in)    :: line_tol
      real(C_DOUBLE), intent(in)    :: tolgrad
      real(C_DOUBLE), intent(in)    :: toldr
      integer(C_INT), intent(in)    :: maxiter
      interface
        subroutine f(n, x, val, getgrad, grad) bind(C)
          use iso_c_binding
          implicit none
          integer(C_INT), intent(in)    :: n
          real(C_DOUBLE), intent(in)    :: x(n)
          real(C_DOUBLE), intent(inout) :: val
          integer(C_INT), intent(in)    :: getgrad
          real(C_DOUBLE), intent(inout) :: grad(n)
        end subroutine f
        subroutine write_iter_info(iter, n, val, maxdr, maxgrad, x) bind(C)
          use iso_c_binding
          implicit none
          integer(C_INT), intent(in) :: iter
          integer(C_INT), intent(in) :: n
          real(C_DOUBLE), intent(in) :: val
          real(C_DOUBLE), intent(in) :: maxdr
          real(C_DOUBLE), intent(in) :: maxgrad
          real(C_DOUBLE), intent(in) :: x(n)
        end subroutine write_iter_info
      end interface
      real(C_DOUBLE), intent(out)   :: minimum
    end function oct_minimize
  end interface

  contains


  ! This is the Frobenius dot product, normalized by the dimension n:
  ! a*b = (1/n) * Trace[ a^\dagger b ]
  ! The normalization implies that if a is unitary, a*a=1.
  complex(8) function frobenius_dotp(a, b) result(dotp)
    complex(8), intent(in) :: a(:, :), b(:, :)
    complex(8), allocatable :: c(:, :)
    integer :: i, n
    n = size(a, 1)
    allocate(c(n, n))
    c = matmul(conjg(transpose(a)), b)
    dotp = (0.0_8, 0.0_8)
    do i = 1, n
      dotp = dotp + c(i, i)
    end do
    dotp = dotp / n
    deallocate(c)
  end function frobenius_dotp


  real(8) function frobenius_norm(a) result(nrm)
    complex(8), intent(in) :: a(:, :)
    nrm = sqrt(real(frobenius_dotp(a,a), 8))
  end function frobenius_norm


  logical function check_unitarity(a, epsilon) result(l)
    complex(8), intent(in) :: a(:, :)
    real(8), intent(in) :: epsilon

    complex(8), allocatable :: unit_matrix(:, :)
    integer :: n, i
    n = size(a, 1)
    allocate(unit_matrix(n, n))
    unit_matrix = (0.0_8, 0.0_8)
    forall(i = 1:n) unit_matrix(i, i) = (1.0_8, 0.0_8)
    l = frobenius_norm( matmul(conjg(transpose(a)), a) - unit_matrix ) < epsilon
    deallocate(unit_matrix)
  end function check_unitarity


  subroutine interpolation_coefficients(nn, xa, xx, cc)
    integer, intent(in)    :: nn    !< the number of points and coefficients
    real(8),   intent(in)  :: xa(:) !< the nn points where we know the function
    real(8),   intent(in)  :: xx    !< the point where we want the function
    real(8),   intent(out) :: cc(:) !< the coefficients

    integer :: ii, kk

    ! no push_sub, called too frequently
    do ii = 1, nn
      cc(ii) = 1.0_8
      do kk = 1, nn
        if(kk  ==  ii) cycle
        cc(ii) = cc(ii)*(xx - xa(kk))/(xa(ii) - xa(kk))
      end do
    end do

  end subroutine interpolation_coefficients


  subroutine interpolate_0(xa, ya, x, y)
    real(8), intent(in)  :: xa(:)
    real(8), intent(in)  :: ya(:)
    real(8), intent(in)  :: x
    real(8), intent(out) :: y

    integer :: n, i
    real(8), allocatable :: c(:)

    ! no push_sub, called too frequently

    n = size(xa)
    allocate(c(1:n))

    call interpolation_coefficients(n, xa, x, c)

    y = 0.0_8
    do i = 1, n
      y = y + c(i)*ya(i)
    end do

    deallocate(c)
  end subroutine interpolate_0


  subroutine fft_r2r_1d(in, out, size, dir)
   real(C_DOUBLE), intent(inout) :: in(:), out(:)
   integer(C_INT), intent(in)    :: size
   integer, intent(in)           :: dir
  
   type(C_PTR) :: plan
   integer(C_FFTW_R2R_KIND) :: kind

   if(dir < 0) then
     kind = FFTW_R2HC
   elseif(dir > 0) then
     kind = FFTW_HC2R
   else
     return
   end if
   plan = fftw_plan_r2r_1d(size, in, out, kind, FFTW_ESTIMATE)
   call fftw_execute_r2r(plan, in, out)
   call fftw_destroy_plan(plan)

  end subroutine fft_r2r_1d


  subroutine fft_r2c_1d(in, zout, size)
   real(C_DOUBLE), intent(inout) :: in(:)
   complex(C_DOUBLE_COMPLEX), intent(inout) :: zout(:)
   integer(C_INT), intent(in)    :: size
  
   type(C_PTR) :: plan

   plan = fftw_plan_dft_r2c_1d(size, in, zout, FFTW_ESTIMATE)
   call fftw_execute_dft_r2c(plan, in, zout)
   call fftw_destroy_plan(plan)

  end subroutine fft_r2c_1d

  subroutine fft_c2r_1d(zin, out, size)
   complex(C_DOUBLE_COMPLEX), intent(inout) :: zin(:)
   real(C_DOUBLE), intent(inout) :: out(:)
   integer(C_INT), intent(in)    :: size
  
   type(C_PTR) :: plan

   plan = fftw_plan_dft_c2r_1d(size, zin, out, FFTW_ESTIMATE)
   call fftw_execute_dft_c2r(plan, zin, out)
   call fftw_destroy_plan(plan)

  end subroutine fft_c2r_1d


  subroutine numder(x, h, res, err, f, ndiv)
    real(8), intent(in)  :: x, h
    real(8), intent(out) :: res, err
    interface
      subroutine f(x, fx)
        implicit none
        real(8), intent(in)    :: x
        real(8), intent(inout) :: fx
      end subroutine f
    end interface
    integer, optional, intent(in) :: ndiv

    real(8), parameter :: redux = 1.4_8, &
                          redux2 = redux*redux,  &
                          termination_factor = 2.0_8
    integer :: ndiv_
    integer :: i, j
    real(8) :: newerr, fac, hh, fx1, fx2
    real(8), allocatable :: a(:, :)

    if(h .eq. 0.0_8) stop "h must be nonzero in loct_numerical_derivative2"

    if(present(ndiv)) then
      ndiv_ = ndiv
    else
      ndiv_ = 10
    end if
    allocate(a(ndiv_, ndiv_))

    hh = h
    call f(x+hh, fx1); call f(x-hh, fx2)
    a(1,1) = (fx1-fx2) /(2.0_8*hh)

    err = 1.0e20_8
    do i = 2, ndiv_
      hh = hh / redux
      call f(x+hh, fx1)
      call f(x-hh, fx2)
      a(1,i) = (fx1-fx2) / (2.0_8*hh)
      fac = redux2
      do j = 2, i
        a(j,i) = (a(j-1,i)*fac-a(j-1,i-1)) / (fac-1.0_8)
        fac = redux2*fac
        newerr = max(abs(a(j,i)-a(j-1,i)),abs(a(j,i)-a(j-1,i-1)))
        if (newerr .le. err) then
          err = newerr
          res = a(j,i)
        end if
      end do
      if (abs(a(i,i)-a(i-1,i-1)) .ge. termination_factor*err) return
    end do
    deallocate(a)

  end subroutine numder


  real(8) function rdelta(i,j)
    integer  , intent (in   )  ::  i, j
    if (i.EQ.j) rdelta = 1.0_8
    if (i.NE.j) rdelta = 0.0_8
  end function rdelta


  complex(8) function expi(arg)
    real(8), intent (in) :: arg
    expi = cmplx( cos(arg) , sin(arg), kind = 8 )
  end function expi


  subroutine eigensolver( hdim , h0 , eval , evec )
    integer, intent (in) :: hdim
    complex(8), intent (in) :: h0(1:hdim, 1:hdim)
    real(8), intent (out) :: eval(1:hdim)
    complex(8), intent (out) :: evec(1:hdim, 1:hdim)

    integer :: info
    complex(8), allocatable :: wrk(:)
    real(8), allocatable :: rwrk(:)
    integer :: lwrk

    evec = h0
    eval = 0.0_8
    lwrk = 2*hdim-1
    allocate(wrk(lwrk))
    allocate(rwrk(3*hdim-2))
    call zheev( 'V', 'U', hdim, evec, hdim, eval, wrk, lwrk, rwrk, info)

    deallocate(rwrk)
    deallocate(wrk)
  end subroutine eigensolver


end module math_m
