!! Copyright (C) 2014-20xx The quote developing team.
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
module input_m
  use aotus_module

  implicit none

  private
  public ::            &
    input_file,        &
    input_error,       &
    input_error_array, &
    input_init,        &
    input_end

  public ::            &
    aot_get_zval

  integer :: input_error
  integer, allocatable :: input_error_array(:)
  type(flu_state) :: input_file

  contains

  subroutine aot_get_zval(val, ErrCode, L, key, default)
    type(flu_State) :: L !! Handle to the lua script
    complex(8), intent(inout) :: val(:)
    integer, intent(out) :: errCode(:)
    character(len=*), intent(in), optional :: key
    complex(8), intent(in), optional :: default(:)

    real(8), allocatable :: rval(:), rdefault(:)
    integer, allocatable :: rerrcode(:)
    integer :: j, dim

    dim = size(val)

    allocate(rval(2*dim))
    allocate(rerrcode(2*dim))
    if(present(default)) then
      allocate(rdefault(2*dim))
      do j = 1, dim
        rdefault(2*j-1) = real (default(j))
        rdefault(2*j)   = aimag(default(j))
      end do
    end if

    call aot_get_val(L = L, key = key, val = rval, errcode = rerrcode, default = rdefault)

    do j = 1, dim
      val(j) = rval(2*j-1) + (0.0_8, 1.0_8) * rval(2*j)
      errcode = rerrcode(2*j-1)
    end do

    deallocate(rerrcode)
    deallocate(rval)
    if(present(default)) then
      deallocate(rdefault)
    end if

  end subroutine aot_get_zval

  subroutine input_init
    open(unit = 15, file = 'parser.log')

    call open_config_file(L = input_file, &
      filename = INSTALL_PREFIX//'/share/quote/definitions.lua', &
      ErrCode = input_error)
    if (input_error /= 0) then
      write(*,*) 'FATAL Error when opening '//INSTALL_PREFIX//'/share/quote/definitions.lua file'
      STOP
    end if

    call open_config_file(L = input_file, filename = 'input.lua', ErrCode = input_error)
    if (input_error /= 0) then
      write(*,*) 'FATAL Error when opening the input file'
      STOP
    end if

  end subroutine input_init


  subroutine input_end
    close(unit = 15)
    call close_config(input_file)
  end subroutine input_end


end module input_m
