!! Copyright (C) 2014-20xx The quote developing team.
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
module parameters_m
  use tdfield_m

  private
  public ::          &
    parameters_t,    &
    parameters_init, &
    parameters_end

  integer, parameter :: &
    PARAMETERS_IS_TDFIELD = 0

  type parameters_t
    private
    integer :: type = -1
    integer :: nparameters
    real(8), allocatable :: u(:)
  end type parameters_t

  contains

  subroutine parameters_init(p)
    type(parameters_t), intent(inout) :: p

    p%type = PARAMETERS_IS_TDFIELD

    select case(p%type)
    case(PARAMETERS_IS_TDFIELD)
      allocate(p%u(tdf_nparams))

    end select

  end subroutine parameters_init

  subroutine parameters_end(p)
    type(parameters_t), intent(inout) :: p

    if(allocated(p%u)) deallocate(p%u)

  end subroutine parameters_end


end module parameters_m
