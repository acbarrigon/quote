!! Copyright (C) 2014-20xx The quote developing team.
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.

module propagator_m
  use aotus_module
  use global_m
  use hamiltonian_m
  use input_m
  use math_m
  use states_m
  use tdfield_m

  implicit none

  private
  public ::          &
    propagator_t,    &
    propagator_init, &
    propagator_dt,   &
    propagator_full

  integer, parameter, public ::      &
    propagator_taylor_expansion = 2, &
    propagator_rungekutta       = 3, &
    propagator_cfmagnus4        = 4

  type propagator_t
    integer :: type
  end type propagator_t


  contains


  subroutine propagator_init(pr)
    type(propagator_t), intent(inout) :: pr

    call aot_get_val(L = input_file, key = 'propagator', val = pr%type, &
      errcode = input_error, default = propagator_taylor_expansion)
    write(*, *)
    write(*, *) '******************************************************'
    select case(pr%type)
    case(propagator_taylor_expansion)
      write(*, *) 'Propagator: Taylor expansion'
    case(propagator_rungekutta)
      write(*, *) 'Propagator: Runge-Kutta'
    case(propagator_cfmagnus4)
      write(*, *) 'Propagator: CF Magnus (4th order)'
    end select
    write(*, *) '******************************************************'
    write(*, *)

  end subroutine propagator_init


  subroutine propagator_dt(pr, h, st, stdt, nt, tdf, dir)
    type(propagator_t), intent(in) :: pr
    type(hamiltonian_t), intent(in), target :: h
    type(states_t), intent(in) :: st
    type(states_t), intent(inout) :: stdt
    integer, intent(in) :: nt
    type(tdfield_t), intent(inout), target :: tdf(:)
    character (len=*), intent (in) :: dir

    integer :: nfield, j, eps, i
    integer, parameter :: taylor_order = 4
    real(8) :: fac, val, t, t2, tdt, alpha1, alpha2, t1, c1, c2
    type(operator_t) :: h0, ht_, htdt_, hthalf_
    type(operator_t), allocatable :: vt_(:), vthalf_(:), vtdt_(:), vt1(:), vt2(:)
    type(states_t) :: sttdt, sttemp, k1, k2, k3, k4, k_

    nfield = hamiltonian_nfield(h)
    h0 = hamiltonian_h0(h)
    allocate(vt_(nfield))
    do j = 1, nfield
      vt_(j) = hamiltonian_v(h, j)
    end do

    select case(dir)
      case('forward');  eps = 1
      case('backward'); eps = -1
    end select

    select case(pr%type)

    case(propagator_taylor_expansion)

      call operator_init(ht_, dim, mode = object)
      t = nt * dt
      t2 = (nt + eps*0.5_8)*dt

      select case(picture)
      case(interaction_picture)
        do j = 1, nfield
          call operator_to_interaction(vt_(j), t2)
        end do
      case(schrodinger_picture)
        ht_ = h0
      end select

      do j = 1, nfield
        call tdfield_get(tdf(j), t2, val)
        call operator_zaxpy(cmplx(val, 0.0_8, 8), vt_(j), ht_)
      end do

      sttdt = st
      sttemp = st
      fac = 1.0_8
      do j = 1, taylor_order
        call states_zgemv_(-im*eps*dt, ht_, sttemp, cmplx(0.0_8, 0.0_8, 8), sttemp)
        fac = fac/real(j, 8)
        call states_zaxpy( cmplx(fac, 0.0_8, 8), sttemp, sttdt)
      end do
      stdt = sttdt

      call operator_end(ht_)

    case(propagator_rungekutta)

      t = nt * dt
      t2 = (nt + eps*0.5_8)*dt
      tdt = (nt + eps)*dt

      allocate(vthalf_(nfield))
      allocate(vtdt_(nfield))
      do j = 1, nfield
        vthalf_(j) = hamiltonian_v(h, j)
        vtdt_(j) = hamiltonian_v(h, j)
      end do
      call operator_init(ht_, dim, mode = object)
      call operator_init(hthalf_, dim, mode = object)
      call operator_init(htdt_, dim, mode = object)

      select case(picture)

      case(schrodinger_picture)
        ht_ = h0
        hthalf_ = h0
        htdt_ = h0
      case(interaction_picture)
        do j = 1, nfield
          call operator_to_interaction(vt_(j), t)
          call operator_to_interaction(vthalf_(j), t2)
          call operator_to_interaction(vtdt_(j), tdt)
        end do
      end select

      do j = 1, nfield
        call tdfield_get(tdf(j), t, val)
        call operator_zaxpy(cmplx(val, 0.0_8, 8), vt_(j), ht_)
        call tdfield_get(tdf(j), t2, val)
        call operator_zaxpy(cmplx(val, 0.0_8, 8), vthalf_(j), hthalf_)
        call tdfield_get(tdf(j), tdt, val)
        call operator_zaxpy(cmplx(val, 0.0_8, 8), vtdt_(j), htdt_)
      end do

      k1 = st
      k2 = st
      k3 = st
      k4 = st

      call states_zgemv_( -im * eps, ht_, st, cmplx(0.0_8, 0.0_8, 8), k1)
      k_ = st
      call states_zaxpy( cmplx(0.5_8*dt, 0.0_8, 8), k1, k_)
      call states_zgemv_( -im * eps, hthalf_, k_, cmplx(0.0_8, 0.0_8, 8), k2)
      k_ = st
      call states_zaxpy( cmplx(0.5_8*dt, 0.0_8, 8), k2, k_)
      call states_zgemv_( -im * eps, hthalf_, k_, cmplx(0.0_8, 0.0_8, 8), k3)
      k_ = st
      call states_zaxpy( cmplx(dt, 0.0_8, 8), k3, k_)
      call states_zgemv_( -im * eps, htdt_, k_, cmplx(0.0_8, 0.0_8, 8), k4)

      stdt = st
      call states_zaxpy( cmplx(dt/6.0_8, 0.0_8, 8), k1, stdt)
      call states_zaxpy( cmplx(dt/3.0_8, 0.0_8, 8), k2, stdt)
      call states_zaxpy( cmplx(dt/3.0_8, 0.0_8, 8), k3, stdt)
      call states_zaxpy( cmplx(dt/6.0_8, 0.0_8, 8), k4, stdt)

      do j = 1, nfield
        call operator_end(vthalf_(j))
        call operator_end(vtdt_(j))
      end do
      deallocate(vthalf_)
      deallocate(vtdt_)
      call operator_end(ht_)
      call operator_end(hthalf_)
      call operator_end(htdt_)

    case(propagator_cfmagnus4)

      allocate(vt1(nfield))
      allocate(vt2(nfield))
      do j = 1, nfield
        vt1(j) = hamiltonian_v(h, j)
        vt2(j) = hamiltonian_v(h, j)
      end do

      alpha1 = (3.0_8 - 2.0_8*sqrt(3.0_8))/12.0_8
      alpha2 = (3.0_8 + 2.0_8*sqrt(3.0_8))/12.0_8
      c1 = 0.5_8 - sqrt(3.0_8)/6.0_8
      c2 = 0.5_8 + sqrt(3.0_8)/6.0_8
      t1 = nt * dt + c1 * dt
      t2 = nt * dt + c2 * dt

      call operator_init(ht_, dim, mode = object)
      t = nt * dt

      select case(picture)
      case(interaction_picture)
        do j = 1, nfield
          call operator_to_interaction(vt1(j), t1)
          call operator_to_interaction(vt2(j), t2)
        end do
      case(schrodinger_picture)
        ht_ = h0
      end select

      do j = 1, nfield
        call tdfield_get(tdf(j), t1, val)
        call operator_zaxpy(cmplx(2.0_8 * alpha2 * val, 0.0_8, 8), vt1(j), ht_)
        call tdfield_get(tdf(j), t2, val)
        call operator_zaxpy(cmplx(2.0_8 * alpha1 * val, 0.0_8, 8), vt2(j), ht_)
      end do

      sttdt = st
      sttemp = st
      fac = 1.0_8
      do j = 1, taylor_order
        call states_zgemv_(-im*eps*dt*0.5_8, ht_, sttemp, cmplx(0.0_8, 0.0_8, 8), sttemp)
        fac = fac/real(j, 8)
        call states_zaxpy( cmplx(fac, 0.0_8, 8), sttemp, sttdt)
      end do
      stdt = sttdt

      select case(picture)
      case(interaction_picture)
        call operator_init(ht_, dim, mode = object)
      case(schrodinger_picture)
        ht_ = h0
      end select

      do j = 1, nfield
        call tdfield_get(tdf(j), t1, val)
        call operator_zaxpy(cmplx(2.0_8 * alpha1 * val, 0.0_8, 8), vt1(j), ht_)
        call tdfield_get(tdf(j), t2, val)
        call operator_zaxpy(cmplx(2.0_8 * alpha2 * val, 0.0_8, 8), vt2(j), ht_)
      end do

      sttdt = stdt
      sttemp = stdt
      fac = 1.0_8
      do j = 1, taylor_order
        call states_zgemv_(-im*eps*dt*0.5_8, ht_, sttemp, cmplx(0.0_8, 0.0_8, 8), sttemp)
        fac = fac/real(j, 8)
        call states_zaxpy( cmplx(fac, 0.0_8, 8), sttemp, sttdt)
      end do
      stdt = sttdt

      call operator_end(ht_)
      do j = 1, nfield
        call operator_end(vt1(j))
        call operator_end(vt2(j))
      end do
      deallocate(vt1)
      deallocate(vt2)

    case default
      stop 'ERROR'
    end select

    do j = 1, nfield
      call operator_end(vt_(j))
    end do
    deallocate(vt_)
    call operator_end(h0)
  end subroutine propagator_dt


  subroutine propagator_full(pr, h , st , tdf)
    type(propagator_t), intent(in) :: pr
    type(hamiltonian_t), intent(in), target :: h
    type(states_t), intent(inout) :: st(0:maxntime)
    type(tdfield_t), intent(inout), target :: tdf(:)

    real(8) :: t1, t2
    integer :: nt

    write(*, *)
    write(*, *) '******************************************************'
    write(*, *) 'Performing full propagation...'
    call cpu_time(t1)
    do nt = 0, maxntime - 1
      call propagator_dt(pr, h, st(nt), st(nt+1), nt, tdf, 'forward')
    end do
    call cpu_time(t2)
    write(*, *) 'Done.'
    write(*, '(a,f12.4,a)') ' Total propagation time: ', t2-t1, ' seconds.'
    write(*, *) '******************************************************'
    write(*, *)

  end subroutine propagator_full


end module propagator_m
