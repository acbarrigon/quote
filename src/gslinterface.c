
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_min.h>
#include <gsl/gsl_multimin.h>

/* A. First, the interface to the gsl function that calculates the minimum
   of an N-dimensional function, with the knowledge of the function itself
   and its gradient. */

/* This is a type used to communicate with Fortran; func_d is the type of the
   interface to a Fortran subroutine that calculates the function and its
   gradient. */
typedef void (*func_d)(const int*, const double*, double*, const int*, double*);
typedef struct{
  func_d func;
} param_fdf_t;

/* Compute both f and df together. */
static void
my_fdf (const gsl_vector *v, void *params, double *f, gsl_vector *df)
{
  double *x, *gradient, ff2;
  int i, dim, getgrad;
  param_fdf_t * p;

  p = (param_fdf_t *) params;
 
  dim = v->size;
  x = (double *)malloc(dim*sizeof(double));
  gradient = (double *)malloc(dim*sizeof(double));

  for(i=0; i<dim; i++) x[i] = gsl_vector_get(v, i);
  getgrad = (df == NULL) ? 0 : 1;

  p->func(&dim, x, &ff2, &getgrad, gradient);

  if(f != NULL)
    *f = ff2;
      
  if(df != NULL){
    for(i=0; i<dim; i++)
      gsl_vector_set(df, i, gradient[i]);
  }

  free(x); free(gradient);
}

static double
my_f (const gsl_vector *v, void *params)
{
  double val;

  my_fdf(v, params, &val, NULL);
  return val;
}

/* The gradient of f, df = (df/dx, df/dy). */
static void
my_df (const gsl_vector *v, void *params, gsl_vector *df)
{
  my_fdf(v, params, NULL, df);
}

typedef void (*print_f_ptr)(const int*, const int*, const double*, const double*, const double*, const double*);

int oct_minimize
     (const int *method, const int *dim, double *point, const double *step, const double *line_tol, 
      const double *tolgrad, const double *toldr, const int *maxiter, func_d f, 
      const print_f_ptr write_info, double *minimum)
{
  int iter = 0;
  int status;
  double maxgrad, maxdr;
  int i;
  double * oldpoint;
  double * grad;

  const gsl_multimin_fdfminimizer_type *T = NULL;
  gsl_multimin_fdfminimizer *s;
  gsl_vector *x;
  gsl_vector *absgrad, *absdr;
  gsl_multimin_function_fdf my_func;

  param_fdf_t p;

  p.func = f;

  oldpoint = (double *) malloc(*dim * sizeof(double));
  grad     = (double *) malloc(*dim * sizeof(double));

  my_func.f = &my_f;
  my_func.df = &my_df;
  my_func.fdf = &my_fdf;
  my_func.n = *dim;
  my_func.params = (void *) &p;

  /* Starting point */
  x = gsl_vector_alloc (*dim);
  for(i=0; i<*dim; i++) gsl_vector_set (x, i, point[i]);

  /* Allocate space for the gradient */
  absgrad = gsl_vector_alloc (*dim);
  absdr = gsl_vector_alloc (*dim);

  //GSL recommends line_tol = 0.1;
  switch(*method){
  case 1: 
    T = gsl_multimin_fdfminimizer_steepest_descent;
    break;
  case 2: 
    T = gsl_multimin_fdfminimizer_conjugate_fr;
    break;
  case 3: 
    T = gsl_multimin_fdfminimizer_conjugate_pr;
    break;
  case 4: 
    T = gsl_multimin_fdfminimizer_vector_bfgs;
    break;
  case 5: 
    T = gsl_multimin_fdfminimizer_vector_bfgs2;
    break;
  }

  s = gsl_multimin_fdfminimizer_alloc (T, *dim);

  gsl_multimin_fdfminimizer_set (s, &my_func, x, *step, *line_tol);
  do
    {
      iter++;
      for(i=0; i<*dim; i++) oldpoint[i] = point[i];

      /* Iterate */
      status = gsl_multimin_fdfminimizer_iterate (s);

      /* Get current minimum, point and gradient */
      *minimum = gsl_multimin_fdfminimizer_minimum(s);
      for(i=0; i<*dim; i++) point[i] = gsl_vector_get(gsl_multimin_fdfminimizer_x(s), i);
      /* for(i=0; i<*dim; i++) grad[i] = gsl_vector_get(gsl_multimin_fdfminimizer_gradient(s), i); */
      maxgrad = 0.0;
      for(i=0; i<*dim; i++) {
        grad[i] = gsl_vector_get(gsl_multimin_fdfminimizer_gradient(s), i); 
        maxgrad = maxgrad + grad[i]*grad[i];
      }
      maxgrad = sqrt(maxgrad);

      /* Compute convergence criteria */
      for(i=0; i<*dim; i++) gsl_vector_set(absdr, i, fabs(point[i]-oldpoint[i]));
      maxdr = gsl_vector_max(absdr);
      /*
      for(i=0; i<*dim; i++) gsl_vector_set(absgrad, i, fabs(grad[i]));
      maxgrad = gsl_vector_max(absgrad);
      */

      /* Print information */
      write_info(&iter, dim, minimum, &maxdr, &maxgrad, point);
      
      /* Store infomation for next iteration */
      for(i=0; i<*dim; i++) oldpoint[i] = point[i];

      if (status){
        printf("\nGSL minimization stopped. Reason: %s\n", gsl_strerror(status));
        break;
      }

      if ( (maxgrad <= *tolgrad) || (maxdr <= *toldr) ) status = GSL_SUCCESS;
      else status = GSL_CONTINUE;
    }
  while (status == GSL_CONTINUE && iter < *maxiter);

  if(status == GSL_CONTINUE) status = 1025;

  gsl_multimin_fdfminimizer_free (s);
  gsl_vector_free (x); gsl_vector_free(absgrad); gsl_vector_free(absdr);

  free(oldpoint);
  free(grad);

  return status;
}




typedef void (*func_s_d)(const int*, const double*, double*);
typedef struct{
  func_s_d func;
} param_f_t;

double simplex_function(const gsl_vector *v, void *params){
  int dim, i;
  double *x;
  double val;
  param_f_t * p;

  p = (param_f_t *) params;

  dim = v->size;
  x = (double *)malloc(dim*sizeof(double));
  for(i=0; i<dim; i++) x[i] = gsl_vector_get(v, i);

  p->func(&dim, x, &val);

  free(x);
  return val;
}

typedef void (*print_sf_ptr)(const int*, const int*, const double*, const double*, const double*);

int oct_minimize_simplex
(int *dim, double *point, double *step, double *toldr, int *maxiter, func_s_d f, print_sf_ptr write_info, double *minimum)
{
  int status, iter, i;
  double size;
  const gsl_multimin_fminimizer_type *T;
  gsl_vector *x, *ss;
  gsl_multimin_function my_func;
  gsl_multimin_fminimizer * s = NULL;
  param_f_t p;

  T = gsl_multimin_fminimizer_nmsimplex2;

  x = gsl_vector_alloc(*dim);
  for(i=0; i<*dim; i++) gsl_vector_set (x, i, point[i]);

  ss = gsl_vector_alloc(*dim);
  gsl_vector_set_all(ss, *step);

  p.func = f;

  my_func.n = *dim;
  my_func.f = simplex_function;
  my_func.params = (void *) &p;

  s = gsl_multimin_fminimizer_alloc(T, *dim);
  gsl_multimin_fminimizer_set (s, &my_func, x, ss);

  iter = 0;

  do
    {
      iter++;
      status = gsl_multimin_fminimizer_iterate(s);

      if(status){
        printf("\nGSL minimization stopped. Reason: %s\n", gsl_strerror(status));
        break;
      }

      /* Get current minimum */
      *minimum = gsl_multimin_fminimizer_minimum(s);
      for(i=0; i<*dim; i++) point[i] = gsl_vector_get(gsl_multimin_fminimizer_x(s), i);

      size = gsl_multimin_fminimizer_size (s);
      status = gsl_multimin_test_size (size, *toldr);

      if (status == GSL_SUCCESS){printf("converged\n");}

      /* Print information */
      write_info(&iter, dim, minimum, &size, point);

    }
  while(status == GSL_CONTINUE && iter < *maxiter);

  if(status == GSL_CONTINUE) status = 1025;

  gsl_multimin_fminimizer_free(s);
  gsl_vector_free(ss);
  gsl_vector_free(x);

  return status;
}


