!! Copyright (C) 2014-20xx The quote developing team.
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.




!*******************************************************
!*******************************************************
!                                                      !
!>This module contains the routines needed to perform  !
! the optimization runs.                               !
!                                                      !
!*******************************************************
!******************************************************* 
module static_m
  use hamiltonian_m

  implicit none

  private
  public :: static

  contains

  subroutine static(h, filename)
    type(hamiltonian_t), intent(in) :: h
    character(len=*), intent(in) :: filename

    call hamiltonian_print(h, trim(filename))

  end subroutine static


end module static_m
