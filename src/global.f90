!! Copyright (C) 2014-20xx The quote developing team.
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
module global_m
  use iso_c_binding
  use aotus_module
  use input_m
  use math_m

  implicit none

  integer :: job
  integer, parameter :: job_static = 0, &
                        job_td     = 1, &
                        job_qoct   = 2


  integer :: dim, maxntime, maxnfreq
  real(8) :: maxtime, dt, dw, maxfreq

  integer :: picture
  integer, parameter :: schrodinger_picture = 0, &
                        interaction_picture = 1

  type string
    character(len=100) :: str
  end type string

  interface
    subroutine catch_signal() bind(C, name = 'catch_signal')
      use iso_c_binding
    end subroutine catch_signal
  end interface

  integer :: nforward_props, &
             nbackward_props, &
             ngradient_calcs
  real(8) :: propagation_time, &
             bpropagation_time, &
             gradient_time, &
             grad_t0, grad_tf, &
             prop_time_t0, prop_time_tf, &
             total_time_t0, total_time_tf

  contains

  subroutine global_init()

    call catch_signal()

    ! First, read what kind of job is to be done.
    call aot_get_val(L = input_file, key = 'job', val = job, &
      errcode = input_error, default = job_td)
      write(*, *)
      write(*, *) '******************************************************'
      select case ( job )
        case ( job_qoct )
          write(*, *) 'Selected job: optimal control of a selected target'
        case ( job_td )
          write(*, *) 'Selected job: single time-propagation'
        case ( job_static )
          write(*, *) 'Selected job: diagonalization'
        case default
      end select
      write(*, *) '******************************************************'
      write(*, *)

    ! The propagating interval.
    call aot_get_val(L = input_file, key = 'maxtime', val = maxtime, &
      errcode = input_error, default = 10.0_8 * pi)
    call aot_get_val(L = input_file, key = 'maxntime', val = maxntime, &
      errcode = input_error, default = 1000)
    if(mod(maxntime, 2).ne.0) then
      write(*, *) 'maxntime must be even.'
      stop
    end if
    dt = maxtime / maxntime
    dw = dpi/maxtime
    maxfreq = dpi/dt
    maxnfreq = int( maxfreq/dw ) + 1 !Number of grid points in freq-domain
      write(*, *)
      write(*, *) '******************************************************'
      write(*, *) 'Total propagation time (T) = ', maxtime
      write(*, *) 'Number of time steps = ', maxntime
      write(*, *) 'dt = ', dt
      write(*, *) 'max freq = ', maxfreq
      write(*, *) 'dw = ', dw
      write(*, *) 'maxnfreq = ', maxnfreq
      write(*, *) '******************************************************'
      write(*, *)

    ! Should the calculation be done in the Schrödinger or in the interaction picture?
    call aot_get_val(L = input_file, key = 'picture', val = picture, &
      errcode = input_error, default = interaction_picture)
      write(*, *)
      write(*, *) '******************************************************'
      select case(picture)
        case (interaction_picture)
          write(*, *) 'Transformation to the interaction picture.'
        case (schrodinger_picture)
          write(*, *) 'No transformation to the interaction picture.'
      end select
      write(*, *) '******************************************************'
      write(*, *)

    call aot_get_val(L = input_file, key = 'dim', val = dim, errcode = input_error, default = 64)

    nforward_props = 0
    nbackward_props = 0
    ngradient_calcs = 0
    propagation_time = 0.0_8
    bpropagation_time = 0.0_8
    gradient_time = 0.0_8
    call cpu_time(total_time_t0)
  end subroutine global_init


  subroutine global_end
    call cpu_time(total_time_tf)

    write(*, '(a)')
    write(*, '(a)')       'Stats: '
    write(*, '(a,i8)')    '   # Forward propagations : ', nforward_props
    write(*, '(a,f15.5)') '   Average wall time for forward propagations: ', propagation_time / nforward_props
    write(*, '(a,i8)')    '   # Backward propagations : ', nbackward_props
    write(*, '(a,f15.5)') '   Average wall time for backward propagations: ', bpropagation_time / nbackward_props
    write(*, '(a,i8)')    '   # Gradient calculations : ', ngradient_calcs
    write(*, '(a,f15.5)') '   Average wall time for backward propagations: ', gradient_time / ngradient_calcs
    write(*, '(a,f15.5)') '   Total run time : ', total_time_tf - total_time_t0

    write(*, '(a)')
  end subroutine global_end


  ! returns true if a file named stop exists
  function clean_stop()
    logical :: clean_stop, file_exists

    clean_stop = .false.
    inquire(file='stop', exist=file_exists)
    if(file_exists) then
      open(unit = 11, file = 'stop', status = 'old')
      close(unit = 11, status = 'delete')
      clean_stop = .true.
    end if
  end function clean_stop

  
end module global_m
