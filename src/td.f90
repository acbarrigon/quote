!! Copyright (C) 2014-20xx The quote developing team.
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
module td_m
  use global_m
  use tdfield_m
  use hamiltonian_m  
  use observables_m
  use input_m
  use output_m
  use propagator_m
  use states_m
  use target_m
  use, intrinsic :: iso_c_binding


  implicit none

  private
  public ::                      &
    td


  integer :: counter_nops ! This should hold the number of operator-state operations.

  contains
  
  subroutine td(h)
    type(hamiltonian_t), intent(inout) :: h

    type(propagator_t) :: pr
    integer :: nfield, i, j, ierr
    real(8) :: j1
    real(8), allocatable :: fluence(:)
    type(tdfield_t), allocatable :: tdf(:)
    type(states_t), allocatable :: st(:)
    type(states_T) :: finalst
    
    call propagator_init(pr)

    nfield = hamiltonian_nfield(h)
    allocate ( tdf(1:nfield) )
    do j = 1, nfield
      call tdfield_init(tdf(j), maxntime, dt)
    end do

    allocate(st(0:maxntime))
    do j = 0, maxntime
      call states_init(st(j), dim, mode = object)
    end do
    call states_set_initial(st(0))
    
    call states_write(init_st, './output/initial_coeffs')

    call tdfield_get_initial( tdf )

    ! This is the field, as specified in the input file, or as read in the file.
    call tdfield_save( tdf, './output/reftdf0')
    ! We go to and back from the parameters representation.
    if( tdf_rep == TDFIELD_FOURIER .or. tdf_rep == TDFIELD_CONSTRAINED_FOURIER) then
      do j = 1, nfield
        call tdfield_realtime_to_params(tdf(j))
        call tdfield_params_to_realtime(tdf(j))
      end do
    end if
    ! This is the field, once it has been forced to be consistent with the parametrization.
    call tdfield_save( tdf, './output/reftdf1')

    call propagator_full(pr, h, st, tdf)

    finalst = st(maxntime)

    select case(picture)
    case(schrodinger_picture)
      call states_write(finalst, 'output/finalst')
    case(interaction_picture)
      call states_write(finalst, 'output/finalst-int')
      call states_to_schroedinger(finalst, maxntime * dt)
      call states_write(finalst, 'output/finalst-sch')
    end select

    call target_init(h, dim, ierr)
    if(ierr .eq. 0) then
      j1 = target_j1(h, st(maxntime))
      allocate(fluence(nfield))
      call tdfield_fluence(tdf, fluence)
      write(*, '(a,e22.12e3)') ' J1 = ', j1
      write(*, '(a,e22.12e3)') ' J2 = ', fluence(1)
      deallocate(fluence)
    end if

    call output_print(tdf , h , st)
    call observables_print(h, maxntime, st, dt)

    deallocate(tdf)
    do j = 0, maxntime
      call states_end(st(j))
    end do
    deallocate(st)
  end subroutine td
  
  
end module td_m


