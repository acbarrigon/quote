!! Copyright (C) 2014-20xx The quote developing team.
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
  subroutine optimal_control_nlopt(pr, h, reftdf, opttdf)
    type(propagator_t), intent(in) :: pr
    type(hamiltonian_t), intent(in) :: h
    type(tdfield_t), intent(inout) :: reftdf(:)
    type(tdfield_t), intent(inout) :: opttdf(:)

    character(len=50) :: filename
    integer :: i, j, nt, nparams, ires, evaluation_counter, nfield
    integer(8) :: opt, local_opt
    real(8) :: j1, minimum
    real(8), allocatable :: xx(:), j2(:)
    type(states_t), allocatable :: st(:)!, stb(:), stout(:)
    type(tdfield_t), allocatable :: tdf(:)

    write(*, *) '******************************************************'
    write(*, *) 'QOCT algorithm: '//qoct_algorithm_name(qoct_algorithm)%str
    write(*, *) '******************************************************'
    write(*, *)
    nfield = hamiltonian_nfield(h)
    allocate ( tdf(1:nfield) )
    allocate ( j2(1:nfield) )

    do j = 1, nfield
      call tdfield_init(tdf(j), maxntime, dt)
    end do
    call tdfield_copy(tdf, reftdf)

    allocate(st(0:maxntime))
    do j = 0, maxntime
      call states_init(st(j), dim, mode = object)
    end do
    call states_set_initial(st(0))

    call cpu_time(prop_time_t0)
    do nt = 0, maxntime - 1
      call propagator_dt(pr, h, st(nt), st(nt+1), nt, reftdf, 'forward')
    end do
    call cpu_time(prop_time_tf)
    propagation_time = propagation_time + (prop_time_tf - prop_time_t0)
    nforward_props = nforward_props + 1
    j1 = target_j1(h, st(maxntime))
    write(*, '(a,e22.12e3)') ' J1 = ', j1
    write(*, '(a,f15.5)') ' Propagation time = ', prop_time_tf - prop_time_t0

    write(*, *)
    write(*, *) 'Initial J1 = ', j1
    call tdfield_fluence(reftdf, j2)
    write(*, *) 'Initial J2 = ', -qoct_penalty * j2(1)
    write(*, *) 'Initial J  = ', j1 - qoct_penalty * j2(1)
    write(*, *)

    nparams = tdf_nparams

    call nlo_create(opt, qoct_algorithm, nparams)
    call nlo_create(local_opt, qoct_algorithm_nlopt_ld_lbfgs, nparams)

    if(qoct_bounded_optimization) then
      call nlo_set_lower_bounds(ires, opt, lower_bounds)
      if(ires.ne.1) then
        write(*, *) 'Failed to create lower bounds. NLOPT error code = ', ires
      end if
      call nlo_set_upper_bounds(ires, opt, upper_bounds)
      if(ires.ne.1) then
        write(*, *) 'Failed to create upper bounds. NLOPT error code = ', ires
      end if
    end if

    call nlo_set_min_objective(ires, opt, func, 0)

    call nlo_set_ftol_rel(ires, opt, qoct_tol_f)
    call nlo_set_ftol_abs(ires, opt, qoct_tol_f_abs)
    call nlo_set_xtol_rel(ires, opt, qoct_tol_dr)
    call nlo_set_xtol_abs1(ires, opt, qoct_tol_dr_abs)
    call nlo_set_stopval(ires, opt, -qoct_stopval)

    call nlo_set_ftol_rel(ires, local_opt, qoct_tol_f)
    call nlo_set_ftol_abs(ires, local_opt, qoct_tol_f_abs)
    call nlo_set_xtol_rel(ires, local_opt, qoct_tol_dr)
    call nlo_set_xtol_abs1(ires, local_opt, qoct_tol_dr_abs)
    call nlo_set_stopval(ires, local_opt, -qoct_stopval)

    call nlo_set_maxeval(ires, opt, qoct_maxiter)

    call nlo_set_local_optimizer(ires, opt, local_opt)

    allocate(xx(nparams))
    call tdfield_get_v(tdf(1), xx)

    evaluation_counter = 0
    write(*, '(a)') ' #Func. eval.     J1               J2               J          '
    write(*, '(a)') '==============================================================='
    call nlo_optimize(ires, opt, xx, minimum)


    if (ires.lt.1 .or. ires > 4) then
      write(*, '(a,i5)') 'The nlopt optimization failed. Code = ', ires
      select case(ires)
      case(NLOPT_MAXEVAL_REACHED)
        write(*, '(a)') '(NLOPT_MAXEVAL_REACHED)'
      case(NLOPT_MAXTIME_REACHED)
        write(*, '(a)') '(NLOPT_MAXTIME_REACHED)'
      case(NLOPT_FAILURE)
        write(*, '(a)') '(NLOPT_FAILURE)'
      case(NLOPT_INVALID_ARGS)
        write(*, '(a)') '(NLOPT_INVALID_ARGS)'
      case(NLOPT_OUT_OF_MEMORY)
        write(*, '(a)') '(NLOPT_OUT_OF_MEMORY)'
      case(NLOPT_ROUNDOFF_LIMITED)
        write(*, '(a)') '(NLOPT_ROUNDOFF_LIMITED)'
      case(NLOPT_FORCED_STOP)
        write(*, '(a)') '(NLOPT_FORCED_STOP)'
      end select
    else
      write(*, '(a,i5)') 'The nlopt optimization finished successfully. Code = ', ires
      select case(ires)
      case(NLOPT_STOPVAL_REACHED)
        write(*, '(a)') '(NLOPT_STOPVAL_REACHED)'
      case(NLOPT_FTOL_REACHED)
        write(*, '(a)') '(NLOPT_FTOL_REACHED)'
      case(NLOPT_XTOL_REACHED)
        write(*, '(a)') '(NLOPT_XTOL_REACHED)'
      end select
    end if

    call tdfield_copy(opttdf, tdf)
    call tdfield_set_v(opttdf(1), xx)

    ! Final propagation to check everything is fine.
    call cpu_time(prop_time_t0)
    do nt = 0, maxntime - 1
      call propagator_dt(pr, h, st(nt), st(nt+1), nt, opttdf, 'forward')
    end do
    call cpu_time(prop_time_tf)
    propagation_time = propagation_time + (prop_time_tf - prop_time_t0)
    nforward_props = nforward_props + 1
    j1 = target_j1(h, st(maxntime))

    if(object == evolution_operator) then
      write(*, *)
      write(*, *) '===='
      write(*, *) 'D_FS(U, U_target) = ', acos( abs(states_dotp(st(maxntime), target_u))**2 )
      write(*, *) '===='
      write(*, *)
    end if

    write(*, *) 
    write(*, *) 'Final J1 = ', j1
    call tdfield_fluence(opttdf, j2)
    write(*, *) 'Final J2 = ', -qoct_penalty * j2(1)
    write(*, *) 'Final J  = ', j1 - qoct_penalty * j2(1)
    write(*, *)

    call nlo_destroy(opt)
    deallocate(xx)
    do j = 1, nfield
      call tdfield_end(tdf(j))
    end do
    deallocate(tdf)
    deallocate(j2)
    do j = 0, maxntime
      call states_end(st(j))
    end do
    deallocate(st)


    contains

    subroutine func(val, n, x, grad, need_gradient, f_data)
      real(8) :: val, x(n), grad(n), f_data
      integer :: n, need_gradient


      real(8), allocatable :: j2(:)

      call tdfield_set_v(tdf(1), x)
      call tdfield_params_to_realtime(tdf(1))
      allocate(j2(1:nfield))
      call tdfield_fluence(tdf, j2)
      j2 = - qoct_penalty * j2

      if(need_gradient .ne. 0) then
        call qoct_function(pr, h, st, tdf, val, grad)
      else
        call qoct_function(pr, h, st, tdf, val)
      end if

      evaluation_counter = evaluation_counter + 1
      write(*, '(i6,3f18.8)') evaluation_counter, -val - j2(1), j2(1), -val

      if(clean_stop()) then
        call nlo_force_stop(ires, opt)
      end if

    end subroutine

  end subroutine optimal_control_nlopt
