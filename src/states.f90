!! Copyright (C) 2014-20xx The quote developing team.
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.

module states_m
  use aotus_module
  use global_m
  use input_m

  implicit none

  private
  public :: states_mod_init,        &
            states_set_tm,          &
            states_get_initial,     &
            states_t,               &
            operator_t,             &
            states_to_schroedinger, &
            states_to_interaction,  &
            states_write,           &
            states_read,            &
            operator_write,         &
            operator_read,          &
            states_init,            &
            states_end,             &
            operator_init,          &
            operator_end,           &
            states_set,             &
            states_get,             &
            operator_set,           &
            operator_get,           &
            operator_to_interaction,&
            operator_to_eigenbasis, &
            states_mode,            &
            states_set_initial,     &
            states_dotp,            &
            states_zgemv_,          &
            states_zaxpy,           &
            operator_zaxpy,         &
            operator_expvalue

  type states_t
    private
    integer  :: mode = -1
    integer  :: dim
    complex(8), allocatable :: c(:)
    complex(8), allocatable :: u(:, :)
  end type states_t

  type operator_t
    private
    integer :: mode = -1
    integer :: dim
    complex(8), allocatable :: m(:, :)
  end type operator_t

  interface states_set
    module procedure states_set_wf, states_set_u
  end interface

  interface states_get
    module procedure states_get_wf, states_get_u
  end interface

  interface states_to_schroedinger
    module procedure states_to_schroedinger_wf, &
                     states_to_schroedinger_st, &
                     states_to_schroedinger_u
  end interface

  interface states_to_interaction
    module procedure states_to_interaction_wf, &
                     states_to_interaction_st, &
                     states_to_interaction_u
  end interface


  integer, public :: object
  integer, parameter, public :: wavefunction = 0, &
                        evolution_operator = 1

  integer, public :: initial_state
  type(states_t), public :: init_st
  complex(8), allocatable :: states_tm(:, :)
  real(8), allocatable :: states_eval(:)

  contains

  subroutine states_mod_init()
    ! What should be propagated / optimized? A wavefunction or an evolution operator.
    call aot_get_val(L = input_file, key = 'object', val = object, &
      errcode = input_error, default = wavefunction)
      write(*, *)
      write(*, *) '******************************************************'
      select case ( object )
        case ( wavefunction )
          write(*, *) 'Mode: Propagation of wave functions.'
        case ( evolution_operator )
          write(*, *) 'Mode: Propagation of the evolution operator.'
      end select
      write(*, *) '******************************************************'
      write(*, *)
  end subroutine states_mod_init


  subroutine states_set_tm(tm, eval)
    complex(8), intent(in) :: tm(:, :)
    real(8), intent(in) :: eval(:)

    allocate(states_tm(dim, dim))
    allocate(states_eval(dim))

    states_tm = tm
    states_eval = eval
  end subroutine states_set_tm


  subroutine states_get_initial()

    complex(8), allocatable :: initial_states(:)

    call aot_get_val(L = input_file, key = 'initial_state', val = initial_state, &
        errcode = input_error, default = interaction_picture)

    allocate ( initial_states(1:dim) )
    allocate(input_error_array(dim))
    initial_states = (0.0_8, 0.0_8)
    initial_states(1) = (1.0_8, 0.0_8)
    call aot_get_zval(L = input_file, key = 'initial_states', val = initial_states, &
      errcode = input_error_array, default = initial_states)
    deallocate(input_error_array)
    initial_states = initial_states/sqrt(dot_product(initial_states, initial_states))

    call states_init(init_st, dim, mode = wavefunction)
    call states_set_wf(init_st, initial_states(:))

    write(*, *)
    write(*, *) '******************************************************'
    select case(picture)
    case(interaction_picture)
      if(initial_state .eq. schrodinger_picture) then
        call states_to_eigenbasis(init_st)
        write(*, *) 'The initial state was transformed to the eigenbasis.'
      end if
    case(schrodinger_picture)
      if(initial_state .eq. interaction_picture) then
        call states_from_eigenbasis(init_st)
        write(*, *) 'The initial state was transformed to the input basis.'
      end if
    end select
    write(*, *) '******************************************************'
    write(*, *)

    deallocate(initial_states)
  end subroutine states_get_initial


  subroutine states_init(a, dim, mode)
    type(states_t), intent(inout) :: a
    integer, intent(in) :: dim, mode
    if(a%mode .ne. -1) then
      call states_end(a)
    end if
    a%mode = mode
    a%dim = dim
    select case(mode)
    case(wavefunction)
      allocate(a%c(dim))
      a%c = (0.0_8, 0.0_8)
    case(evolution_operator)
      allocate(a%u(dim, dim))
      a%u = (0.0_8, 0.0_8)
    end select
  end subroutine states_init


  subroutine states_end(a)
    type(states_t), intent(inout) :: a
    a%mode = -1
    if(allocated(a%c)) deallocate(a%c)
    if(allocated(a%u)) deallocate(a%u)
  end subroutine states_end


  subroutine operator_init(o, dim, mode)
    type(operator_t), intent(inout) :: o
    integer, intent(in) :: dim, mode
    if(o%mode .ne. -1) then
      call operator_end(o)
    end if
    o%mode = mode
    o%dim = dim
    allocate(o%m(dim, dim))
    o%m = (0.0_8, 0.0_8)
  end subroutine operator_init


  subroutine operator_end(o)
    type(operator_t), intent(inout) :: o
    o%mode = -1
    if(allocated(o%m)) deallocate(o%m)
  end subroutine operator_end


  subroutine states_set_wf(a, c)
    type(states_t), intent(inout) :: a
    complex(8), intent(in) :: c(:)
    if(a%mode .eq. wavefunction) then
      a%c = c
    else
      stop 'ERROR 1'
    end if
  end subroutine states_set_wf


  subroutine states_set_u(a, u)
    type(states_t), intent(inout) :: a
    complex(8), intent(in) :: u(:, :)
    if(a%mode .eq. evolution_operator) then
      a%u = u
    else
      stop 'ERROR 2'
    end if
  end subroutine states_set_u


  subroutine operator_set(o, m)
    type(operator_t), intent(inout) :: o
    complex(8), intent(in) :: m(:, :)
    o%m = m
  end subroutine operator_set


  subroutine operator_get(o, m)
    type(operator_t), intent(in) :: o
    complex(8), intent(inout) :: m(:, :)
    m = o%m
  end subroutine operator_get


  subroutine states_get_wf(a, c)
    type(states_t), intent(in) :: a
    complex(8), intent(inout) :: c(:)
    if(a%mode .eq. wavefunction) then
      c = a%c
    else
      stop 'ERROR 3'
    end if
  end subroutine states_get_wf


  subroutine states_get_u(a, u)
    type(states_t), intent(in) :: a
    complex(8), intent(inout) :: u(:, :)
    if(a%mode .eq. evolution_operator) then
      u = a%u
    else
      stop 'ERROR 4'
    end if
  end subroutine states_get_u


  subroutine states_to_eigenbasis(st)
    type(states_t), intent(inout)   :: st

    complex(8), allocatable :: c(:)

    allocate(c(st%dim))
    call states_get(st, c)
    c = matmul(conjg(transpose(states_tm)), c)
    call states_set(st, c)
    deallocate(c)
  end subroutine states_to_eigenbasis


  subroutine states_from_eigenbasis(st)
    type(states_t), intent(inout)   :: st

    complex(8), allocatable :: c(:)

    allocate(c(st%dim))
    call states_get(st, c)
    c = matmul(states_tm, c)
    call states_set(st, c)
    deallocate(c)
  end subroutine states_from_eigenbasis


  subroutine operator_to_eigenbasis(o)
    type(operator_t), intent(inout)   :: o

    complex(8), allocatable :: m(:, :)

    allocate(m(o%dim, o%dim))
    call operator_get(o, m)
    m = matmul(conjg(transpose(states_tm)), matmul(m, states_tm))
    call operator_set(o, m)
    deallocate(m)
  end subroutine operator_to_eigenbasis


  subroutine states_to_schroedinger_st(st, t)
    type(states_t), intent(inout)   :: st
    real(8), intent(in)             :: t

    complex(8), allocatable :: c(:), u(:, :)

    select case(st%mode)
    case(wavefunction)
      allocate(c(st%dim))
      call states_get(st, c)
      call states_to_schroedinger_wf(c, t)
      call states_set(st, c)
      deallocate(c)
    case(evolution_operator)
      allocate(u(st%dim, st%dim))
      call states_get(st, u)
      call states_to_schroedinger_u(u, t)
      call states_set(st, u)
      deallocate(u)
    end select
  end subroutine states_to_schroedinger_st


  subroutine states_to_schroedinger_wf(c, t)
    complex(8), intent(inout)       :: c(:)
    real(8), intent(in)             :: t

    integer :: k, dim

    dim = size(c)
    do k = 1, dim
      c(k) = c(k) * exp( - im * t * states_eval(k) )
    end do
  end subroutine states_to_schroedinger_wf


  subroutine states_to_schroedinger_u(u, t)
    complex(8), intent(inout)       :: u(:, :)
    real(8), intent(in)             :: t

    integer :: i, j

    do i = 1, dim
      do j = 1, dim
        u(i, j) = expi( -(states_eval(i)) * t ) * u(i, j)
      end do
    end do

  end subroutine states_to_schroedinger_u


  subroutine states_to_interaction_st(st, t)
    type(states_t), intent(inout)   :: st
    real(8), intent(in)             :: t

    complex(8), allocatable :: c(:), u(:, :)

    select case(st%mode)
    case(wavefunction)
      allocate(c(st%dim))
      call states_get(st, c)
      call states_to_interaction_wf(c, t)
      call states_set(st, c)
      deallocate(c)
    case(evolution_operator)
      allocate(u(st%dim, st%dim))
      call states_get(st, u)
      call states_to_interaction_u(u, t)
      call states_set(st, u)
      deallocate(u)
    end select
  end subroutine states_to_interaction_st


  subroutine states_to_interaction_wf(c, t)
    complex(8), intent(inout)       :: c(:)
    real(8), intent(in)             :: t

    integer :: k, dim

    dim = size(c)
    do k = 1, dim
      c(k) = c(k) * exp( im * t * states_eval(k) )
    end do
  end subroutine states_to_interaction_wf


  subroutine states_to_interaction_u(u, t)
    complex(8), intent(inout)       :: u(:, :)
    real(8), intent(in)             :: t

    integer :: i, j

    do i = 1, dim
      do j = 1, dim
        u(i, j) = expi( (states_eval(i)) * t ) * u(i, j)
      end do
    end do
  end subroutine states_to_interaction_u


  subroutine operator_to_interaction(o, t, op)
    type(operator_t), intent(inout)   :: o
    real(8), intent(in)             :: t
    type(operator_t), intent(inout), optional :: op

    integer :: i, j

    if(present(op)) then
      do i = 1, dim
        do j = 1, dim
          op%m(i, j) = expi( (states_eval(i)-states_eval(j)) * t ) * o%m(i, j)
        end do
      end do
    else
      do i = 1, dim
        do j = 1, dim
          o%m(i, j) = expi( (states_eval(i)-states_eval(j)) * t ) * o%m(i, j)
        end do
      end do
    end if

  end subroutine operator_to_interaction


  subroutine states_write(st, filename)
    type(states_t), intent(in)     :: st
    character(len = *), intent(in) :: filename

    integer :: i
    complex(8), allocatable :: c(:)

    open(unit=11, status='unknown', file=trim(filename))

    select case(st%mode)
    case(wavefunction)
      allocate(c(st%dim))
      call states_get(st, c)
      do i = 1, st%dim
        write(11, *) c(i)
      end do
      deallocate(c)
    case(evolution_operator)
      write(11, *) st%dim
      do i = 1, st%dim
        write(11, *) st%u(i, :)
      end do
    end select

    close(11)
  end subroutine states_write


  subroutine states_read(st, filename)
    type(states_t), intent(inout) :: st
    character(len = *), intent(in) :: filename

    integer :: i
    complex(8), allocatable :: c(:)

    open(unit=11, status='unknown', file=trim(filename))

    select case(st%mode)
    case(wavefunction)
      do i = 1, st%dim
        read(11, *) st%c(i)
      end do
    case(evolution_operator)
      read(11, *) i
      do i = 1, st%dim
        read(11, *) st%u(i, :)
      end do
    end select

    close(11)
  end subroutine states_read


  subroutine operator_write(o, filename)
    type(operator_t), intent(in)     :: o
    character(len = *), intent(in) :: filename

    integer :: i

    open(unit=11, status='unknown', file=trim(filename))
    write(11, *) o%dim
    do i = 1, o%dim
      write(11, *) o%m(i, :)
    end do
    close(11)

  end subroutine operator_write


  subroutine operator_read(o, filename)
    type(operator_t), intent(inout)     :: o
    character(len = *), intent(in) :: filename

    integer :: i, file_dim

    open(unit=11, status='unknown', file=trim(filename))
    read(11, *) file_dim
    do i = 1, o%dim
      read(11, *) o%m(i, :)
    end do
    close(11)

  end subroutine operator_read


  integer function states_mode(st) result(mode)
    type(states_t), intent(in) :: st
    mode = st%mode
  end function states_mode


  subroutine states_set_initial(st)
    type(states_t), intent(inout) :: st

    integer :: i
    complex(8), allocatable :: u0(:, :)

    select case(st%mode)
    case(wavefunction)
      st = init_st
    case(evolution_operator)
      allocate(u0(st%dim, st%dim))
      u0 = (0.0_8, 0.0_8)
      forall(i = 1:st%dim) ; u0(i, i) = (1.0_8, 0.0_8) ; end forall
      call states_set(st, u0)
      deallocate(u0)
    end select

  end subroutine states_set_initial


  complex(8) function states_dotp(a, b) result(dotp)
    type(states_t), intent(in) :: a, b
    select case(a%mode)
    case(wavefunction)
      dotp = dot_product(a%c, b%c)
    case(evolution_operator)
      dotp = frobenius_dotp(a%u, b%u)
    end select
  end function states_dotp


  !y := alpha*A*x + beta*y,   or   y := alpha*A**T*x + beta*y,   or
  subroutine states_zgemv_(alpha, o, stx, beta, sty)
    complex(8), intent(in) :: alpha, beta
    type(operator_t), intent(in) :: o
    type(states_t), intent(inout) :: sty
    type(states_t), intent(in) :: stx

    select case(stx%mode)
    case(wavefunction)
      sty%c = alpha * matmul(o%m, stx%c) + beta * sty%c
    case(evolution_operator)
      sty%u = alpha * matmul(o%m, stx%u) + beta * sty%u
    end select

  end subroutine states_zgemv_

 
  subroutine states_zaxpy(alpha, stx, sty)
    complex(8), intent(in) :: alpha
    type(states_t), intent(inout) :: stx, sty

    select case(stx%mode)
    case(wavefunction)
      sty%c = alpha * stx%c + sty%c
    case(evolution_operator)
      sty%u = alpha * stx%u + sty%u
    end select

  end subroutine states_zaxpy


  subroutine operator_zaxpy(alpha, ox, oy)
    complex(8), intent(in) :: alpha
    type(operator_t), intent(inout) :: ox, oy
    oy%m = alpha * ox%m + oy%m
  end subroutine operator_zaxpy


  complex(8) function operator_expvalue(o, st) result(z)
    type(operator_t), intent(in) :: o
    type(states_t), intent(in)   :: st

    if(st%mode .ne. wavefunction) then
      stop 'ERROR 5'
    end if

    z = dot_product(st%c, matmul(o%m, st%c))

  end function operator_expvalue


end module states_m




