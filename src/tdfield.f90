!! Copyright (C) 2014-20xx The quote developing team.
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.

module tdfield_m
  use aotus_module
  use aot_fun_module
  use aot_table_module
  use global_m
  use input_m
  use math_m

  implicit none

  private
  public :: tdfield_t,                  &
            tdfield_mod_init,           &
            tdfield_mod_end,            &
            tdfield_init,               &
            tdfield_copy,               &
            tdfield_get,                &
            tdfield_get_v,              &
            tdfield_set,                &
            tdfield_set_v,              &
            tdfield_save,               &
            tdfield_fluence,            &
            tdfield_integral,           &
            tdfield_dfluence,           &
            tdfield_multiply,           &
            tdfield_diff,               &
            tdfield_der,                &
            tdfield_dotp,               &
            tdfield_realtime_to_params, &
            tdfield_params_to_realtime, &
            tdfield_realtime_to_fourier,&
            tdfield_get_random_array,   &
            tdfield_get_initial,        &
            tdfield_read_bounds,        &
            tdfield_end

  integer, public, parameter ::      &
    tdfield_realtime            = 0, &
    tdfield_fourier             = 1, &
    tdfield_constrained_fourier = 2, &
    tdfield_generic             = 4

  integer, public, parameter ::       &
    tdfield_init_from_file       = 1, &
    tdfield_init_from_expression = 2, &
    tdfield_init_random          = 3, &
    tdfield_init_from_params     = 4


  integer, public :: tdf_rep
  integer, public :: tdf_nparams
  integer, public :: tdf_rng_seed
  real(8), public :: tdf_cutoff
  character(len=100) :: tdf_generic_pulse, tdf_generic_der_pulse
  real(8), allocatable, public :: upper_bounds(:), lower_bounds(:)


  type tdfield_t
    private
    integer :: rep
    integer :: nmax
    real(8) :: dt
    real(8), allocatable :: u(:)
    real(8), allocatable :: v(:)
    type(aot_fun_type) :: generic_function
    character(len=100) :: generic_function_name ! Do not know how to get the name from the type...
    type(aot_fun_type) :: generic_der_function
    character(len=100) :: generic_der_function_name
  end type tdfield_t


  interface tdfield_save
    module  procedure tdfield_save_1, tdfield_save_n
  end interface

  interface tdfield_copy
    module procedure tdfield_copy_1, tdfield_copy_n
  end interface

  interface tdfield_set
    module procedure tdfield_set_i, tdfield_set_all
  end interface

  interface tdfield_set_v
    module procedure tdfield_set_v_i, tdfield_set_v_all
  end interface

  interface tdfield_get
    module procedure tdfield_get_i, tdfield_get_all, tdfield_get_t
  end interface

  interface tdfield_get_v
    module procedure tdfield_get_v_i, tdfield_get_v_all
  end interface


  contains


  subroutine tdfield_mod_init()

    integer :: max_frequency_index, frequency_index
    real(8) :: l, max_cutoff

    call aot_get_val(L = input_file, key = 'tdfield_representation', val = tdf_rep, &
      errcode = input_error, default = tdfield_realtime)

    l = maxntime * dt

    write(*, *) '******************************************************'
    select case(tdf_rep)
    case(tdfield_realtime)
      tdf_nparams = maxntime + 1
      tdf_cutoff = (2.0_8*pi/l) * maxntime/2 + 1.0e-12_8

      write(*, *) 'Real time parameterization of the td field.'
    case(tdfield_fourier)
      max_frequency_index = maxntime/2 - 1
      max_cutoff = (2.0_8*pi/l) * max_frequency_index + 1.0e-12_8
      call aot_get_val(L = input_file, key = 'cutoff', val = tdf_cutoff, &
        errcode = input_error, default = max_cutoff)
      if(tdf_cutoff > max_cutoff) then
        tdf_cutoff = max_cutoff
        write(*, *) 'WARNING: tdf_cutoff set to max value = ', max_cutoff
      end if
      frequency_index = int( tdf_cutoff * l / (2.0_8 * pi) )
      tdf_nparams = 2 * frequency_index + 1
      if (tdf_nparams == maxntime + 1) tdf_nparams = tdf_nparams - 1
      write(*, *) 'Fourier series parameterization of the td field.'
      write(*, *) '  Max. cutoff = ', max_cutoff
      write(*, *) '  Cutoff      = ', tdf_cutoff
      write(*, *) '  Parameters  = ', tdf_nparams
    case(tdfield_constrained_fourier)
      ! This is just the same that the TDFIELD_FOURIER case...
      max_frequency_index = maxntime/2 - 1
      max_cutoff = (2.0_8*pi/l) * max_frequency_index + 1.0e-12_8
      call aot_get_val(L = input_file, key = 'cutoff', val = tdf_cutoff, &
        errcode = input_error, default = max_cutoff)
      if(tdf_cutoff > max_cutoff) then
        tdf_cutoff = max_cutoff
        write(*, *) 'WARNING: tdf_cutoff set to max value = ', max_cutoff
      end if
      frequency_index = int( tdf_cutoff * l / (2.0_8 * pi) )
      tdf_nparams = 2 * frequency_index - 1
      write(*, *) 'Constrained-Fourier series parameterization of the td field.'
      write(*, *) '  Max. cutoff = ', max_cutoff
      write(*, *) '  Cutoff      = ', tdf_cutoff
      write(*, *) '  Parameters  = ', tdf_nparams

    case(tdfield_generic)
      call aot_get_val(L = input_file, key = 'tdfield_generic_definition', val = tdf_generic_pulse, &
        errcode = input_error, default = '')
      if(tdf_generic_pulse .eq. '') then
        write(*, *) 'You must supply a pulse definition with the tdfield_generic_definition variable.'
        stop
      end if
      call aot_get_val(L = input_file, key = 'tdfield_generic_der_definition', val = tdf_generic_der_pulse, &
        errcode = input_error, default = '')
      if(tdf_generic_pulse .eq. '') then
        write(*, *) 'You must supply a pulse definition with the tdfield_generic_der_definition variable.'
        stop
      end if
      write(*, *) 'Generic pulse read from the input file, with name: "'//trim(tdf_generic_pulse)//'"'
      write(*, *) 'Derivativive of generic pulse read from the input file, with name: "'//trim(tdf_generic_der_pulse)//'"'
      call aot_get_val(L = input_file, key = 'tdfield_generic_nparams', val = tdf_nparams, &
        errcode = input_error, default = 0)
      if(tdf_nparams == 0) then
        write(*, *) 'You must specify the number of parameters through "tdf_generic_nparams" when'
        write(*, *) 'using tdfield_generic.'
        stop
      else
        write(*, *) '  Parameters  = ', tdf_nparams
      end if

    case default
      stop 'tdfield_representation has an invalid value.'
    end select
    write(*, *) '******************************************************'
    write(*, *)

  end subroutine tdfield_mod_init


  subroutine tdfield_mod_end()
    if(allocated(lower_bounds)) deallocate(lower_bounds)
    if(allocated(upper_bounds)) deallocate(upper_bounds)
  end subroutine tdfield_mod_end


  subroutine tdfield_init(tdf, maxntime, dt, rep)
    type(tdfield_t), intent(inout) :: tdf
    integer, intent(in) :: maxntime
    real(8), intent(in) :: dt
    integer, optional, intent(in) :: rep
    if(present(rep)) then
      tdf%rep = rep
    else
      tdf%rep = tdf_rep
    end if
    tdf%nmax = maxntime
    tdf%dt   = dt
    allocate(tdf%u(0:maxntime))
    tdf%u = 0.0_8
    allocate(tdf%v(tdf_nparams))
    tdf%v = 0.0_8
    if(tdf%rep == tdfield_generic) then
      tdf%generic_function_name = trim(tdf_generic_pulse)
      tdf%generic_der_function_name = trim(tdf_generic_der_pulse)
      !! WARNING: probably one should check that the function could be properly open.
    end if
  end subroutine tdfield_init


  subroutine tdfield_end(tdf)
    type(tdfield_t), intent(inout) :: tdf
    deallocate(tdf%u)
    deallocate(tdf%v)
  end subroutine tdfield_end


  subroutine tdfield_copy_1(tdfo, tdfi)
    type(tdfield_t), intent(inout) :: tdfo
    type(tdfield_t), intent(in)  :: tdfi
    ! We assume that both are initialized, and have the same size.
    tdfo%rep = tdfi%rep
    tdfo%dt = tdfi%dt
    tdfo%nmax = tdfi%nmax
    tdfo%u  = tdfi%u
    tdfo%v  = tdfi%v
    if(tdfi%rep .eq. tdfield_generic) then
      tdfo%generic_function_name = tdfi%generic_function_name
      tdfo%generic_der_function_name = tdfi%generic_der_function_name
    end if
  end subroutine tdfield_copy_1


  subroutine tdfield_copy_n(tdfo, tdfi)
    type(tdfield_t), intent(inout) :: tdfo(:)
    type(tdfield_t), intent(in)  :: tdfi(:)
    integer :: j
    do j = 1, size(tdfi)
      call tdfield_copy_1(tdfo(j), tdfi(j))
    end do
  end subroutine tdfield_copy_n


  ! Construct the "v" array of the field, i.e. the set of parameters,
  ! from its real-time specification ("u" array).
  subroutine tdfield_realtime_to_params(tdf)
    type(tdfield_t), intent(inout) :: tdf

    integer :: j, k
    complex(8), allocatable :: zout(:)
    real(8), allocatable :: in(:)

    select case(tdf_rep)
    case(TDFIELD_REALTIME)
      tdf%v(1:tdf_nparams) = tdf%u(0:tdf_nparams-1)
    case(TDFIELD_FOURIER)

      allocate(in(0:tdf%nmax-1))
      allocate(zout(0:tdf%nmax/2))
      in = tdf%u
      call fft_r2c_1d(in, zout, tdf%nmax)
      zout = zout * tdf%dt / sqrt(tdf%nmax * tdf%dt)

      tdf%v(1) = real(zout(0), kind = 8)
      k = 2
      do j = 1, tdf_nparams/2
        tdf%v(k) = real(zout(j), kind = 8)
        if(k+1 <= tdf_nparams) tdf%v(k+1) = -aimag(zout(j))
        k = k + 2
      end do

      deallocate(in)
      deallocate(zout)
    case(TDFIELD_CONSTRAINED_FOURIER)
      allocate(in(0:tdf%nmax-1))
      allocate(zout(0:tdf%nmax/2))
      in = tdf%u
      call fft_r2c_1d(in, zout, tdf%nmax)
      zout = zout * tdf%dt / sqrt(tdf%nmax * tdf%dt)

      k = 1
      do j = 1, tdf_nparams/2 + 1
        if(j < tdf_nparams / 2 + 1) then
          tdf%v(k) = real(zout(j), kind = 8)
          tdf%v(k+1) = - aimag(zout(j))
        else
          tdf%v(k) = - aimag(zout(j))
        end if
        k = k + 2
      end do

    case(TDFIELD_GENERIC)
      write(*, *) 'Error in tdfield_realtime_to_params: cannot transform when using a generic function.'
      stop

    end select

  end subroutine tdfield_realtime_to_params


  ! Construct the "u" array of the field, i.e. the set of values in the
  ! real time axis, from the set of parameters ("v" array).
  subroutine tdfield_params_to_realtime(tdf)
    type(tdfield_t), intent(inout) :: tdf

    integer :: j, k
    real(8) :: t
    complex(8), allocatable :: zin(:)

    select case(tdf_rep)
    case(tdfield_realtime)
      ! This is obviously just a copy.
      tdf%u(0:tdf_nparams-1) = tdf%v(1:tdf_nparams)
    case(tdfield_fourier)

      allocate(zin(0:tdf%nmax/2))
      zin = (0.0_8, 0.0_8)

      zin(0) = cmplx(tdf%v(1), 0.0_8, kind = 8)
      k = 2
      do j = 1, tdf_nparams/2
        if(k+1 <= tdf_nparams) then
          zin(j) = cmplx(tdf%v(k), -tdf%v(k+1), kind = 8)
        else
          zin(j) = cmplx(tdf%v(k), 0.0_8, kind = 8)
        end if
        k = k + 2
      end do

      call fft_c2r_1d(zin, tdf%u(0:), tdf%nmax)
      tdf%u(tdf%nmax) = tdf%u(0)
      tdf%u = tdf%u / sqrt(tdf%nmax * tdf%dt)

      deallocate(zin)

    case(tdfield_constrained_fourier)
      allocate(zin(0:tdf%nmax/2))
      zin = (0.0_8, 0.0_8)

      zin(0) = (0.0_8, 0.0_8)
      k = 1
      do j = 1, tdf_nparams/2 + 1
        if(j < tdf_nparams / 2 + 1) then
          zin(j) = cmplx(tdf%v(k), -tdf%v(k+1), kind = 8)
        else
          zin(j) = cmplx( -sum(real(zin(1:j-1), kind=8)), -tdf%v(k), kind = 8)
        end if
        k = k + 2
      end do

      call fft_c2r_1d(zin, tdf%u(0:), tdf%nmax)
      tdf%u(tdf%nmax) = tdf%u(0)
      tdf%u = tdf%u / sqrt(tdf%nmax * tdf%dt)

    case(tdfield_generic)

      call aot_fun_open(L = input_file, fun = tdf%generic_function, key = trim(tdf_generic_pulse))
      tdf%u = 0.0_8
      do j = 0, tdf%nmax
        t = tdf%dt*j
        call aot_fun_put(L = input_file, fun = tdf%generic_function, arg = tdf%v(1:tdf_nparams))
        call aot_fun_put(L = input_file, fun = tdf%generic_function, arg = t)
        call aot_fun_do(L = input_file, fun = tdf%generic_function, nresults = 1, errcode = input_error)
        call aot_top_get_val(L = input_file, val = tdf%u(j) , errcode = input_error)
      end do
      call aot_fun_close(L = input_file, fun = tdf%generic_function)
    
    end select

  end subroutine tdfield_params_to_realtime


  subroutine tdfield_get_initial( tdf )
    type(tdfield_t), intent(inout) :: tdf(:)

    integer :: j, nt, n, nmax, epsilon_functions, nfield
    integer :: tdfield_init_from
    character (len=80) :: filename, function_name
    real(8) :: et, t
    type(aot_fun_type) :: amplitude_function

    call aot_get_val(L = input_file, key = 'tdfield_init_from', val = tdfield_init_from, &
      errcode = input_error, default = tdfield_init_from_expression)

    nfield = size(tdf)

    select case(tdf_rep)
    case(tdfield_generic)
      if( (tdfield_init_from .ne. tdfield_init_from_params) .and. &
          (tdfield_init_from .ne. tdfield_init_random) .and. &
          (tdfield_init_from .ne. tdfield_init_from_file) ) then
        write(*, *) 'If the tdfield representation is tdfield_generic, then you cannot set'
        write(*, *) 'tdfield_init_from = tdfield_init_from_expression.'
        stop
      end if
    end select

    select case(tdfield_init_from)

    case(tdfield_init_from_params)
      if(nfield > 1) then
        write(*, *) 'When tdfield_init_from = tdfield_init_from_params, then nfield must be 1'
        stop
      end if
      allocate(input_error_array(tdf_nparams))
      call aot_get_val(L = input_file, key = 'tdfield_init_parameters', val = tdf(1)%v, &
        errcode = input_error_array, default = (/ (0.0_8, j = 1, tdf_nparams ) /) )
      call tdfield_params_to_realtime(tdf(1))
      deallocate(input_error_array)

    case(tdfield_init_from_file)
      call aot_get_val(L = input_file, key = 'initialize_from_filename', val = filename, &
        errcode = input_error, default = './output/opttdf')
      call tdfield_read( tdf,  filename)

    case(tdfield_init_random)
      do j = 1, nfield
        call tdfield_get_random_array(tdf(j)%v)
        call tdfield_params_to_realtime(tdf(j))
      end do

    case(tdfield_init_from_expression)

      if(nfield == 1) then
        call aot_fun_open(L = input_file, fun = amplitude_function, key = 'epsilon')
        nmax = tdf(1)%nmax
        do nt = 0, nmax
          t = nt*tdf(1)%dt
          call aot_fun_put(L = input_file, fun = amplitude_function, arg = t)
          call aot_fun_do(L = input_file, fun = amplitude_function, nresults = 1)
          call aot_top_get_val(L = input_file, val = et , ErrCode = input_error)
          tdf(1)%u(nt) = et
        end do
        call aot_fun_close(L = input_file, fun = amplitude_function)
      else
        call aot_table_open(L = input_file, thandle = epsilon_functions, key = 'epsilon_functions')
        do n = 1, nfield
          call aot_get_val(L = input_file, thandle = epsilon_functions, val = function_name, errcode = input_error, pos = n)
          call aot_fun_open(L = input_file, fun = amplitude_function, key = trim(function_name))
          nmax = tdf(1)%nmax
          do nt = 0, nmax
            t = nt*tdf(1)%dt
            call aot_fun_put(L = input_file, fun = amplitude_function, arg = t)
            call aot_fun_do(L = input_file, fun = amplitude_function, nresults = 1)
            call aot_top_get_val(L = input_file, val = et , ErrCode = input_error)
            tdf(n)%u(nt) = et
          end do
          call aot_fun_close(L = input_file, fun = amplitude_function)
        end do
        call aot_table_close(L = input_file, thandle = epsilon_functions)
      end if

    case default
      write(*, *) 'Error in variable "initialize_from".'
      stop
    end select

  end subroutine tdfield_get_initial


  logical function tdfield_read_bounds ()
    integer :: j

    allocate(upper_bounds(tdf_nparams))
    allocate(lower_bounds(tdf_nparams))
    lower_bounds = 0.0_8
    upper_bounds = 0.0_8

    if( aot_exists(L = input_file, key = 'lower_bounds' ) ) then
      write(*, *) 'lower_bounds found in input file'
      allocate(input_error_array(tdf_nparams))
      call aot_get_val(L = input_file, key = 'lower_bounds', val = lower_bounds, &
        errcode = input_error_array, default = (/ (0.0_8, j=1, tdf_nparams) /) )
      deallocate(input_error_array)
      if( aot_exists(L = input_file, key = 'upper_bounds' ) ) then
        write(*, *) 'upper_boudns found in input file'  
        allocate(input_error_array(tdf_nparams))
        call aot_get_val(L = input_file, key = 'upper_bounds', val = upper_bounds, &
          errcode = input_error_array, default =  (/ (0.0_8, j=1, tdf_nparams) /) )
        deallocate(input_error_array)
      else
        write(*, *) 'upper_bounds not found in input file, but lower bounds was present.'
        stop
      end if
      tdfield_read_bounds = .true.
    else
      tdfield_read_bounds = .false.
    end if

  end function tdfield_read_bounds


  subroutine tdfield_read( tdf, name)
    type(tdfield_t), intent(inout) :: tdf(:)
    character(len=*), intent(in) :: name

    character :: c
    integer :: n, m, i, nt, nfield
    real(8) :: t
    
    open ( unit = 11 , file = trim(name) , status = 'unknown' )

    select case(tdf(1)%rep)
    case(tdfield_realtime)
      nfield = size(tdf)
      do
        read(11, '(a1)') c
        if(c .ne. '#') exit
      end do
      backspace(11)
      do n = 1, nfield
        do nt = 0, tdf(1)%nmax
          read(11,*) t, tdf(n)%u(nt)
        end do
      end do

    case default
      read(11, '(a1)') c
      do n = 1, size(tdf)
        do m = 1, tdf_nparams
          read(11, *) c, i, tdf(n)%v(m)
        end do
        call tdfield_params_to_realtime(tdf(n))
      end do

    end select

    close(11)

  end subroutine tdfield_read

  subroutine tdfield_get_all(tdf, u)
    type(tdfield_t), intent(in) :: tdf
    real(8), intent(inout) :: u(:)
    u = tdf%u
  end subroutine tdfield_get_all

  subroutine tdfield_get_i(tdf, i, val)
    type(tdfield_t), intent(in) :: tdf
    integer, intent(in) :: i
    real(8), intent(inout) :: val
    val = tdf%u(i)
  end subroutine tdfield_get_i

  subroutine tdfield_get_t(tdf, t, val)
    type(tdfield_t), intent(inout) :: tdf
    real(8), intent(in)         :: t
    real(8), intent(inout)      :: val

    integer :: j
    real(8), allocatable :: timearray(:), valarray(:)

    if(t < 0.0_8 .or. t > tdf%dt * tdf%nmax) then
      val = 0.0_8
      return
    end if

    select case(tdf%rep)
    case(tdfield_generic)
      call aot_fun_open(L = input_file, fun = tdf%generic_function, key = trim(tdf%generic_function_name))
      call aot_fun_put(L = input_file, fun = tdf%generic_function, arg = tdf%v)
      call aot_fun_put(L = input_file, fun = tdf%generic_function, arg = t)
      call aot_fun_do(L = input_file, fun = tdf%generic_function, nresults = 1)
      call aot_top_get_val(L = input_file, val = val , errcode = input_error)
      call aot_fun_close(L = input_file, fun = tdf%generic_function)
    case default
      j = int(t/tdf%dt)
      allocate(timearray(1:4))
      allocate(valarray(1:4))
      if(j==0) then
        timearray = (/ j*tdf%dt, (j+1)*tdf%dt, (j+2)*tdf%dt, (j+3)*tdf%dt /)
        valarray  = (/ tdf%u(j), tdf%u(j+1), tdf%u(j+2), tdf%u(j+3) /)
        call interpolate_0(timearray, valarray, t, val)
      else if( j < tdf%nmax - 1) then
        timearray = (/ (j-1)*tdf%dt, j*tdf%dt, (j+1)*tdf%dt, (j+2)*tdf%dt/)
        valarray  = (/ tdf%u(j-1), tdf%u(j), tdf%u(j+1), tdf%u(j+2) /)
        call interpolate_0(timearray, valarray, t, val)
      else
        timearray = (/ (tdf%nmax-3)*tdf%dt, (tdf%nmax-2)*tdf%dt, (tdf%nmax -1)*tdf%dt, tdf%nmax*tdf%dt /)
        valarray  = (/ tdf%u(tdf%nmax-3), tdf%u(tdf%nmax-2), tdf%u(tdf%nmax-1), tdf%u(tdf%nmax) /)
        call interpolate_0(timearray, valarray, t, val)
      end if
      deallocate(timearray, valarray)
    end select

  end subroutine tdfield_get_t

  subroutine tdfield_get_v_all(tdf, v)
    type(tdfield_t), intent(in) :: tdf
    real(8), intent(inout) :: v(:)
    v = tdf%v
  end subroutine tdfield_get_v_all

  subroutine tdfield_get_v_i(tdf, i, val)
    type(tdfield_t), intent(in) :: tdf
    integer, intent(in) :: i
    real(8), intent(inout) :: val
    val = tdf%v(i)
  end subroutine tdfield_get_v_i

  subroutine tdfield_set_all(tdf, u)
    type(tdfield_t), intent(inout) :: tdf
    real(8), intent(in) :: u(:)
    tdf%u = u
  end subroutine tdfield_set_all

  subroutine tdfield_set_i(tdf, i, val)
    type(tdfield_t), intent(inout) :: tdf
    integer, intent(in) :: i
    real(8), intent(in) :: val
    tdf%u(i) = val
  end subroutine tdfield_set_i

  subroutine tdfield_set_v_all(tdf, v)
    type(tdfield_t), intent(inout) :: tdf
    real(8), intent(in) :: v(:)
    tdf%v = v
  end subroutine tdfield_set_v_all

  subroutine tdfield_set_v_i(tdf, i, val)
    type(tdfield_t), intent(inout) :: tdf
    integer, intent(in) :: i
    real(8), intent(in) :: val
    tdf%v(i) = val
  end subroutine tdfield_set_v_i


  subroutine tdfield_save_n( tdf , filename)
    type(tdfield_t), intent(in) :: tdf(:)
    character(len=*), intent(in) :: filename

    integer :: n, i
    character(len=100) :: filenamex

    n = size(tdf)
    if(n > 1) then
      do i = 1, n
        write(filenamex,'(a,i2.2)') trim(filename), i
        call tdfield_save_1(tdf(i), filenamex)
      end do
    else
      call tdfield_save_1(tdf(1), filename)
    end if

  end subroutine tdfield_save_n


  subroutine tdfield_save_1( tdf , filename)
    type(tdfield_t), intent(in) :: tdf
    character(len=*), intent(in) :: filename

    complex(8), allocatable :: zout(:)
    real(8) :: wk
    integer :: nt, k, iunit, j
    
    open ( unit = 11 , file = trim(filename) , status = 'unknown' )
    select case(tdf_rep)
    case(TDFIELD_REALTIME)
      write(11, '(a)' ) '# rep = realtime'
    case(TDFIELD_FOURIER)
      write(11, '(a)' ) '# rep = fourier'
    case(TDFIELD_CONSTRAINED_FOURIER)
      write(11, '(a)' ) '# rep = constrained_fourier'
    case(TDFIELD_GENERIC)
      write(11, '(a)' ) '# rep = generic'
    end select
    if(tdf_rep .ne. TDFIELD_REALTIME) then
      do k = 1, tdf_nparams
        write(11, '(a1,i8,es18.8)') '#', k, tdf%v(k)
      end do
    end if
    do nt = 0, tdf%nmax
      write(11,'(2(es18.8e3,1x))') nt * tdf%dt, tdf%u(nt)
    end do
    close(11)


    iunit = 11
    open(unit = iunit, file = trim(filename)//'.ps')

    select case(tdf_rep)
    case(tdfield_fourier)
      write(11, '(a)' ) '# rep = fourier'
      allocate(zout(0:tdf_nparams/2))
      zout = cmplx(0.0_8, 0.0_8, 8)
      zout(0) = cmplx(tdf%v(1), 0.0_8, kind = 8)
      k = 2
      do j = 1, tdf_nparams/2
        if(k+1 <= tdf_nparams) then
          zout(j) = cmplx(tdf%v(k), -tdf%v(k+1), kind = 8)
        else
          zout(j) = cmplx(tdf%v(k), 0.0_8, kind = 8)
        end if
        k = k + 2
      end do
      do k = 0, tdf_nparams/2
        wk = 2*pi/(tdf%nmax*tdf%dt)*k
        write(iunit, *) wk, abs(zout(k))**2
      end do
      deallocate(zout)

    case(tdfield_constrained_fourier)
      write(11, '(a)' ) '# rep = fourier'
      allocate(zout(0:tdf_nparams/2+1))
      zout = cmplx(0.0_8, 0.0_8, 8)
      k = 1
      do j = 1, tdf_nparams/2 + 1
        if(j < tdf_nparams / 2 + 1) then
          zout(j) = cmplx(tdf%v(k), -tdf%v(k+1), kind = 8)
        else
          zout(j) = cmplx( -sum(real(zout(1:j-1), kind = 8)), -tdf%v(k), kind = 8)
        end if
        k = k + 2
      end do
      do k = 0, tdf_nparams/2 + 1
        wk = 2*pi/(tdf%nmax*tdf%dt)*k
        write(iunit, *) wk, abs(zout(k))**2
      end do
      deallocate(zout)

    case default
      call tdfield_realtime_to_fourier(tdf, zout)
      do k = 0, tdf%nmax/2
        wk = 2*pi/(tdf%nmax*tdf%dt)*k
        write(iunit, *) wk, abs(zout(k))**2
      end do
      deallocate(zout)

    end select

    close(unit = iunit)
    
  end subroutine tdfield_save_1

  !
  !------------------------------------------------------------------------------------------------
  !
  
  subroutine tdfield_fluence( tdf , flu )

    type(tdfield_t), intent(in) :: tdf(:)
    real(8), intent (inout) :: flu(:)
    
    integer :: n, fmax, k
    real(8) :: oddsum
    
    select case(tdf_rep)
    case(TDFIELD_REALTIME, TDFIELD_GENERIC)
      flu = 0.d0
      do n = 1, size(tdf)
        flu(n) = 0.5_8 * tdf(n)%u(0)**2 + &
          sum(tdf(n)%u(1:tdf(n)%nmax-1)**2) + &
          0.5_8 * tdf(n)%u(tdf(n)%nmax)**2
        flu(n) = flu(n)*tdf(n)%dt
      end do
    case(TDFIELD_FOURIER)
      flu = 0.0_8
      do n = 1, size(tdf)
        flu(n) = tdf(n)%v(1)**2 + 2.0_8 * sum( tdf(n)%v(2:)**2 )
      end do
    case(TDFIELD_CONSTRAINED_FOURIER)
      flu = 0.0_8
      fmax = tdf_nparams/2 + 1
      do n = 1, size(tdf)
        oddsum = 0.0_8
        do k = 1, fmax - 1
          flu(n) = flu(n) + 2.0_8 * tdf(n)%v(2*k-1)**2 + 2.0_8 * tdf(n)%v(2*k)**2
          oddsum = oddsum + tdf(n)%v(2*k-1)
        end do
        flu(n) = flu(n) + 2.0_8 * oddsum**2 + 2.0_8 * tdf(n)%v(2*fmax-1)**2
      end do
    end select
  
  end subroutine tdfield_fluence

  real(8) function tdfield_dfluence(tdf, i) result(dfl)
    type(tdfield_t), intent(inout) :: tdf
    integer, intent(in) :: i

    integer :: fmax, k
    real(8) :: oddsum
    type(tdfield_t) :: dtdf

    select case(tdf_rep)
    case(TDFIELD_REALTIME)
      dfl = 2.0_8 * tdf%v(i) * tdf%dt
    case(TDFIELD_FOURIER)
      if(i == 1) then
        dfl = 2.0_8 * tdf%v(i)
      else
        dfl = 4.0_8 * tdf%v(i)
      end if
    case(TDFIELD_CONSTRAINED_FOURIER)
      dfl = 4.0_8 * tdf%v(i)
      if( (mod(i, 2) .eq. 1) .and. (i .ne. tdf_nparams) ) then
        fmax = tdf_nparams/2 + 1
        oddsum = 0.0_8
        do k = 1, fmax - 1
          oddsum = oddsum + tdf%v(2*k-1)
        end do
        dfl = dfl + 4.0_8 * oddsum
      end if
    case(tdfield_generic)
      call tdfield_init(dtdf, maxntime, tdf%dt)
      call tdfield_der(tdf, i, dtdf)
      dfl = 2.0_8 * tdfield_dotp(tdf, dtdf)
      call tdfield_end(dtdf)
    end select

  end function tdfield_dfluence


  subroutine tdfield_multiply(tdf, alpha)
    type(tdfield_t), intent(inout) :: tdf
    real(8),         intent(in)    :: alpha
    tdf%u = alpha * tdf%u
  end subroutine tdfield_multiply


  real(8) function tdfield_diff(tdf1, tdf2) result(diff)
    type(tdfield_t), intent(in) ::  tdf1, tdf2
    diff = tdf1%dt * sum( (tdf1%u(:) - tdf2%u(:))**2 )
  end function tdfield_diff

  ! Computes the derivative of tdf with respect to the i-th
  ! parameter.
  subroutine tdfield_der(tdf, i, dtdf)
    type(tdfield_t), intent(inout) :: tdf
    integer,         intent(in)    :: i
    type(tdfield_t), intent(inout) :: dtdf

    integer :: j
    real(8) :: t

    select case(tdf%rep)
    case(tdfield_generic)
      dtdf%rep = tdfield_realtime
      call aot_fun_open(L = input_file, fun = tdf%generic_der_function, key = trim(tdf_generic_der_pulse))
      do j = 0, tdf%nmax
        t = j * tdf%dt
        call aot_fun_put(L = input_file, fun = tdf%generic_der_function, arg = tdf%v)
        call aot_fun_put(L = input_file, fun = tdf%generic_der_function, arg = real(i, 8))
        call aot_fun_put(L = input_file, fun = tdf%generic_der_function, arg = t)
        call aot_fun_do(L = input_file, fun = tdf%generic_der_function, nresults = 1)
        call aot_top_get_val(L = input_file, val = dtdf%u(j) , errcode = input_error)
      end do
      call aot_fun_close(L = input_file, fun = tdf%generic_der_function)

    case default
      dtdf%v = 0.0_8
      dtdf%v(i) = 1.0_8
      call tdfield_params_to_realtime(dtdf)
    end select

  end subroutine tdfield_der


  real(8) function tdfield_integral(tdf) result(i)
    type(tdfield_t), intent(in) :: tdf

    integer :: j

    ! This is Simpson's rule, fourth order in dt.
    i = 0.0_8
    do j = 1, tdf%nmax/ 2
      i = i + tdf%u(2*j-2) + 4.0_8 * tdf%u(2*j-1) + tdf%u(2*j)
    end do
    i = i * tdf%dt / 3.0_8

  end function tdfield_integral


  real(8) function tdfield_dotp(tdf1, tdf2) result(z)
    type(tdfield_t), intent(in) :: tdf1, tdf2

    integer :: j

    ! This is the trapezoidal rule, second order in dt.
    !z = 0.5_8 * tdf1%u(0) * tdf2%u(0)
    !do j = 1, tdf1%nmax - 1
    !  z = z + tdf1%u(j)*tdf2%u(j)
    !end do
    !z = z + 0.5_8 * tdf1%u(tdf1%nmax) * tdf2%u(tdf1%nmax)
    !z = z * tdf1%dt

    ! This is Simpson's rule, fourth order in dt.
    z = 0.0_8
    do j = 1, tdf1%nmax/ 2
      z = z + tdf1%u(2*j-2)*tdf2%u(2*j-2) + 4.0_8 * tdf1%u(2*j-1)*tdf2%u(2*j-1) + tdf1%u(2*j)*tdf2%u(2*j)
    end do
    z = z * tdf1%dt / 3.0_8
  end function tdfield_dotp


  subroutine tdfield_realtime_to_fourier(tdf, zout)
    type(tdfield_t), intent(in) :: tdf
    complex(8), allocatable, intent(inout) :: zout(:)

    real(8), allocatable :: in(:)

    allocate(in(0:tdf%nmax-1))
    if(allocated(zout)) deallocate(zout)
    allocate(zout(0:tdf%nmax/2))
    in = tdf%u
    call fft_r2c_1d(in, zout, tdf%nmax)
    ! Next lines are to get the proper normalization of the basis functions.
    zout = zout * tdf%dt / sqrt(tdf%nmax * tdf%dt)
    zout(1:tdf%nmax/2-1) = zout(1:tdf%nmax/2-1) * sqrt(2.0_8)

    deallocate(in)
  end subroutine tdfield_realtime_to_fourier


  subroutine tdfield_get_random_array(array)
  real(8), intent (inout) :: array(:)
    
  integer :: n, i
  real(8) :: r

  n = size(array)

  call aot_get_val(L = input_file, key = 'rng_seed', val = tdf_rng_seed, &
    errcode = input_error, default = 1)

  r = rand(tdf_rng_seed)
  do i = 1, n
    r = rand()
    array(i) = lower_bounds(i) + r * (upper_bounds(i) - lower_bounds(i))
  end do

  end subroutine tdfield_get_random_array


  real(8) function pulse_shape(t0, tau, t, ramp_factor)
    real(8), intent(in) :: t0, tau, t, ramp_factor

    ! This is a box function
    !if( t <= t0+tau/2.0 .and. t>= t0-tau/2.0) then
    !  pulse_shape = 1.0_8
    !else
    !  pulse_shape = 0.0_8
    !end if

    pulse_shape = 0.5_8 * ( erf( ramp_factor * (t - (t0-tau/2)) ) + erf( ramp_factor*((t0+tau/2) - t) ) )

  end function pulse_shape

end module tdfield_m
