!! Copyright (C) 2014-20xx The quote developing team.
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
module hamiltonian_m
  use global_m
  use math_m
  use states_m
  use tdfield_m

  implicit none

  private
  public ::                      &
    hamiltonian_t,               &
    hamiltonian_init,            &
    hamiltonian_end,             &
    hamiltonian_h0,              &
    hamiltonian_v,               &
    hamiltonian_get_eval,        &
    hamiltonian_get_evec,        &
    hamiltonian_print,           &
    hamiltonian_dim_files,       &
    hamiltonian_transform_matrix,&
    hamiltonian_transform_v,     &
    hamiltonian_derivative,      &
    hamiltonian_nfield

  type hamiltonian_t
    private
    integer :: nfield
    integer :: dim ! This is the dimension that the matrices have in the input files.
    type(operator_t) :: h0_
    real(8), allocatable :: eval(:)
    complex(8), allocatable :: evec(:, :)
    type(operator_t), allocatable :: vv(:)
  end type hamiltonian_t


  contains


  subroutine hamiltonian_init(h)
    type(hamiltonian_t), intent(inout) :: h

    complex(8), allocatable :: h0(:, :)

    call aot_get_val(L = input_file, key = 'nfield', val = h%nfield, &
      errcode = input_error, default = 1)

    h%dim = dim
    allocate(h%eval(h%dim))
    allocate(h%evec(h%dim, h%dim))

    call hamiltonian_get_matrices(h)

    allocate(h0(dim, dim))
    call operator_get(h%h0_, h0)
    call eigensolver(h%dim, h0 , h%eval , h%evec )
    deallocate(h0)
    call hamiltonian_print(h, 'output/eigen')

  end subroutine hamiltonian_init


  subroutine hamiltonian_end(h)
    type(hamiltonian_t), intent(inout) :: h
    integer :: k
    call operator_end(h%h0_)
    deallocate(h%eval)
    deallocate(h%evec)
    do k = 1, h%nfield
      call operator_end(h%vv(k))
    end do
    deallocate(h%vv)
  end subroutine hamiltonian_end


  integer function hamiltonian_dim_files(h)
    type(hamiltonian_t), intent(in) :: h
    hamiltonian_dim_files = h%dim
  end function hamiltonian_dim_files

  subroutine hamiltonian_transform_v(h)
    type(hamiltonian_t), intent(inout) :: h

    integer :: n
    character(len=3) :: filename

    if(picture == interaction_picture) then
      do n = 1, h%nfield
        call operator_to_eigenbasis(h%vv(n))
        write(filename, '(a2,i1)' ) 'Vp', n
        call operator_write(h%vv(n), './output/'//trim(filename))
      end do
    end if

  end subroutine hamiltonian_transform_v


  subroutine hamiltonian_get_matrices(h)
    type(hamiltonian_t), intent (inout) :: h

    integer :: n
    character(len=2) :: filename
    logical :: h0_exist
    logical :: v_exist(1:h%nfield)

    inquire( file = './input/H0', exist = h0_exist )
    if ( .not.h0_exist ) then
      print *, 'Error: the program could not find the h0 file. The program will stop'
      stop
    else
      call operator_init(h%h0_, dim, mode = object)
      call operator_read(h%h0_, './input/H0')
    endif

    if(h%nfield .eq. 1) then

      allocate(h%vv(1))
      call operator_init(h%vv(1), h%dim, mode = object)
      inquire( file = './input/V', exist = v_exist(1) )
      if ( .not. v_exist(1) ) then
        write(*, *) 'The program could not find the V file'
      endif
      call operator_read(h%vv(1), './input/V')
   
    else

      allocate(h%vv(h%nfield))
      do n = 1, h%nfield
        call operator_init(h%vv(n), h%dim, mode = object)
      end do
      do n = 1, h%nfield
        write(filename, '(a1,i1)' ) 'V', n
        inquire( file = 'input/'//filename, exist = v_exist(n) )
        if ( .not. v_exist(n) ) then
          write(*, *) 'The program could not find the '//filename//' file'
        else
          call operator_read(h%vv(n), 'input/'//trim(filename))
        endif
      end do
      if ( any( .not. v_exist ) ) then
        print *, 'Error: the program could not find'
        print *, 'some of the "Vx" files. The program will stop'
        stop
      endif
    end if

  end subroutine hamiltonian_get_matrices


  function hamiltonian_h0(h) result(h0)
    type(hamiltonian_t), target :: h
    type(operator_t), pointer :: h0
    h0 => h%h0_
  end function hamiltonian_h0


  function hamiltonian_v(h, j) result(v)
    type(hamiltonian_t), target :: h
    type(operator_t), pointer :: v
    integer :: j
    v => h%vv(j)
  end function hamiltonian_v


  function hamiltonian_get_eval(h) result(eval)
    real(8), pointer :: eval(:)
    type(hamiltonian_t), target :: h
    eval => h%eval
  end function hamiltonian_get_eval


  function hamiltonian_get_evec(h) result(evec)
    complex(8), pointer :: evec(:, :)
    type(hamiltonian_t), target :: h
    evec => h%evec
  end function hamiltonian_get_evec


  subroutine hamiltonian_print( h, filename )
    type(hamiltonian_t), intent(in) :: h
    character(len=*), intent(in) :: filename

    integer :: j, iunit

!!$    !Let us check that everything is allright
!!$    complex(8), allocatable :: diag(:, :), aux(:, :)
!!$
!!$    allocate(diag(dim, dim))
!!$    allocate(aux(dim, dim))
!!$    diag = cmplx(0.0_8, 0.0_8, 8)
!!$    do j = 1, dim
!!$      diag(j, j) = h%eval(j)
!!$    end do
!!$    aux = matmul(conjg(transpose(h%evec)), matmul(h%h0, h%evec))
!!$    write(*, *) 'Error in the diagonalization:', maxval(abs(aux-diag))
!!$    write(*, *) (/ (aux(j, j), j = 1, dim ) /)
!!$    write(*, *)
!!$    aux = matmul(conjg(transpose(h%evec)), h%evec)
!!$    do j = 1, dim
!!$      diag(j, j) = cmplx(1.0_8, 0.0_8, 8)
!!$    end do
!!$    write(*, *) 'Error int the unitarity:', maxval(abs(aux-diag))
!!$    stop
!!$    deallocate(diag)
!!$    deallocate(aux)


    iunit = 15
    open(unit = iunit, file = trim(filename))
    do j = 1, dim
      write(iunit, *) h%eval(j), h%evec(1:h%dim, j)
    end do
    close(unit = iunit)

  end subroutine hamiltonian_print


  subroutine hamiltonian_transform_matrix(a, h)
    complex(8), intent(inout) :: a(:, :)
    type(hamiltonian_t), intent(in) :: h

    select case(picture)
    case(interaction_picture)
      a = matmul(conjg(transpose(h%evec)), matmul(a, h%evec))
    end select
  end subroutine hamiltonian_transform_matrix


  subroutine hamiltonian_derivative(h, tdf, k, st, stout)
    type(hamiltonian_t), intent(in) :: h
    type(tdfield_t), intent(inout) :: tdf
    integer, intent(in) :: k
    type(states_t), intent(in) :: st(0:maxntime)
    type(states_t), intent(inout) :: stout(0:maxntime)

    integer :: nt
    real(8) :: t, val
    type(tdfield_t) :: dtdf

    type(operator_t) :: vp

    call tdfield_init(dtdf, maxntime, dt)
    call tdfield_der(tdf, k, dtdf)

    do nt = 0, maxntime
      t = nt*dt
      vp = h%vv(1)
      if( picture .eq. interaction_picture ) then
        call operator_to_interaction(vp, t)
      end if
      call tdfield_get(dtdf, nt, val)
      call states_zgemv_( cmplx(val, 0.0_8, 8), vp, st(nt), cmplx(0.0_8, 0.0_8, 8), stout(nt) )
    end do

    call tdfield_end(dtdf)
  end subroutine hamiltonian_derivative


  integer function hamiltonian_nfield(h)
    type(hamiltonian_t), intent(in) :: h
    hamiltonian_nfield = h%nfield
  end function hamiltonian_nfield


end module hamiltonian_m
