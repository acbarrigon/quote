!! Copyright (C) 2014-20xx The quote developing team.
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.


module target_m
  use aotus_module
  use global_m
  use input_m
  use observables_m
  use hamiltonian_m
  use math_m
  use states_m

  implicit none

  private
  public ::             &
    target_init,        &
    target_end,         &
    target_j1,          &
    target_calculate_bc

  public :: target_u, target_op


  integer, parameter ::       &
    qoct_target_occup    = 1, &
    qoct_target_operator = 3, &
    qoct_target_u        = 4

  integer :: target_type
  integer :: target_observable
  type(operator_t) :: target_op
  type(states_t)   :: target_u

  interface target_j1
    module procedure target_j1_st
  end interface target_j1

  interface target_calculate_bc
    module procedure target_calculate_bc_st
  end interface target_calculate_bc


  contains


  subroutine target_init(h, n, ierr)
    type(hamiltonian_t), intent(in) :: h
    integer, intent(in)  :: n
    integer, intent(out) :: ierr

    integer :: i, j, dim_file
    logical :: file_exists
    character(len=100) :: operator_filename
    type(states_t) :: target_st
    complex(8), allocatable :: target_states(:)
    complex(8), allocatable :: targetmatrix(:, :)
    complex(8), pointer :: evec(:, :)
    real(8), pointer :: eval(:)

    target_observable = 0
    call aot_get_val(L = input_file, key = 'target_type', val = target_type, &
      errcode = input_error, default = -1)
    allocate ( targetmatrix(1:n, 1:n) )

    if( (object .eq. evolution_operator) .and. (target_type .ne. QOCT_TARGET_U) ) then
      write(*, *) 'When the object is the evolution operator, target_type must be qoct_target_u.'
      ierr = -1
      return
    end if

    select case(target_type)

    case(qoct_target_u)

      call aot_get_val(L = input_file, key = 'utarget', val = operator_filename, &
        errcode = input_error, default = '')
      if(operator_filename == '') then
        write(*, '(a)') 'You must suppy a target file name.'
        ierr = -1
      end if
      inquire( file = './input/'//trim(operator_filename), exist = file_exists )
      if ( .not. file_exists ) then
        write(*, '(a)') 'Error: Could not find evolution operator target file ./input/'//trim(operator_filename)
        stop
      else
        call states_init(target_u, dim, mode = evolution_operator)
        call states_read(target_u, filename = './input/'//trim(operator_filename))
      end if
      ierr = 0

      call states_get(target_u, targetmatrix)
      if(.not. check_unitarity(targetmatrix, 1.0e-8_8)) then
        stop 'Target evolution operator is not unitary'
      end if

      if(picture .eq. schrodinger_picture) then
        call states_to_schroedinger(target_u, maxtime)
      end if

    case(QOCT_TARGET_OCCUP)
      allocate ( target_states(1:n) )
      target_states = (0.0_8, 0.0_8)
      allocate(input_error_array(2*n))
      call aot_get_zval(L = input_file, key = 'target_states', val = target_states, &
        errcode = input_error_array, default = target_states)
      deallocate(input_error_array)
      target_states = target_states/sqrt(dot_product(target_states, target_states))
      call states_init(target_st, n, mode = wavefunction)
      call states_set(target_st, target_states)
      call states_write(target_st, './output/target_coeffs')

      if( picture .eq. schrodinger_picture ) then
        if(initial_state == interaction_picture) then
          evec => hamiltonian_get_evec(h)
          target_states = matmul(evec(1:dim, 1:dim), target_states)
          nullify(evec)
        end if
      end if

      do i = 1, dim
        do j = 1, dim
          targetmatrix(i, j) = conjg(target_states(j))*target_states(i)
        end do
      end do

      call operator_init(target_op, dim, mode = object)
      call operator_set(target_op, targetmatrix)
      if(picture .eq. interaction_picture) then
        call operator_to_interaction(target_op, maxtime)
      end if

      call states_end(target_st)
      deallocate(target_states)
      ierr = 0

    case(QOCT_TARGET_OPERATOR)
      call aot_get_val(L = input_file, key = 'target_operator', val = operator_filename, &
        errcode = input_error, default = '')
      if(operator_filename == '') then
        write(*, '(a)') 'You must suppy a target file name.'
        ierr = -1
      end if
      target_observable = observables_number(operator_filename)
      if(target_observable == 0) then
        write(*, '(a)') 'Target file '//trim(operator_filename)//' not found.'
        ierr = -1
      end if

      target_op = observables(target_observable)%op
      if(picture .eq. interaction_picture) then
        call operator_to_interaction(target_op, maxtime)
      end if

      ierr = 0

    case default
      ierr = -1
    end select

    deallocate(targetmatrix)
  end subroutine target_init


  subroutine target_calculate_bc_st(h, stb, st)
    type(hamiltonian_t), intent(in) :: h
    type(states_t), intent(in) :: st
    type(states_t), intent(inout) :: stb

    complex(8), allocatable :: cc(:), dd(:), u(:, :), b(:, :)
    select case(states_mode(st))
    case(wavefunction)
      call states_zgemv_( (1.0_8, 0.0_8), target_op, st, (0.0_8, 0.0_8), stb)
    case(evolution_operator)
      call states_end(stb)
      call states_init(stb, dim, mode = evolution_operator)
      call states_zaxpy( states_dotp(target_u, st), target_u, stb)
    end select
  end subroutine target_calculate_bc_st


  subroutine target_end()
    call states_end(target_u)
    call operator_end(target_op)
  end subroutine target_end


  real(8) function target_j1_st(h, st) result(x)
    type(hamiltonian_t), intent(in) :: h
    type(states_t), intent(in) :: st

    complex(8) :: z

    select case(states_mode(st))
    case(wavefunction)
      x = real( operator_expvalue(target_op, st), 8)
    case(evolution_operator)
      z = states_dotp(st, target_u)
      x = abs(z)**2
    end select
  end function target_j1_st


end module target_m
