Test       : Hubbard dimer
Program    : quote
TestGroups : all
Enabled    : Yes

# This test corresponds with the Hubbard dimer run described in [Fuks & Maitra,
# 2014 16, 14504 (2014)]. Specifically, the td runs presented below reproduce
# the charge transfer shown in Fig. 4.

Precision: 1.0e-6


# TD runs.

# Schrödinger picture. Taylor expansion propagator.
Input: 01-hubbard.inp
match ; <Dn>(T) ; LINEFIELD(output/td-Dn, -1, 2);  0.222911665502E+000

# Same, with RK.
Input: 02-hubbard.inp
match ; <Dn>(T) ; LINEFIELD(output/td-Dn, -1, 2);  0.223146329204E+000

# Same, with CFM4
Input: 03-hubbard.inp
match ; <Dn>(T) ; LINEFIELD(output/td-Dn, -1, 2);  0.223145883596E+000


# Interaction picture. Taylor expansion propagator.
Input: 04-hubbard.inp
match ; <Dn>(T) ; LINEFIELD(output/td-Dn, -1, 2);  0.223142743585E+000

# Interaction picture. RK.
Input: 05-hubbard.inp
match ; <Dn>(T) ; LINEFIELD(output/td-Dn, -1, 2);  0.223145846391E+000

# Interaction picture. CFM4
Input: 06-hubbard.inp
match ; <Dn>(T) ; LINEFIELD(output/td-Dn, -1, 2);  0.223145851981E+000




# Optimization runs

# Schrödinger picture, Taylor expansion, qoct_algorithm_nlopt_ld_lbfgs
Input: 07-hubbard.inp
# The optimization tests seem to be rather sensible, so we reduce the tolerance.
Precision: 1.0e-3
match ; Final J ; GREPFIELD(out, 'Final J ', 4); 0.61795810905387238

# Then, an optimization run. Schrödinger picture, RK, qoct_algorithm_nlopt_ld_lbfgs
Input: 08-hubbard.inp
Precision: 1.0e-3
match ; Final J ; GREPFIELD(out, 'Final J ', 4); 0.61802941929058042

# Then, an optimization run. Interaction picture, Taylor expansion, qoct_algorithm_nlopt_ld_lbfgs
Input: 09-hubbard.inp
Precision: 1.0e-3
match ; Final J ; GREPFIELD(out, 'Final J ', 4); 0.61802961655344790

# Then, an optimization run. Interaction picture, RK, qoct_algorithm_nlopt_ld_lbfgs
Input: 10-hubbard.inp
Precision: 1.0e-3
match ; Final J ; GREPFIELD(out, 'Final J ', 4); 0.61802997134072213




