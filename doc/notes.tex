\documentclass[a4paper,12pt]{article}


%\usepackage[dvips]{color}
\usepackage{framed}
\usepackage{pslatex}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsbsy}
\usepackage{amsfonts}
\usepackage{mathrsfs}
\usepackage{cancel}
\usepackage[normalem]{ulem}
\usepackage{fullpage}
\usepackage{bm}


\begin{document}

\title{Equations in the {\rm quote} code}


\author{}


\maketitle

\begin{abstract}
The following is a brief description of the key equations implemented in {\tt quote}.
\end{abstract}

\section{Model}

The program {\tt quote} considers the evolution of a quantum system. This may be
described:
\begin{enumerate}

\item In terms of the linear equation for the Hilbert space state $c(t)$,
\begin{eqnarray}
i \frac{\rm d}{{\rm d}t} \vert c(t)\rangle & = & \left( \hat{H}_0 + f(u, t)\hat{V}
\right)\vert c(t)\rangle\,,
\\
\vert c(0)\rangle & = & \vert c_0\rangle\,.
\end{eqnarray}

\item In terms of the linear equation for the evolution operator $\hat{U}(t,t')$:
\begin{eqnarray}
i \frac{\rm d}{{\rm d}t} \hat{U}(t,t') & = & \left( \hat{H}_0 + f(u, t)\hat{V}
\right)\hat{U}(t,t')\,,
\\
\hat{U}(t,t) & = & \hat{I}\,,
\end{eqnarray}
which is defined as:
\begin{equation}
\vert c(t)\rangle = \hat{U}(t,t')\vert c(t_0)\rangle\,.
\end{equation}
for any arbitrary times $t,t'$. When t' = 0, the initial time for the propagation,
we will use the simplified notation $\hat{U}(t,0) \equiv \hat{U}(t)$.

\item In terms of the density matrix $\hat{\rho}$, using Lindblad's equation:

{\em MISSING: Lindblad's equation}

{\em NOTE: This option is not yet implemented in {\tt quote}.}

\end{enumerate}

The real-valued function $f(u,t)$ depends on
a set of ``control parameters'' $u_1,\dots,u_P$, a set of $P$ real
numbers. The specification of these parameters determines the evolution of the
system, $c(t) = c[u](t)$, $\hat{U}(t) = \hat{U}[u](t)$, or $\hat{\rho}(t)=\hat{\rho}[u](t)$. In the following,
we will use $X$ to denote indistinctly $c$, $\hat{U}$, or $\hat{\rho}$.
The problem addressed by {\tt quote} is the maximization of a function:
\begin{equation}
\label{eq:g}
G(u) = F(X[u], u)\,,
\end{equation}
where $F$ is the ``target functional''; it may be rather general, but in
{\tt quote}, right now it may only have the form:
\begin{equation}
F(X[u], u) = J_1(X[u](T)) + J_2(u)\,.
\end{equation}
The first term $J_1$ encodes the ``real'' target that is pursued, and note that we restrict the
problem to ``terminal'' targets, i.e. targets that depend only on the state of
the system at the final propagation time $T$. The second term $J_2$ is
optional and is called the ``penalty'', used to favour or penalize regions
of the parameter search space.

In {\tt quote}, one may work in the interaction picture. This implies working with the
transfomed state:
\begin{equation}
\vert \tilde{c}(t) \rangle = e^{it\hat{H}_0}\vert c(t)\rangle\,,
\end{equation}
that evolves according to:
\begin{eqnarray}
i \frac{\rm d}{{\rm d}t} \vert \tilde{c}(t)\rangle & = & f(u, t)\hat{\tilde{V}}(t)
\vert \tilde{c}(t)\rangle\,,
\\
\vert \tilde{c}(0)\rangle & = & \vert \tilde{c}_0\rangle = \vert c_0\rangle \,,
\end{eqnarray}
$\hat{\tilde{V}}(t)$ is the operator $\hat{V}$ in the interaction picture. Any operator
$\hat{O}$ in the previous Schr{\"{o}}dinger picture transforms into the interaction picture as
\begin{equation}
\hat{\tilde{O}}(t) = e^{it\hat{H}_0}\hat{O}e^{-it\hat{H}_0}\,.
\end{equation}
This implies:
\begin{equation}
\langle c(t) \vert \hat{O}\vert c(t) \rangle = 
\langle \tilde{c}(t) \vert \hat{\tilde{O}}(t)\vert \tilde{c}(t) \rangle\,.
\end{equation}

The evolution operator may also be defined in the interaction picture:
\begin{equation}
\vert \tilde{c}(t)\rangle = \hat{\tilde{U}}(t,t')\vert \tilde{c}(t')\rangle\,.
\end{equation}
It is related to the evolution operator in the Schr{\"{o}}dinger picture by:
\begin{equation}
\hat{\tilde{U}}(t,t') = e^{it\hat{H}_0}\hat{U}(t,t')e^{-it'\hat{H}_0}\,,
\end{equation}
and its equation of motion is:
\begin{eqnarray}
i \frac{\rm d}{{\rm d}t} \hat{\tilde{U}}(t,t') & = & f(u, t)\hat{\tilde{V}}(t)
\hat{\tilde{U}}(t,t'),
\\
\hat{\tilde{U}}(t,t) & = & \hat{I}\,.
\end{eqnarray}

We end this section with the definition of the dot product for operators used
in {\tt quote}, the Fr{\"{o}}benius dot product:
\begin{equation}
\hat{A}\cdot\hat{B} = \frac{1}{d}{\rm Tr}[\hat{A}^\dagger\hat{B}]\,.
\end{equation}


\section{Representations of the model}

The {\tt quote} code may work in two different representations:
\begin{enumerate}

\item using the Schr{\"{o}}dinger picture, in the basis
implied by the user in the definition of the input matrices for the $\hat{H}_0$ and $\hat{V}$ operators
(that we will call basis $\mathcal{B}$), and

\item using the interaction picture, in the basis of eigenstates of $\hat{H}_0$ (basis $\mathcal{B}'$).

\end{enumerate}

Denoting by ${\bf c}(t)$ the coordinates of the $\vert c(t)\rangle$ state
in the original $\mathcal{B}$ basis, the
equations of motion are:
\begin{eqnarray}
i \frac{\rm d}{{\rm d}t} {\bf c}(t) & = & \left( {\bf H}_0 + f(u, t){\bf V}
\right){\bf c}(t)\,,
\\
{\bf c}(0) & = & {\bf c}_0\,,
\end{eqnarray}
where ${\bf H}_0$ and ${\bf V}$ are the matrices representing the $\hat{H}_0$ and
$\hat{V}$ operators in the same basis. The equations of motion
for the corresponding evolution operator matrix are:
\begin{eqnarray}
i \frac{\rm d}{{\rm d}t} {\bf U}(t) & = & \left( {\bf H}_0 + f(u, t){\bf V}
\right){\bf U}(t)\,,
\\
{\bf U}(0) & = & {\bf I}\,.
\end{eqnarray}

In practice, if the field-free Hamiltonian ${\bf H}_0$ can be diagonalized, it
is better to move to the eigenbasis. The diagonalization is achieved with
a unitary matrix ${\bf M}$, such that:
\begin{equation}
\pmb{\epsilon} = {\bf M}^\dagger
{\bf H}_0 {\bf M}\,,
\end{equation}
where $\pmb{\epsilon}$ is the diagonal matrix formed by the eigenvalues $(\epsilon_1,\dots,\epsilon_n)$
of ${\bf H}_0$. This may also written as:
\begin{equation}
{\bf H}_0 {\bf M} = {\bf M}\;\pmb{\epsilon}\,.
\end{equation}
The columns of the matrix ${\bf M}$, ${\bf u}^i$, are the eigenvectors of ${\bf H}_0$ in the original basis:
\begin{equation}
{\bf H}_0\;{\bf u}^i = \epsilon_i {\bf u}^i\,.
\end{equation}

If ${\bf c}$ are the coefficients of some state in the original basis $\mathcal{B}$, the coefficients
${\bf c}'$ in the eigenbasis ${\mathcal B}'$ are given by:
\begin{equation}
{\bf c}' = {\bf M}^\dagger{\bf c}\,.
\end{equation}
Likewise, the representation of any operator ${\bf O}$ transforms as:
\begin{equation}
{\bf O}' = {\bf M}^\dagger {\bf O} {\bf M}\,.
\end{equation}
(for example it becoms clear that ${\bf H}'_0 = \pmb{\epsilon}$). 
This also holds for the evolution operator:
\begin{equation}
{\bf U}'(t) = {\bf M}^\dagger {\bf U}(t) {\bf M}\,.
\end{equation}

The equations of motion transform now to:
\begin{eqnarray}
i \frac{\rm d}{{\rm d}t} {\bf c}'(t) & = & \left( \pmb{\epsilon} + f(u,t){\bf V}'
\right){\bf c}'(t)\,,
\\
{\bf c}'(0) & = & {\bf c}'_0\,.
\end{eqnarray}
or
\begin{eqnarray}
i \frac{\rm d}{{\rm d}t} {\bf U}'(t) & = & \left( \pmb{\epsilon} + f(u,t){\bf V}'
\right){\bf U}'(t)\,,
\\
{\bf U}'(0) & = & {\bf I}\,.
\end{eqnarray}

However, {\tt quote} does not solve these equations, since, as stated above, when
working in the eigenbasis it also works in the interaction picture. Therefore,
we are concerned with the equations of motion for
\begin{equation}
\tilde{\bf c}'(t) = e^{it\pmb{\epsilon}}{\bf c}'(t)\,,
\end{equation}
and for
\begin{equation}
\tilde{\bf U}'(t) = e^{it\pmb{\epsilon}}{\bf U}'(t)\,.
\end{equation}
These equations of motion are:
\begin{eqnarray}
i \frac{\rm d}{{\rm d}t} {\bf \tilde{c}}'(t) & = &  f(u,t){\bf \tilde{V}}'(t)
{\bf \tilde{c}}'(t)\,,
\\
{\bf \tilde{c}}'(0) & = & {\bf \tilde{c}}'_0\,.
\end{eqnarray}
and
\begin{eqnarray}
i \frac{\rm d}{{\rm d}t} {\bf \tilde{U}}'(t) & = & f(u,t){\bf \tilde{V}}'(t)
{\bf \tilde{U}}'(t)\,,
\\
{\bf \tilde{U}}'(0) & = & {\bf I}\,,
\end{eqnarray}
where
\begin{equation}
\tilde{\bf V}'(t) = e^{it\pmb{\epsilon}}{\bf V}'e^{-it\pmb{\epsilon}}\,.
\end{equation}






\section{QOCT equations}


In these section, we state the ``QOCT equations'', by which we mean the equations
of the gradient of the function $G$ given in Eq.~(\ref{eq:g}).

\subsection{Schr{\"{o}}dinger picture, wavefunction}

Let us first recall the equations of motion that are to be manipulated:
\begin{eqnarray}
i \frac{\rm d}{{\rm d}t} {\bf c}(t) & = & \left( {\bf H}_0 + f(u, t){\bf V}
\right){\bf c}(t)\,,
\\
{\bf c}(0) & = & {\bf c}_0\,,
\end{eqnarray}
Given a function $G$ defined as:
\begin{equation}
G(u) = F({\bf c}[u], u) = J_1({\bf c}[u](T)) + J_2(u)\,,
\end{equation}
the gradient is given by:
\begin{equation}
\frac{\partial G}{\partial u_m}(u) = \frac{\partial J_2}{\partial u_m}(u) +
2{\rm Im} \int_0^T\!\!{\rm d}t\; 
\frac{\partial f}{\partial u_m}(u,t)
{\bf d}^\dagger[u](t)
{\bf V} {\bf c}[u](t)\,.
\end{equation}
The ``costate'' ${\bf d}[u](t)$ can be obtained by solving the
following equations of motion:
\begin{eqnarray}
i \frac{\rm d}{{\rm d}t} {\bf d}[u](t) & = & {\bf H}^\dagger(u,t)
{\bf d}[u](t)\,,
\\
{\bf d}[u](T) & = & \frac{\partial J_1}{\partial {\bf c}^\dagger[u](T)} \,.
\end{eqnarray}
Usually,
\begin{equation}
J_1({\bf c}) = {\bf c}^\dagger {\bf O} {\bf c}\,,
\end{equation}
and the last final-time condition is simply:
\begin{equation}
{\bf d}[u](T) = {\bf O}{\bf c}[u](T)\,.
\end{equation}

\subsection{Schr{\"{o}}dinger picture, evolution operator}

In this case, the equations of motion are:
\begin{eqnarray}
i \frac{\rm d}{{\rm d}t} {\bf U}(t) & = & \left( {\bf H}_0 + f(u, t){\bf V}
\right){\bf U}(t)\,,
\\
{\bf U}(0) & = & {\bf I}\,.
\end{eqnarray}
Given a function $G$ defined as:
\begin{equation}
G(u) = F({\bf U}[u], u) = J_1({\bf U}[u](T)) + J_2(u)\,,
\end{equation}
the gradient is given by:
\begin{equation}
\frac{\partial G}{\partial u_m}(u) = \frac{\partial J_2}{\partial u_m}(u) +
2{\rm Im} \int_0^T\!\!{\rm d}t\; 
\frac{\partial f}{\partial u_m}(u,t)
{\bf B}^\dagger[u](t)\cdot
{\bf V} {\bf U}[u](t)\,.
\end{equation}
In this case, the equations of motion for the costate ${\bf B}[u](t)$ are:
\begin{eqnarray}
i \frac{\rm d}{{\rm d}t} {\bf B}[u](t) & = & {\bf H}^\dagger(u,t)
{\bf B}[u](t)\,,
\\
{\bf B}[u](T) & = & \frac{\partial J_1}{\partial {\bf U}^\dagger[u](T)} \,.
\end{eqnarray}
In a typical case:
\begin{equation}
J_1({\bf U}) = \vert {\bf O}\cdot {\bf U}\vert^2\,,
\end{equation}
and therefore
\begin{equation}
{\bf B}[u](T) = ({\bf O}\cdot{\bf U}[u](T)){\bf O}\,.
\end{equation}


\subsection{Interaction picture, wavefunction}

The equations of motion are in this case:
\begin{eqnarray}
i \frac{\rm d}{{\rm d}t} {\bf \tilde{c}}'(t) & = &  f(u,t){\bf \tilde{V}}'(t)
{\bf \tilde{c}}'(t)\,,
\\
{\bf \tilde{c}}'(0) & = & {\bf \tilde{c}}'_0\,.
\end{eqnarray}

The function $G$ is defined as:
\begin{equation}
G(u) = F'({\bf \tilde{c}}'[u], u) = J'_1({\bf \tilde{c}}'[u](T)) + J_2(u)\,,
\end{equation}
The function $J'_1$ may be related to the previous definition in the Schr{\"{o}}dinger picture:
\begin{equation}
J'_1({\bf \tilde{c}}') = J_1({\bf M}e^{-iT\pmb{\epsilon}}\tilde{\bf {c}}')\,.
\end{equation}
If, as before, $J_1({\bf c}) = {\bf c}^\dagger{\bf O}{\bf c}$, then:
\begin{equation}
J'_1(\tilde{\bf c}') = \tilde{\bf c}'^\dagger e^{iT\pmb{\epsilon}}{\bf M}^\dagger {\bf O} {\bf M}
e^{-iT\pmb{\epsilon}}\tilde{\bf c}' = 
\tilde{\bf c}'^\dagger \tilde{\bf O}'(T)
\tilde{\bf c}'
\,.
\end{equation}


The gradient is given by:
\begin{equation}
\frac{\partial G}{\partial u_m}(u) = \frac{\partial J_2}{\partial u_m}(u) +
2{\rm Im} \int_0^T\!\!{\rm d}t\; 
\frac{\partial f}{\partial u_m}(u,t)
\tilde{\bf d}'^\dagger[u](t)
\tilde{\bf V}'(t) \tilde{\bf c}'[u](t)\,.
\end{equation}
The ``costate'' $\tilde{\bf d}'[u](t)$ can be obtained by solving the
following equations of motion:
\begin{eqnarray}
i \frac{\rm d}{{\rm d}t} \tilde{\bf d}'[u](t) & = & f(u,t)\tilde{\bf V}^\dagger(t)
\tilde{\bf d}'[u](t)\,,
\\
\tilde{\bf d}'[u](T) & = & \frac{\partial J'_1}{\partial \tilde{\bf c}'^\dagger[u](T)} \,.
\end{eqnarray}
In the usual case,
\begin{equation}
{\bf d}[u](T) = \tilde{\bf O}'(T)\tilde{\bf c}'[u](T)\,.
\end{equation}



\subsection{Interaction picture, evolution operator}

Finally, the equations of motion are now:
\begin{eqnarray}
i \frac{\rm d}{{\rm d}t} {\bf \tilde{U}}'(t) & = & f(u,t){\bf \tilde{V}}'(t)
{\bf \tilde{U}}'(t)\,,
\\
{\bf \tilde{U}}'(0) & = & {\bf I}\,,
\end{eqnarray}

The function to optimize is now
\begin{equation}
G(u) = F'(\tilde{\bf U}'[u], u) = J'_1(\tilde{\bf U}'[u](T)) + J_2(u)\,,
\end{equation}
If we once again have the typical case $J_1({\bf U}) = \vert {\bf O}\cdot {\bf U}\vert^2$,
then
\begin{equation}
J'_1(\tilde{\bf U}') = \vert \tilde{\bf O}'(T)\cdot \tilde{\bf U}(T)\vert^2\,.
\end{equation}

The gradient is now given by:
\begin{equation}
\frac{\partial G}{\partial u_m}(u) = \frac{\partial J_2}{\partial u_m}(u) +
2{\rm Im} \int_0^T\!\!{\rm d}t\; 
\frac{\partial f}{\partial u_m}(u,t)
\tilde{\bf B}'^\dagger[u](t)\cdot
\tilde{\bf V}'(t) \tilde{\bf U}'[u](t)\,.
\end{equation}
In this case, the equations of motion for the costate ${\bf B}[u](t)$ are:
\begin{eqnarray}
i \frac{\rm d}{{\rm d}t} \tilde{\bf B}'[u](t) & = & f(u,t)\tilde{\bf V}'^\dagger(t)
\tilde{\bf B}'[u](t)\,,
\\
\tilde{\bf B}'[u](T) & = & (\tilde{\bf O}'(T)\cdot\tilde{\bf U}'[u](T))\tilde{\bf O}'(T)\,.
\end{eqnarray}




\section{Parametrizations}

We consider a control function $f$ defined on the propagation interval
$[0,T]$.  The function is discretized in this time interval as:
\begin{eqnarray}
f_j & = & f(t_j)
\\
t_j & = & hj (j=0,1,\dots,N)
\\
T & = & Nh
\end{eqnarray}

The function can then be defined by the specification of $P$ parameters
$u_1,u_2,\dots,u_P\equiv u$: $f=f(u,t)$. In order to implement the equations
of QOCT, one needs the derivative functions:
\begin{equation}
\nonumber
\frac{\partial f}{\partial u_m}(u,t)\,.
\end{equation}
These may or may not belong to the class of functions that can be
parametrized:
i.e. there may be a set of parameters $u^0$ for which:
\begin{equation}
\frac{\partial f}{\partial u_m}(u,t) = f(u^0,t)\,.
\end{equation}
but, depending on the parametrization, it may happen that there is not such set of
parameters. 



\subsection{\label{sec:fourier} Parametrization: Fourier expansion}


We may start considering the 
expansion of the functions in a Fourier series, and consider the coefficients as the
parameters, or alternatively use a set of parameters to define the Fourier coefficients.

Note that once we assume a Fourier expansion, the function is automatically
periodic, i.e. it can be considered to be defined outside the original
interval $[0,T]$, and:
\begin{equation}
f(t+T) = f(t) \text{ for any } t\,,
\end{equation}
and 
\begin{equation}
f_{j+N}=f_j\,.
\end{equation}
In particular:
\begin{equation}
f_N = f_0\,.
\end{equation}

The Fourier expansion is defined through the following identities:
\begin{eqnarray}
\label{eq:fs1}
f_j & = & \frac{1}{\sqrt{T}} \sum_{k=0}^{N-1} c_k
e^{i\frac{2\pi}{N}kj}\,,
\\
c_k & = & \frac{h}{\sqrt{T}}\sum_{j=0}^{N-1} f_j e^{-i\frac{2\pi}{N}kj}\,.
\end{eqnarray}

The coefficients $c_k$ are also periodic:
\begin{equation}
c_{k+N}=c_k\,.
\end{equation}

The values of the function at arbitrary points $t$ can then be \emph{Fourier
  interpolated} through the formula:
\begin{equation}
f(t) = \frac{1}{\sqrt{T}} \sum_{k=0}^{N-1} c_k e^{i\frac{2\pi}{T}kt}\,.
\end{equation}

Since the function $f$ is real, the following holds:
\begin{eqnarray}
c_{-k} &= & c^*_k\,,
\\
c_0 & = & c_0^*\,,
\\
c_{N/2} & = & c_{N/2}^*\,,
\\
c_{N/2+l} & = & c^*_{N/2-l}\;\;\;(l=1,2,\dots,N/2-1)\,.
\end{eqnarray}
The last relation means that one only needs to compute the
$c_0,c_1,\dots,c_{N/2}$ (complex) Fourier coefficients, since the rest can be
obtained from them. This fact is used by the FFTW library in its
\emph{real-to-complex} and \emph{complex-to-real} procedures. The previous two
identities mean that the two coefficients $c_0$ and $c_{N/2}$ are real
numbers.

Since we want to use real numbers, it is better to transform the previous
exponential sum [Eq.~(\ref{eq:fs1})] into a sines and cosines Fourier series:
\begin{equation}
f_j = \frac{1}{\sqrt{T}}c_0 + \frac{2}{\sqrt{T}}\sum_{k=1}^{N/2}{\rm Re}c_k \cos \omega_kt_j
+ \frac{2}{\sqrt{T}}\sum_{k=2}^{N/2-1} (-{\rm Im}c_k) \sin \omega_kt_j
\end{equation}
The frequencies $\omega_k$ are given by:
\begin{equation}
\omega_k = \frac{2\pi}{T}k\,.
\end{equation}

In practice, we will truncate this series up to a frequency $\omega_M$. In
principle, the maximum possible frequency is $\omega_{N/2}$; however in that
case the number of degrees of freedom in Fourier space would equal the number
of degrees of freedom in real space ($N$), and therefore, there is no gain in
using Fourier space. Therefore, we will assume that always:
\begin{equation}
M \le N/2-1\,.
\end{equation}
In this way, there is some simplications in the expressions because we will
have the same number of ``cosine'' and ``sine'' terms:
\begin{equation}
f(t) = \frac{1}{\sqrt{T}}c_0 + \frac{2}{\sqrt{T}}\sum_{k=1}^{M}{\rm Re}c_k \cos \omega_kt
+ \frac{2}{\sqrt{T}}\sum_{k=1}^{M} (-{\rm Im}c_k) \sin \omega_kt\,.
\end{equation}
We can use $P=2M+1$ real parameters $u\equiv u_1,\dots,u_P$ to determine the
Fourier coefficients, as:
\begin{eqnarray}
\nonumber
{\rm Re} c_0 & = & u_1\,,
\\\nonumber
{\rm Re} c_1 & = & u_2\,,
\\\nonumber
{\rm Im} c_1 & = & - u_3\,,
\\\nonumber
\dots & = & \dots
\\\nonumber
{\rm Re} c_l & = & u_{2l} \,,
\\\nonumber
{\rm Im} c_l & = & - u_{2l+1}\,,
\\\nonumber
\dots & = & \dots
\\\nonumber
{\rm Re} c_M & = &  u_{2M} \,,
\\
{\rm Im} c_M  & = & - u_{2M+1}\,.
\end{eqnarray}
Therefore:
\begin{equation}
f(u,t) = \frac{1}{\sqrt{T}}u_0 + \frac{2}{\sqrt{T}}\sum_{k=1}^{M}u_{2k} \cos \omega_kt
+ \frac{2}{\sqrt{T}}\sum_{k=1}^{M} u_{2k+1} \sin \omega_kt\,.
\end{equation}
It is then easy to prove that:
\begin{equation}
\frac{\partial f}{\partial u_m}(u,t) = f(e_m,t)\,,
\end{equation}
where $e_m$ is the set of parameters where all of them are zero, except the
$m$-th one that is equal to one.

Finally, we need the expression for the \emph{fluence}, in terms of the
parameters:
\begin{equation}
L(u) := \int_0^T\!\!{\rm d}t\;\; f^2(u,t) \approx h\sum_{j=0}^{N-1} f_j^2 =
\sum_{k=0}^{N-1} \vert c_k\vert^2 = \vert c_0\vert^2 + 2 \sum_{k=1}^{N/2}
\vert c_k\vert^2 = u_1^2 + 2\sum_{k=1}^M(u_{2k}^2+u_{2k+1}^2)\,.
\end{equation}
The derivatives of this are also needed:
\begin{equation}
\frac{\partial L}{\partial u_l}(u) = \left\{\begin{array}{ll}
2u_l & (l=1)
\\
4u_l & (l>1)
\end{array}\right.
\end{equation}




\subsection{\label{sec:cfourier} Parametrization: constrained Fourier expansion}

Now we want to make sure that:
\begin{equation}
\int_0^T\!\!{\rm d}t\; f(t)=0
\end{equation}
and
\begin{equation}
f(0)=f(T)=0\,.
\end{equation}
Assuming a Fourier series, this is achieved through the constraints:
\begin{equation}
c_0 = 0\,,
\end{equation}
and
\begin{equation}
\sum_{k=1}^M {\rm Re} c_k = 0\,.
\end{equation}

These two constraints imply that we will only have $P=2M-1$ degrees of
freedom, instead of $2M+1$. We can automatically enforce the constraints by
establishing the following parametrization:
\begin{eqnarray}
\nonumber
{\rm Re} c_1 & = & u_1\,,
\\\nonumber
{\rm Im} c_1 & = & - u_2\,,
\\\nonumber
\dots & = & \dots
\\\nonumber
{\rm Re} c_l & = & u_{2l-1} \,,
\\\nonumber
{\rm Im} c_l & = & - u_{2l}\,,
\\\nonumber
\dots & = & \dots
\\\nonumber
{\rm Re} c_M & = &  -\sum_{l=1}^{M-1} u_{2l-1} \,,
\\
{\rm Im} c_M  & = & - u_{2M-1}\,.
\end{eqnarray}

The function $f(u,t)$ is therefore given by:
\begin{eqnarray}
\nonumber
f(u,t) & = & \frac{2}{\sqrt{T}}\sum_{k=1}^{M-1} u_{2k-1} (\cos \omega_k t -
\sin \omega_M t )
\\
& & 
+ \frac{2}{\sqrt{T}}\sum_{k=1}^{M-1} u_{2k}\sin \omega_k t
+ \frac{2}{\sqrt{T}} u_{2M-1}
\sin \omega_M t
\end{eqnarray}

Despite the slightly more complicated expression, one can also verify that the
derivatives of the function with respect to the parameters are also simply given by:
\begin{equation}
\frac{\partial f}{\partial u_m}(u,t) = f(e_m,t)\,,
\end{equation}

Finally, the fluence and its derivatives are in this case given by:
\begin{equation}
L(u) \approx 2\sum_{k=1}^{M} \vert c_k\vert^2 =
2\sum_{k=1}^{M-1}(u_{2k-1}^2+u_{2k}^2) + 2(\sum_{k=1}^{M-1}u_{2k-1})^2 + 2u_{2M-1}^2\,,
\end{equation}
\begin{equation}
\frac{\partial L}{\partial u_l}(u) = \left\{\begin{array}{ll}
4u_l & \text{if $l$ is even or $l=2M-1$},
\\
4u_l + 2\sum_{k=1}^{M-1}u_{2k-1}& \text{if $l$ is odd and $l\ne 2M-1$}.
\end{array}\right.
\end{equation}

\section{Input variables}

In the following, the default value is either provided between brackets following
the input variable name, or given as the underlined option whenever the input
variable is to be chosen from a series of options.

These input variables must be defined in a lua script file, named {\tt input.lua},
that is to be used as input file for the quote runs.

\subsection{General}

\begin{itemize}

\item {\tt job} 

Specifies the type of run that the code should do. The options are:
\begin{itemize}
\item {\tt job\_static}: Diagonalizes the static Hamiltonian and prints out the result.
\item \underline{\tt job\_td}: Performs a single time propagation.
\item {\tt job\_qoct}: Performs an optimization run.
\end{itemize}

\item {\tt maxtime} [$10\pi$]

The total propagation time $T$.

\item {\tt maxntime} [1000]

The total number of time steps to perform the propagations. The time step ${\tt dt}$, 
therefore, is $T/{\tt maxntime}$.

\item {\tt picture}

The code may work in the interaction or in the Schr{\"{o}}dinger picture. The possible values:
\begin{itemize}
\item {\tt schrodinger}
\item \underline{\tt interaction}
\end{itemize}

\item {\tt dim} [64]

The dimension of the Hilbert space of the problems. The input matrices ({\tt H0}, ${\tt V}$, etc.) 
should match this dimension.

\item {\tt object}

What to propagate: a Hilbert space state, or the full evolution operator. The options are:
\begin{itemize}
\item \underline{\tt wavefunction}
\item {\tt evolution\_operator}
\end{itemize}


\subsection{External fields}

\item {\tt nfield} [1]
The number of external fields to be included in the calculation. Note that when running
in {\tt job\_qoct} mode this should be equal to one (the general case is not implemented yet).


\item {\tt tdfield\_representation}
The parametrization used to represent the external field amplitude. The options are:
\begin{itemize}

\item \underline{\tt tdfield\_realtime}
Each parameter is simply the value of the amplitude at each of the nodes of the
real-time discretization:
\begin{equation}
u_j = \varepsilon(t_j)\;\;\; (j=1,{\tt maxntime}+1).
\end{equation} 

\item {\tt tdfield\_fourier}
The amplitude is expanded as a Fourier series, as described in section~\ref{sec:fourier} above. 
The user should provide a cutoff.

\item {\tt tdfield\_constrained\_fourier}
This is the constrained version of the Fourier series, described in section~\ref{sec:cfourier} above.

\item {\tt tdfield\_generic}
The user may specify, in the lua input file, a ``generic'' function, given as a mathematical expression.
The name of the function should be specified through the {\tt tdfield\_generic\_definition} input variable.
It should depend on a number of parameters, given by the {\tt tdfield\_generic\_params} input variable.

\end{itemize}

\item {\tt cutoff}
The cutoff energy used for the Fourier series.

\item {\tt tdfield\_generic\_definition}
The user-defined function, given as a name in a string, that must match a function
definition in the lua script, e.g.:
\begin{verbatim}
tdfield_generic_defition = 'epsilon'

function epsilon(v, t)
  return v[1]*math.sin(v[2]*t)
end
\end{verbatim}
In this case, the function is $v_1\sin(v_2 t)$, and $v_1,v_2$ are two parameters.
The mathematical functions (such as the sine function case that can be used, are described
in the lua users guide [http://www.lua.org/manual/5.3/manual.html\#6.7].

The number of parameters should be given through the {\tt tdfield\_generic\_nparams} input variable.

\item {\tt tdfield\_generic\_der\_definition}
This input variable should hold the name of a lua function that computes the derivative
of the {\tt tdfield\_generic\_definition} function with respect to its parameters. It is
only needed in optimization runs. Following the previous example:
\begin{verbatim}
tdfield_generic_der_definition = 'depsilondv'

function depsilondv(v, j, t)
  if (j==1)
  then
    return math.sin(v[2]*t)
  elseif (j==2)
  then
    return (v[1]*t)*math.cos(v[2]*t)
  end
end
\end{verbatim}

\item {\tt tdfield\_generic\_nparams}

The number of parameters used to define the amplitude function. 

\item {\tt tdfield\_init\_from}

The user may choose how to initialize the amplitude function. The options are:
\begin{itemize}

\item \underline{\tt tdfield\_init\_from\_expression}
An user-supplied function expression for the initial amplitude function. The
name of the function to be used should be provided through the
{\tt initialize\_from\_filename} variable. Then, the input file should
also contain the definition of that function (that should in this case only
be a function of time, and not depend on any extra parameters).

[Note: This possibility will disappear in future commits, as it is unnecessary; one
may always use the {\tt tdfield\_generic} option, provide a generic function definition,
and initialize the parameters]

\item {\tt tdfield\_init\_from\_params} The user supplies the parameters that
determine the amplitude function, through the variable {\tt tdfield\_init\_parameters}.

\item {\tt tdfield\_init\_random} The parameter values are given random values.
In order for this to work, it is necessary that the user constrains the parameter
region, through the {\tt upper\_bounds} and {\tt lower\_bounds} input variables (see below).

\item {\tt tdfield\_init\_from\_file} The amplitude function is read from a file. The name
of the file is given in the {\tt initialize\_from\_filename} variable.

\end{itemize}

\item {\tt tdfield\_init\_parameters}

If {\tt tdfield\_init\_from = tdfield\_init\_from\_params}, the initial parameters
must be given in this input variable (that should be defined as an array).

\item {\tt initialize\_from\_filename}

If {\tt tdfield\_init\_from = tdfield\_init\_from\_file}, the name of the file
should be given through this input variable.

\item {\tt lower\_bounds} 

An array containing lower bounds for the amplitude
function parameters. Note that not all optimation algorithms allow for a constrained
search, and that all global optimization algorithms do require bounds.

\item {\tt upper\_bounds} 

An array containing upper bounds for the amplitude
function parameters. Note that not all optimation algorithms allow for a constrained
search, and that all global optimization algorithms do require bounds.

Either both supply upper and lower bounds, or do not supply bounds at all, in which
case the optimization will be unconstrained.

\item {\tt rng\_seed} [0]

If {\tt tdfield\_init\_from = tdfield\_init\_random}, the random number generator seed.



\subsection{Propagation}

\item {\tt propagator}

The propagator algorithm. The options are:
\begin{itemize}

\item \underline{\tt propagator\_taylor\_expansion}

\item {\tt propagator\_rungekutta}

\item {\tt propagator\_cfmagnus4}

\end{itemize}

\item {\tt initial\_states}

This array input variable holds the coefficients of the initial Hilbert state to start the propagation from.
If {\tt object = evolution\_operator}, it is not necessary. However, it may be included, as the code may
perform the propagation of the state, in addition to the propagation of the evolution operator.

The elements of the array are the real and imaginary part of the coefficients, in order, i.e.
${\rm Re} c_1, {\rm Im} c_1, {\rm Re} c_2, {\rm Im} c_2, \dots$

\item {\tt observables}

A string with the names of files containing observables. For example, if {\tt observable = "O1,O2"}, then
the code will look for the files {\tt input/O1} and {\tt input/O2}, that should hold two Hermitian matrices
representing the observables.

In a {\tt job = job\_td} run, their expectation value along the evolution
would be printed out in files {\tt output/td-O1} and {\tt output/td-O2}.

\subsection{Optimization}

\item {\tt qoct\_algorithm} [{\tt qoct\_algorithm\_nlopt\_ld\_lbfgs}]

The optimization algorithm for the QOCT runs. The options are all the algorithms included
in the NLOPT library. The options are named {\tt qoct\_algorithm\_xxx}, where
{\tt xxx} is the name of the corresponding NLOPT algorithm (see the NLOPT documentation).
For example, {\tt qoct\_algorithm\_nlopt\_gn\_direct}, etc.

One exception is the option {\tt qoct\_algorithm\_check}, that signals the code
not to do any optimization at all, but to perform a check on the quality 
of the gradient computed with the QOCT formula, by comparing it to the gradient
computed with a finite-differences formula.

\item {\tt qoct\_check\_nparam} [5]

When {\tt qoct\_algorithm = qoct\_algorithm\_check}, the comparison of the
gradients is done for a number of parameters only, given by this variable.

\item {\tt qoct\_check\_fd\_delta} [0.01]

This is the $\Delta$ parameter used in the finite-difference formula
used when {\tt qoct\_algorithm = qoct\_algorithm\_check}.

\item {\tt qoct\_maxiter} [10]

The maximum number of iterations that the optimization will perform before stopping. Note that
it is only approximate, as some algorithms cannot stop after an arbitrary number of iterations.

\item {\tt qoct\_tol\_f} [1.0e-7]

This, and the following {\tt qoct\_tol\_x} input variables, are the stopping criteria for the QOCT optimizations.
Their meaning is described in the NLOPT documentation.

\item {\tt qoct\_tol\_dr} [-1]

\item {\tt qoct\_tol\_f\_abs} [-1]

\item {\tt qoct\_tol\_dr\_abs} [-1]

\item {\tt qoct\_stopval}

Stop the optimization if the algorithm finds a solution above this threshold.

\item {\tt qoct\_penalty} [1.0]

In addition to the $J_1$ functional that is defined through the {\tt target\_type} variable
(see below), the definition of the target functional may include a $J_2$ penalty term, 
that is defined as:
\begin{equation}
J_2(u) = -\alpha \int_0^T\!\!{\rm d}t\; \varepsilon^2(u,t)\,.
\end{equation}
{\tt qoct\_penalty} determines the value of the $\alpha$ weight constant. This penalty can
be used to avoid regions of parameter space with too big field intensities.

\item {\tt target\_type}

How the target is defined. The options are:
\begin{itemize}

\item {\tt qoct\_target\_occup}

The goal is to maximize the population of a subspace, spanned by the states given in the
{\tt target\_states} variable. [In fact, in the current version of the code, only one
state is possible. For more general cases, one should use the {\tt qoct\_target\_operator}
options.]

\item {\tt qoct\_target\_u}

The goal is the maximization of the overlap of the final evolution operator $U(T)$ with
some target evolution operator $U_{\rm target}$. This operator
is given in the file specified by the {\tt utarget} variable.

\item {\tt qoct\_target\_operator}

The goal is the maximization of the expectation value of some
observable, whose name is given in the {\tt target\_operator}
variable.

\end{itemize}

\item {\tt target\_states}

An array containing the coefficients of the state to be used as target,
in case {\tt qoct\_target\_type = qoct\_target\_occup}.


\item {\tt utarget}

If {\tt qoct\_target\_type = qoct\_target\_u}, this variable should be given the name
of a file that stores the definition of the target evolution operator.

\item {\tt target\_operator}

If {\tt qoct\_target\_type = qoct\_target\_operator}, this variable should be given the name
of a file that stores the definition of the target operator, whose
expectacion value is to be maximized.


\end{itemize}

\end{document}
