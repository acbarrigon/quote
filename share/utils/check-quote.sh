#! /bin/bash

##########################################################################################
# Output invocation
echo
echo "______________________________________________________________________"
echo "SCRIPT INVOKED AS:"
echo $0 $@
echo "______________________________________________________________________"
echo
#
##########################################################################################





##########################################################################################
# Definition of the usage
function usage() {
  cat <<EOF

Usage: check-quote.sh [options]

    -h			This message
    -t [runmode]	Options: gs / single_step / td / tdtotalt / opt / totalt
    -x [octopus]	The executable
    -n #nodes		Number of MPI processes to run on.
    -w [omega]		Frequency of the applied external field (default: 1.0)
    -u [u]		An integer number that multiples w.
    -a [a]		The amplitude of the applied external field.
    -k [k]              The amplitude of the "kick" e(-i * k * x) applied to the orbitals at time zero (default: 0)
    -q [nperiods]	The total propagation time will be (2*pi/w)*nperiods (default: 1)
    -m [ndivisions]	The fundamental number of division of each period to get the spacing (default: 200)
    -p [npoints]	(default: 4)
    -l [gridsize]       Whether the grid of points is linear, in which case gridsize is the step size. If "-l" is
                        not given, the grid is logarithmic, meaning that it is built by doubling the number of divisions.
    -r [order]		(default: 4)
    -v [propagator]	[default: qoct_tddft_propagator]
    -o [option]		Place in the environment any octopus variable. Use the syntax 
			-o "OCT_WhateverVarible=WhateverValue"
			For example:
			-o "OCT_ExtraStates=1"
    -f [logfactor]
EOF
  exit 0;
}
#
##########################################################################################


##########################################################################################
# Definition of the quote and compare invocations
function runquote() {
  $CHECKPRBINDIR/quote
}


function runcompare() {
  dim=$(head -n 1 input/H0)
  tail -n 1 output/td-coeff > td-coeff
  tail -n 1 output-reference/td-coeff > td-coeff.ref
  paste td-coeff td-coeff.ref | awk -v dim=$dim '{ nrm2=0.0; for (i = 2; i <= dim+1; ++i) sum = sum + ($i-$(i+2*dim+1))^2; printf "%.12e\n", sqrt(sum) }'
}
#
##########################################################################################





##########################################################################################
# Read command line options. First, assign defaults.

#[ ${OCTOPUSBIN} ] || OCTOPUSBIN=$HOME/dev/octodev/inst/bin
#[ ${OCTOCOMPBIN} ] || OCTOCOMPBIN=$HOME/software/octoprop/inst/bin
#[ ${MPIEXEC} ] || MPIEXEC=mpiexec
[ ${CHECKPRBINDIR} ] || CHECKPRBINDIR=$HOME/bin
echo ${CHECKPRBINDIR}

RUNMODE=td
NPROC=0
omega=1.0
nperiods=1
ndivisions=200
npoints=4
order=4
alpha1=0.01
n1=1
kappa=0.0
logfactor=2.0
TDPropagator=propagator_taylor_expansion
LINEARGRID=no
while getopts "ht:x:n:w:u:a:q:m:p:r:v:l:k:o:f:" opt ; do
    case "$opt" in
        h) usage ;;
        t) RUNMODE="$OPTARG" ;;
	x) CHECKPRBINDIR="$OPTARG" ;;
        n) NPROC="$OPTARG" ;;
        w) omega="$OPTARG" ;;
        u) n1="$OPTARG" ;;
        a) alpha1="$OPTARG" ;;
        k) kappa="$OPTARG" ;;
        q) nperiods="$OPTARG" ;;
        m) ndivisions="$OPTARG" ;;
        p) npoints="$OPTARG" ;;
        r) order="$OPTARG" ;;
        v) SETPROPAGATOR="yes"; TDPropagator="$OPTARG" ;;
        l) LINEARGRID="yes"; gridsize="$OPTARG";;
        o) export $OPTARG ;;
        f) logfactor="$OPTARG" ;;
        ?) echo "Error parsing arguments"; exit 1 ;;
    esac
done
[ $NPROC -gt 0 ] && RUNINPARALLEL=1 ||  RUNINPARALLEL=""
# The order of the approximation of the exponential can already be set here.
OCT_TDExpOrder=$order
comparegrads=$HOME/write/articles/2013/qoct+ehrenfest-sample/scripts/comparegrads

echo ${CHECKPRBINDIR}


#
##########################################################################################


##########################################################################################
# This is for gnuplot: check if we are running in the background or not.
a=$(ps -o stat= -p $$)
[ "${a/+/}" = "$a" ] && RUNINBACKGROUND=1 || RUNINBACKGROUND=0
#
##########################################################################################



echo "______________________________________________________________________"
echo "RUNNING $CHECKPRBINDIR/quote"
echo "______________________________________________________________________"
echo




case $RUNMODE in


##########################################################################################
  reference)

  echo
  echo "______________________________________________________________________"
  echo "RUNNING IN TD REFERENCE MODE"
  echo "______________________________________________________________________"
  echo

  #OCT_CalculationMode=td

  tau=$(echo "2*3.1415926535897932/$omega" | bc -l)
  stime_total=$(echo "$nperiods*$tau" | bc -l)
  ntsteps_reference=$(($ndivisions*$nperiods*100))

  echo "   Characteristic frequency = " $omega
  echo "   Characteristic period = " $tau
  echo "   Number of periods = " $nperiods
  echo "   Total interval = " $stime_total
  echo "   Number of time steps per period = " $ndivisions
  echo "   Number of time steps for the reference calculation = " $ntsteps_reference
  echo "   Reference time step: " $(echo "$stime_total/$ntsteps_reference" | bc -l)
  echo "   Propagator = " $TDPropagator


  echo "omega0 = " $omega > tmp
  echo "alpha1 = " $alpha1 >> tmp
  echo "stime = " $stime_total >> tmp
  echo "ntsteps = " $ntsteps_reference >> tmp
  echo "kappa = " $kappa >> tmp
  echo "propagator = " $TDPropagator >> tmp
  cat tmp input.lua.base > input.lua
  runquote > out.reference 2>&1

  test -d output-reference && rm -rf output-reference
  mv output output-reference

  ;;


##########################################################################################
  td)

  echo
  echo "______________________________________________________________________"
  echo "RUNNING IN TD MODE"
  echo "______________________________________________________________________"
  echo

  tau=$(echo "2*3.1415926535897932/$omega" | bc -l)
  stime_total=$(echo "$nperiods*$tau" | bc -l)

  echo "   Characteristic frequency = " $omega
  echo "   Characteristic period = " $tau
  echo "   Number of periods = " $nperiods
  echo "   Total interval = " $stime_total
  echo "   Number of time steps per period (initial) = " $ndivisions
  echo "   Reference time step (initial) : " $(echo "$stime_total/$ndivisions" | bc -l)
  echo "   Propagator = " $TDPropagator

  declare -a prop=( $TDPropagator )

  declare -a ndiv
  ndiv[1]=$ndivisions
  ndiv[1]=$((ndiv[1]*$nperiods))
  for i in $(seq 2 $npoints)
  do
    if [ ${LINEARGRID} == "no" ] ; then
      ndiv[i]=$(bc -l <<< "scale=0; $logfactor*${ndiv[$i-1]}/1")
    else
      ndiv[$i]=$((ndiv[$i-1]+$gridsize))
    fi
  done

  for k in ${prop[*]}
  do
    TDPropagator=$k
    rm -f results.$k
    for j in ${ndiv[*]}
    do
      i=$(echo "$stime_total/$j" | bc -l)
      echo "   Propagator: " $k ";   Dividing factor: " $j ";   Time step: " $i
      echo "omega0 = " $omega > tmp
      echo "alpha1 = " $alpha1 >> tmp
      echo "stime = " $stime_total >> tmp
      echo "ntsteps = " $j >> tmp
      echo "kappa = " $kappa >> tmp
      echo "propagator = " $TDPropagator >> tmp
      cat tmp input.lua.base > input.lua
      runquote > out.$k.$j 2>&1
      #cost=$(grep 'TD_PROPAGATOR' profiling/time.0*0 | awk '{print $3}')
      #runcompare "/" "reference" #>/dev/null 2> res.$k.$j
      runcompare > res.$k.$j
      #delta=$(grep -F "OCT-COMPARE" res.$k.$j | awk '{printf $3}')
      delta=$(cat res.$k.$j)
      #echo $delta
      #reference_energy=$(head -n 7 td.general/energy | tail -n 1 | awk '{printf $3}')
      #final_energy=$(tail -n 1 td.general/energy | awk '{printf $3}')
      #delta_vel=$(grep -F "OCT-COMPARE" res.$k.$j | awk '{printf $4}')
      #echo $i $delta $cost >> results.$k
      echo $i $delta  >> results.$k
      #echo $i $final_energy $reference_energy >> results.energy.$k
      #echo $i $delta_vel >> results.coordinates.$k
      #rm -f tmp
      #rm -f inp
    done

    cat<<EOF|gnuplot -persist
if ($RUNINBACKGROUND) { 
   set terminal pngcairo enhanced; set output "$k.png" 
}
else {
   set terminal x11 enhanced
}
set key bottom right
stats "results.$k" u (log10(\$1)):(log10(\$2)) name "a"
stitle=sprintf("SLOPE = %f",a_slope)
set ytics nomirror
#set y2tics
set xlabel "log_{10} {/Symbol D}t"
set ylabel "log_{10} E"
#set y2label "Cost (s)"
set title "Error in the wavefunction"
#plot "results.$k" u (log10(\$1)):(log10(\$2)) w p title "$k" axes x1y1, a_intercept + a_slope*x w l title stitle, "results.$k" u (log10(\$1)):((\$3)) w lp title "COST" axes x1y2
plot "results.$k" u (log10(\$1)):(log10(\$2)) w p title "$k" axes x1y1, a_intercept + a_slope*x w l title stitle
EOF

#     cat<<EOF|gnuplot -persist
# if ($RUNINBACKGROUND) { 
#    set terminal pngcairo enhanced; set output "$k.energy.png" 
# }
# else {
#    set terminal x11 enhanced
# }
# set key bottom right
# stats "results.energy.$k" u (log10(\$1)):(log10(\$3-\$2)) name "a"
# stitle=sprintf("SLOPE = %f",a_slope)
# set ytics nomirror
# set xlabel "log_{10} {/Symbol D}t"
# set ylabel "log_{10} E"
# set title "Energy change"
# plot "results.energy.$k" u (log10(\$1)):(log10(abs(\$3-\$2))) w p title "$k" axes x1y1, a_intercept + a_slope*x w l title stitle
# EOF

  done

  ;;


##########################################################################################
  *) 

  echo "Unacceptable run mode";
  exit 1;

  ;;

esac




