#! /bin/bash

if [ ! -d "$1" ] ; then
  echo "You must supply the quote bin directory first argument."
  exit 2
fi
quotedir=$1

export OMP_NUM_THREADS=1

test -d tmp && rm -rf tmp
mkdir tmp

cd tmp

cp ../input.lua.base .
cp -a ../input .

../check-quote.sh -x $quotedir -t reference
../check-quote.sh -x $quotedir -t td

cd ..
