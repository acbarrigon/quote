pi = 3.14159265358979323844

-- job
job_static = 0
job_td     = 1
job_qoct   = 2

schrodinger_picture = 0
interaction_picture = 1

propagator_taylor_expansion = 2
propagator_rungekutta       = 3
propagator_cfmagnus4        = 4

wavefunction = 0
evolution_operator = 1

qoct_target_occup    = 1
qoct_target_operator = 3
qoct_target_u        = 4

tdfield_realtime            = 0
tdfield_fourier             = 1
tdfield_constrained_fourier = 2
tdfield_pulse_train         = 3
tdfield_generic             = 4

tdfield_init_from_file       = 1
tdfield_init_from_expression = 2
tdfield_init_random          = 3
tdfield_init_from_params     = 4

qoct_algorithm_check           = -1
qoct_algorithm_nlopt_gn_direct=0
qoct_algorithm_nlopt_gn_direct_l=1
qoct_algorithm_nlopt_gn_direct_l_rand=2
qoct_algorithm_nlopt_gn_direct_noscal=3
qoct_algorithm_nlopt_gn_direct_l_noscal=4
qoct_algorithm_nlopt_gn_direct_l_rand_noscal=5
qoct_algorithm_nlopt_gn_orig_direct=6
qoct_algorithm_nlopt_gn_orig_direct_l=7
qoct_algorithm_nlopt_gd_stogo=8
qoct_algorithm_nlopt_gd_stogo_rand=9
qoct_algorithm_nlopt_ld_lbfgs_nocedal=10
qoct_algorithm_nlopt_ld_lbfgs=11
qoct_algorithm_nlopt_ln_praxis=12
qoct_algorithm_nlopt_ld_var1=13
qoct_algorithm_nlopt_ld_var2=14
qoct_algorithm_nlopt_ld_tnewton=15
qoct_algorithm_nlopt_ld_tnewton_restart=16
qoct_algorithm_nlopt_ld_tnewton_precond=17
qoct_algorithm_nlopt_ld_tnewton_precond_restart=18
qoct_algorithm_nlopt_gn_crs2_lm=19
qoct_algorithm_nlopt_gn_mlsl=20
qoct_algorithm_nlopt_gd_mlsl=21
qoct_algorithm_nlopt_gn_mlsl_lds=22
qoct_algorithm_nlopt_gd_mlsl_lds=23
qoct_algorithm_nlopt_ld_mma=24
qoct_algorithm_nlopt_ln_cobyla=25
qoct_algorithm_nlopt_ln_newuoa=26
qoct_algorithm_nlopt_ln_newuoa_bound=27
qoct_algorithm_nlopt_ln_neldermead=28
qoct_algorithm_nlopt_ln_sbplx=29
qoct_algorithm_nlopt_ln_auglag=30
qoct_algorithm_nlopt_ld_auglag=31
qoct_algorithm_nlopt_ln_auglag_eq=32
qoct_algorithm_nlopt_ld_auglag_eq=33
qoct_algorithm_nlopt_ln_bobyqa=34
qoct_algorithm_nlopt_gn_isres=35
qoct_algorithm_nlopt_auglag=36
qoct_algorithm_nlopt_auglag_eq=37
qoct_algorithm_nlopt_g_mlsl=38
qoct_algorithm_nlopt_g_mlsl_lds=39
qoct_algorithm_nlopt_ld_slsqp=40
qoct_algorithm_nlopt_ld_ccsaq=41
qoct_algorithm_nlopt_gn_esch=42
qoct_algorithm_nlopt_gn_ags=43



function heaviside(t)
  if (t > 0)
  then
    return 1
  else
    return 0
  end
end

function logistic(t, k)
  return 1.0/(1.0+math.exp(-2*k*t))
end

function dlogisticdt(t, k)
  return 2.0*k*logistic(t,k)*(1-logistic(t,k))
end

function envelope(t, t0, tf, k)
  return logistic(t-t0, k) * logistic(tf-t, k)
end

function denvelopedt0(t, t0, tf, k)
  return -dlogisticdt(t-t0, k) * logistic(tf-t, k)
end

function denvelopedtf(t, t0, tf, k)
  return logisticdt(t-t0, k) * dlogisticdtf(tf-t, k)
end

function pulse(t, mu, phi, omega, t0, tf, k)
  return mu * math.cos(omega*t + phi) * envelope(t, t0, tf, k)
end

function dpulsedmu(t, mu, phi, omega, t0, tf, k)
  return math.cos(omega*t + phi) * envelope(t, t0, tf, k)
end

function dpulsedphi(t, mu, phi, omega, t0, tf, k)
  return - mu * math.sin(omega*t + phi) * envelope(t, t0, tf, k)
end

function dpulsedomega(t, mu, phi, omega, t0, tf, k)
  return - t * math.sin(omega*t + phi) * envelope(t, t0, tf, k)
end

function dpulsedt0(t, mu, phi, omega, t0, tf, k)
  return mu * math.cos(omega*t + phi) * denvelopedt0(t, t0, tf, k)
end